﻿using System;
using System.IO;
using System.Xml;

namespace next.Exchange.Traktor
{
    public class Location
    {
        private FileInfo fileInfo = null;
        private string playlistFileId;

        public Location(string primaryKey)
        {
            var winPath = primaryKey.Replace("/:", "\\");

            fileInfo = new FileInfo(winPath);
            playlistFileId = primaryKey;
        }

        public Location(string locationDirWithColons, string locationFile, string locationVolume)
        {
            string locationDir = locationDirWithColons.Replace("/:", "\\");
            
            var fileName = String.Join("", locationVolume, locationDir, locationFile);
            fileInfo = new FileInfo(fileName);
            
            this.playlistFileId = String.Join("", locationVolume, locationDirWithColons, locationFile);
        }

        public FileInfo FileInfo { get { return this.fileInfo; } }
        public string PlaylistFileId { get { return this.playlistFileId; } }

        public static Location FromXMLElement(XmlElement locationElement)
        {
            string locationDirWithColons = locationElement.GetAttribute("DIR") ?? "";
            string locationFile = locationElement.GetAttribute("FILE") ?? "";
            string locationVolume = locationElement.GetAttribute("VOLUME") ?? "";
            return new Location(locationDirWithColons, locationFile, locationVolume);
        }

        public static Location FromPrimaryKey(string key)
        {
            return new Location(key);
        }
    }
}
