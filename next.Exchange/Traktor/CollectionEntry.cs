﻿using Kit.XML;
using next.Tonality;
using next.Tracks;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Xml;

namespace next.Exchange.Traktor
{
    public class CollectionEntry : ITrackFile
    {
        public string Artist { get; protected set; }
        public string Title { get; protected set; }
        public string Album { get; protected set; }
        public string AlbumTrack { get; protected set; }
        public double? BPM { get; protected set; }
        public DMKey Key { get; protected set; }
        public Location Location { get; protected set; }
        public DateTime? ImportDate { get; protected set; }
        public string PlaylistFileId { get { return Location.PlaylistFileId; } } 

        public static CollectionEntry FromXMLElement(XmlElement entry) 
        {
            var locationElement = entry.FindElement("LOCATION");
            if (locationElement == null) throw new FormatException("Location element not found");
            var location = Location.FromXMLElement(locationElement);

            string artist = entry.GetAttribute("ARTIST") ?? "";
            string title = entry.GetAttribute("TITLE") ?? "";

            if (String.IsNullOrWhiteSpace(artist) && String.IsNullOrWhiteSpace(title))
                throw new FormatException($"Artist and title fields are both empty for {location.FileInfo.FullName}");

            double bpm = 0.0;
            var tempoElement = entry.FindElement("TEMPO");
            if (tempoElement != null)
            {
                string bpmString = tempoElement.GetAttribute("BPM");
                if (!String.IsNullOrWhiteSpace(bpmString))
                {
                    double.TryParse(bpmString, NumberStyles.Any, CultureInfo.GetCultureInfo("en-US"), out bpm);
                }
            }
            
            DMKey numberedKey = null;
            var musicalKeyElement = entry.FindElement("MUSICAL_KEY");
            if (musicalKeyElement != null)
            {
                var musicalKey = MusicalKey.FromXMLElement(musicalKeyElement);
                if (musicalKey != null)
                    numberedKey = (DMKey)musicalKey;
            }

            string albumTitle = null;
            string albumTrackNumber = null;
            var albumElement = entry.FindElement("ALBUM");
            if (albumElement != null)
            {
                albumTitle = albumElement.GetAttribute("TITLE");
                albumTrackNumber = albumElement.GetAttribute("TRACK");
            }

            DMKey okcKey = null;
            DateTime? importDate = null;
            var infoElement = entry.FindElement("INFO");
            if (infoElement != null)
            {
                DateTime tmp_importDate;
                string str_importDate = infoElement.GetAttribute("IMPORT_DATE");
                DateTime.TryParse(str_importDate, out tmp_importDate);
                importDate = (DateTime?)tmp_importDate;

                string str_keycode = infoElement.GetAttribute("KEY");
                if (!String.IsNullOrEmpty(str_keycode))
                {
                    okcKey = (DMKey)new OpenKeyCode(str_keycode);
                }
            }

            // Something is definately wrong with the conversion from musical key numbers
            if (okcKey != null && numberedKey != null && okcKey != numberedKey)
            {
                Debug.WriteLine(String.Format("{0} != {1} for {2} - {3}", numberedKey, okcKey, artist, title));
            }

            return new CollectionEntry
            {
                Artist = artist,
                Title = title,
                Album = albumTitle,
                AlbumTrack = albumTrackNumber,
                BPM = bpm,
                Key = numberedKey,
                Location = location,
                ImportDate = importDate
            };

        }

        string ITrackFile.ReleaseTitle { get { return Album; } }
        string ITrackFile.ReleaseTrackId { get { return AlbumTrack; } }
        double ITrackFile.BPM { get { return this.BPM ?? 1; } }
        public FileInfo FileInfo { get { return this.Location.FileInfo; } }

        // TODO : this needs to go!! abstraction fail
        public ISet<string> LikedByCopy { get { return null; } }
        public ISet<string> Tags { get { return null; } }

        public TrackFile ToTrackFile()
        {
            return new TrackFile(this);
        }
    }
}
