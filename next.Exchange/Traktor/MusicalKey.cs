﻿using next.Tonality;
using System;
using System.Collections.Generic;
using System.Xml;

namespace next.Exchange.Traktor
{
    public class MusicalKey
    {
        private DMKey key;

        private static Dictionary<int, DMKey> dmKeyPerNumber = new Dictionary<int, DMKey>
        {
            { 0, (DMKey)(new OpenKeyCode("1d")) },
            { 7, (DMKey)(new OpenKeyCode("2d")) },
            { 2, (DMKey)(new OpenKeyCode("3d")) },
            { 9, (DMKey)(new OpenKeyCode("4d")) },
            { 4, (DMKey)(new OpenKeyCode("5d")) },
            { 11, (DMKey)(new OpenKeyCode("6d")) },
            { 6, (DMKey)(new OpenKeyCode("7d")) },
            { 1, (DMKey)(new OpenKeyCode("8d")) },
            { 8, (DMKey)(new OpenKeyCode("9d")) },
            { 3, (DMKey)(new OpenKeyCode("10d")) },
            { 10, (DMKey)(new OpenKeyCode("11d")) },
            { 5, (DMKey)(new OpenKeyCode("12d")) },
            { 21, (DMKey)(new OpenKeyCode("1m")) },
            { 16, (DMKey)(new OpenKeyCode("2m")) },
            { 23, (DMKey)(new OpenKeyCode("3m")) },
            { 18, (DMKey)(new OpenKeyCode("4m")) },
            { 13, (DMKey)(new OpenKeyCode("5m")) },
            { 20, (DMKey)(new OpenKeyCode("6m")) },
            { 15, (DMKey)(new OpenKeyCode("7m")) },
            { 22, (DMKey)(new OpenKeyCode("8m")) },
            { 17, (DMKey)(new OpenKeyCode("9m")) },
            { 12, (DMKey)(new OpenKeyCode("10m")) },
            { 19, (DMKey)(new OpenKeyCode("11m")) },
            { 14, (DMKey)(new OpenKeyCode("12m")) }
        };

        public MusicalKey(int value)
        {
            if (value < 0 || value > 23) throw new ArgumentOutOfRangeException("0-23");

            this.key = dmKeyPerNumber[value];
        }

        public Note12 Note { get { return key.Note; } }
        public DMTonality Tonality { get { return key.Tonality; } }

        public static explicit operator DMKey(MusicalKey musicalKey)
        {
            return musicalKey.key;
        }

        public static MusicalKey FromXMLElement(XmlElement musicalKeyElement)
        {
            string musicalKeyString = musicalKeyElement.GetAttribute("VALUE");
            return FromString(musicalKeyString);            
        }

        public static MusicalKey FromString(string musicalKeyString)
        {
            if (!String.IsNullOrWhiteSpace(musicalKeyString))
            {
                int musicalKeyNumber;
                if (int.TryParse(musicalKeyString, out musicalKeyNumber))
                {
                   return new MusicalKey(musicalKeyNumber);
                }
            }
            return null;
        }
    }
}
