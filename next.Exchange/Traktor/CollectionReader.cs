﻿using Kit;
using System;
using System.Diagnostics;
using System.IO;
using System.Reactive.Linq;
using System.Threading.Tasks;
using System.Xml;

namespace next.Exchange.Traktor
{
    public class CollectionReader : ICommand<Task<IObservable<CollectionReader.Update>>, TaskScheduler>
    {
        FileInfo nmlFileInfo;

        public CollectionReader(FileInfo nmlFileInfo)
        {
            this.nmlFileInfo = nmlFileInfo;
        }

        public Task<IObservable<Update>> Execute(TaskScheduler scheduler)
        {
            return Task.Factory.StartNew<IObservable<Update>>(_ =>
            {
                return Observable.Create<Update>(
                    (IObserver<Update> observer) =>
                    {
                        if (nmlFileInfo == null)
                        {
                            observer.OnError(new ArgumentNullException("Missing nml file location"));
                            return () => { };
                        }

                        if (nmlFileInfo.Extension.ToLower() != ".nml")
                        {
                            observer.OnError(new FileLoadException("file extension must be nml"));
                            return () => { };
                        }

                        var xmlDoc = new XmlDocument();

                        observer.OnNext(new Update("Reading NML File..."));

                        try
                        {
                            xmlDoc.Load(nmlFileInfo.FullName);
                        }
                        catch (Exception ex)
                        {
                            observer.OnError(ex);
                            return () => { };
                        }

                        var nmlElements = xmlDoc.GetElementsByTagName("NML");
                        if (nmlElements.Count != 1)
                        {
                            observer.OnError(new FormatException("Expected: 1 'NML' element"));
                            return () => { };
                        }

                        var collectionNodes = xmlDoc.GetElementsByTagName("COLLECTION");

                        var entryElements = collectionNodes.Count > 0
                            ? collectionNodes[0].ChildNodes
                            : xmlDoc.GetElementsByTagName("ENTRY");

                        observer.OnNext(new Update("NML File OK"));

                        ReadEntries(entryElements, observer);

                        observer.OnCompleted();

                        return () => 
                        {
                            xmlDoc = null;
                            observer = null; 
                        };
                    });
            }, scheduler);
        }

        public static void ReadEntries(XmlNodeList entryElements, IObserver<Update> observer)
        {
            int entriesCount = 0;
            int entriesAmount = entryElements.Count;

            observer.OnNext(new Update(
                String.Format("Found {0} entries, reading...", entriesAmount),
                entriesAmount, entriesCount));

            foreach (var entry in entryElements)
            {
                try
                {
                    var xmlEntryElement = (XmlElement)entry;
                    var newEntry = CollectionEntry.FromXMLElement(xmlEntryElement);

                    string message = String.Format(
                            "Read entry #{0} OK: {1} - {2}\n\t{3}",
                            entriesCount, newEntry.Artist, newEntry.Title, newEntry.Location.FileInfo);

                    observer.OnNext(new EntryUpdate(newEntry, message, entriesAmount, entriesCount));
                }
                catch (Exception ex)
                {
                    string message = String.Format(
                        "Failed to read entry #{0}, an exception occured. Exception message: {1}", 
                        entriesCount, ex.Message);

                    Debug.WriteLine(message);

                    observer.OnNext(new FailedReadEntryUpdate(message, entriesAmount, entriesCount));
                }
                
                entriesCount += 1;
            }

            observer.OnNext(new Update("Reading entries: Done", entriesAmount, entriesCount));
        }
    
        public class Update
        {
            public string Message { get; private set; }
            public int EntriesAmount { get; private set; }
            public int EntriesCount { get; private set; }

            public Update(string message, int entriesAmount = 0, int entriesCount = 0)
            {
                this.Message = message;
                this.EntriesAmount = entriesAmount;
                this.EntriesCount = entriesCount;
            }
        }

        public class EntryUpdate : Update
        {
            public CollectionEntry Entry { get; private set; }

            public EntryUpdate(CollectionEntry entry, string message, int entriesAmount, int entriesCount)
                : base(message, entriesAmount, entriesCount)
            {
                this.Entry = entry;
            }
        }

        public class FailedReadEntryUpdate : Update
        {
            public FailedReadEntryUpdate(string errorMessage, int entriesAmount, int entriesCount)
                : base(errorMessage, entriesAmount, entriesCount)
            {
            }
        }
    }
}
