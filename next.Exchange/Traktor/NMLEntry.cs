﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using Kit.XML;

namespace next.Import.Traktor
{
    public class NMLEntry : next.ITrack
    {
        public string Artist { get; protected set; }
        public string Title { get; protected set; }
        public string Album { get; protected set; }
        public string AlbumTrack { get; protected set; }
        public double BPM { get; protected set; }
        public DMKey Key { get; protected set; }
        public NMLLocation Location { get; protected set; }

        public static NMLEntry FromXMLElement(XmlElement entry) 
        {
            if (entry.Name != "ENTRY") throw new FormatException("An NML Entry element should have ENTRY as name");

            string artist = entry.GetTagValue("ARTIST") ?? "";
            string title = entry.GetTagValue("TITLE") ?? "";

            var locationElement = entry.FindElement("LOCATION");
            if (locationElement == null) throw new FormatException("Location element not found");
            var location = NMLLocation.FromXMLElement(locationElement);

            double bpm = 0.0;
            var tempoElement = entry.FindElement("TEMPO");
            if (tempoElement != null)
            {
                string bpmString = tempoElement.GetAttribute("BPM");
                if (!String.IsNullOrWhiteSpace(bpmString))
                {
                    double.TryParse(bpmString, out bpm);
                }
            }
            
            DMKey key = null;
            var musicalKeyElement = entry.FindElement("MUSICAL_KEY");
            if (musicalKeyElement != null)
            {
                var musicalKey = MusicalKey.FromXMLElement(musicalKeyElement);
                key = (DMKey)musicalKey;
            }

            string albumTitle = null;
            string albumTrackNumber = null;
            var albumElement = entry.FindElement("ALBUM");
            if (albumElement != null)
            {
                albumTitle = albumElement.GetAttribute("TITLE");
                albumTrackNumber = albumElement.GetAttribute("TRACK");
            }

            return new NMLEntry
            {
                Artist = artist,
                Title = title,
                Album = albumTitle,
                AlbumTrack = albumTrackNumber,
                BPM = bpm,
                Key = key,
                Location = location
            };

        }

        public static explicit operator TrackFile(NMLEntry nmlEntry)
        {
            throw new NotImplementedException();
        }

        string ITrack.ReleaseTitle { get { return Album; } }
        string ITrack.ReleaseTrackId { get { return AlbumTrack; } }

    }
}
