﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Kit.XML;

namespace next.Exchange.Traktor
{
    public class PlaylistEntry
    {
        public PlaylistEntry(XmlElement entryNode)
        {
            var primaryKeyElement = entryNode.FindElement("PRIMARYKEY");
            var playlistId = primaryKeyElement.GetAttribute("KEY");
            Location = Location.FromPrimaryKey(playlistId);
        }

        public Location Location;
        
        public string FilePath { get { return Location.FileInfo.FullName;  } }
    }
}
