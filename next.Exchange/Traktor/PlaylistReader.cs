﻿using next.Store.TrackFiles;
using next.Tracks;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace next.Exchange.Traktor
{
    public class PlaylistReader
    {
        public Task<List<TrackFile>> Execute(FileInfo fileInfo)
        {
            return Task.Run<List<TrackFile>>(() =>
                {
                    if (fileInfo == null) throw new ArgumentNullException("fileInfo");

                    try
                    {
                        var xmlDoc = new XmlDocument();
                        xmlDoc.Load(fileInfo.FullName);

                        var collectionNode = xmlDoc.GetElementsByTagName("COLLECTION").Item(0);
                        var playlistNode = xmlDoc.GetElementsByTagName("PLAYLIST").Item(0);

                        var playlistCollection = GetCollection(collectionNode);
                        var playList = GetPlaylist(playlistNode, playlistCollection);

                        return playList;
                    }
                    catch (Exception ex)
                    {
                        // TODO : _something_
                    }
                    return null;
                });
        }

        private Dictionary<string, TrackFile> GetCollection(XmlNode collectionNode)
        {
            var entries = collectionNode.SelectNodes("ENTRY");

            var tracks = new List<TrackFile>();

            foreach (var xmlNode in entries)
            {
                try
                {
                    var entry = (XmlElement)xmlNode;
                    var trackFile = CollectionEntry.FromXMLElement(entry).ToTrackFile();
                    tracks.Add(trackFile);
                }
                catch (Exception ex)
                {
                    // TODO : Log
                    ;
                }

            }

            return tracks.ToDictionary(t => t.FileInfo.FullName);
        }

        public List<TrackFile> GetPlaylist(XmlNode playListNode, Dictionary<string, TrackFile> playlistCollection)
        {
            var playlist = new List<TrackFile>();

            foreach (var xmlNode in playListNode.SelectNodes("ENTRY"))
            {
                var xmlElement = (XmlElement)xmlNode;
                var playlistEntry = new PlaylistEntry(xmlElement);
                if (playlistCollection.ContainsKey(playlistEntry.FilePath))
                {
                    playlist.Add(playlistCollection[playlistEntry.FilePath]);
                }
            }

            return playlist;
        }
    }
}
