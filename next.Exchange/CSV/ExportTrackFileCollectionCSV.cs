﻿using next.Store;
using next.Store.TrackFiles;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace next.Exchange.CSV
{
    public class ExportTrackFileCollectionCSV
    {
        public static readonly Encoding utf8NoBOM = new UTF8Encoding(false);
        public static readonly Regex reservedCharPattern = new Regex("[\\;\\n\"]");
        private const char separator = ';';

        public Task Execute(TaskScheduler scheduler, TrackFileCollection collection, FileInfo targetFileInfo)
        {
            return Task.Factory.StartNew(_ =>
                {
                    ExecuteSync(collection, targetFileInfo);
                },
                scheduler);
        }

        public void ExecuteSync(TrackFileCollection collection, FileInfo targetFileInfo)
        {
            // TODO : Write CSV without BOM
            using (var writer = new StreamWriter(targetFileInfo.FullName, append: false, encoding: utf8NoBOM))
            {
                var jsonDTOs = collection.Select(trackFile => TrackFileDTO.ToDTO(trackFile));

                // TODO : Remove redundancy using lambda Expressions

                var headerLineItems = new List<String>()
                {
                    "artist",
                    "title",
                    "release_name",
                    "release_track",
                    "bpm",
                    "root_note",
                    "dm_tonality",
                    "n100_root_note",
                    "filepath"
                };

                writer.WriteLine(String.Join(separator.ToString(), headerLineItems));

                foreach (var dto in jsonDTOs)
                {
                    var lineItems = new List<object> 
                    { 
                        dto.artist, 
                        dto.title, 
                        dto.release_name, 
                        dto.release_track, 
                        dto.bpm,
                        dto.root_note, 
                        dto.dm_tonality, 
                        dto.n100_root_note, 
                        dto.filepath.Replace("\\", "/") 
                    }
                    .Select(item => ToCSVCell(item));

                    string line = String.Join(separator.ToString(), lineItems);

                    writer.WriteLine(line);
                }
            }
        }

        public static string ToCSVCell(object item)
        {
            var cellString = item as string;
            if (cellString == null)
            {
                cellString = item == null ? String.Empty : item.ToString();
            }

            if (reservedCharPattern.IsMatch(cellString))
            {
                cellString = "\"" + cellString.Replace("\"", "\"\"") + "\"";
            }

            return cellString;
        }
    }
}
