﻿# CLI

Available switches:
  /add <list-of-files> - appends the specified files to the current playlist instead of replacing the playlist content and playing them immediately
  /immediate - bypasses the "please wait" dialog when adding files
  /play, /pause, /playpause, /prev, /next, /rand, /stop - playback controls
  /exit - exits foobar2000
  /show, /hide - shows or hides the main foobar2000 window
  /config - opens the Preferences dialog
  /command:<menu command> - invokes the specified main menu command
  /playlist_command:<context menu command> - invokes the specified context menu command on current playlist selection
  /playing_command:<context menu command> - invokes the specified context menu command on currently played track
  /context_command:<context menu command> <files> - invokes the specified context menu command on the specified files