﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using next.Exchange.LastFM.Auth;
using RestSharp;

namespace next.Exchange.LastFM
{
    public class Response 
    {
        public string Content { get; internal set; }
        public string ContentType { get; internal set; }
        public Exception ErrorException { get; internal set; }
        public string ErrorMessage { get; internal set; }
        public HttpStatusCode StatusCode { get; internal set; }
        public string StatusDescription { get; internal set; }
        
        public bool Success
        {
            get
            {
                return StatusCode == HttpStatusCode.OK 
                    && ErrorException == null 
                    && String.IsNullOrEmpty(ErrorMessage);
            }
        }

        public static void Map(IRestResponse from, Response to)
        {
            to.Content = from.Content;
            to.ContentType = from.ContentType;
            to.ErrorException = from.ErrorException;
            to.ErrorMessage = from.ErrorMessage;
            to.StatusCode = from.StatusCode;
            to.StatusDescription = from.StatusDescription;
        }
    }

    public class Response<T> : Response
    {
        public T Data { get; internal set; }

        public static Response<T> From(IRestResponse<T> from)
        {
            var to = new Response<T>();

            Map(from, to);
            to.Data = from.Data;

            return to;
        }
    }
}
