﻿using next.Exchange.LastFM.Auth;
using RestSharp;
using System;
using System.Security.Cryptography;
using System.Text;

namespace next.Exchange.LastFM
{
    public class RestClient
    {
        public readonly string url = "http://ws.audioscrobbler.com/2.0/";
        private string key = "";
        private string secret = "";

        public RestClient(string key, string secret)
        {
            if (String.IsNullOrEmpty(key)) throw new ArgumentException("key cannot be empty");
            if (String.IsNullOrEmpty(secret)) throw new ArgumentException("secret cannot be empty");

            this.key = key;
            this.secret = secret;
        }

        public IRestResponse<T> Execute<T>(RestRequest request) 
            where T : new()
        {
            var client = new RestSharp.RestClient();
            client.BaseUrl = new Uri(url);
            client.UserAgent = "next-track";
            request.AddParameter("api_key", key, ParameterType.QueryString);
            request.AddParameter("format", "json", ParameterType.QueryString);

            var response = client.Execute<T>(request);

            return response; 
        }

        public static RestClient Create()
        {
            return new RestClient(ApiKeys.ApiKey, ApiKeys.Secret);
        }

        public IRestResponse<Token> GetToken()
        {
            var request = new RestRequest();
            request.AddParameter("method", "auth.gettoken", ParameterType.QueryString);
            var response = Execute<Token>(request);
            return response;
        }

        public Uri GetConfirmationUri(string token)
        {
            return new Uri(String.Format("http://www.last.fm/api/auth/?api_key={0}&token={1}", key, token));
        }

        public IRestResponse<Auth.GetSessionResponse> GetSession(string token)
        {
            var request = new RestRequest();
            var signature = GetApiSignature(token);

            request.AddParameter("method", "auth.getSession", ParameterType.QueryString);
            request.AddParameter("token", token, ParameterType.QueryString);
            request.AddParameter("api_sig", signature, ParameterType.QueryString);

            var response = Execute<Auth.GetSessionResponse>(request);
            return response;
        }

        public string GetApiSignature(string token)
        {
            string toEncode = String.Format("api_key{0}methodauth.getSessiontoken{1}{2}", key, token, secret);
            using (var md5 = MD5.Create())
            {
                return GetMd5Hash(md5, toEncode);
            }
        }

        public static string GetMd5Hash(MD5 md5Hash, string input)
        {
            // Convert the input string to a byte array and compute the hash. 
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes 
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data  
            // and format each one as a hexadecimal string. 
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string. 
            return sBuilder.ToString();
        }

    }
}
