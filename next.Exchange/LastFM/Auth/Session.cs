﻿
namespace next.Exchange.LastFM.Auth
{
    public class Session
    {
        public string Name { get; internal set; }
        public string Key { get; internal set; }
        public int Subscriber { get; internal set; }
    }
}
