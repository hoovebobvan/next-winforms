﻿
namespace next.Exchange.LastFM.Auth
{
    public class GetSessionResponse
    {
        public Session Session { get; internal set; }
    }
}
