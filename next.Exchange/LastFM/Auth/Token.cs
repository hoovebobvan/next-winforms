﻿using RestSharp.Deserializers;

namespace next.Exchange.LastFM.Auth
{
    public class Token
    {
        [DeserializeAs(Name = "token")]
        public string Value { get; internal set; }

        public override string ToString()
        {
            return (string)this;
        }

        public static implicit operator string(Token t)
        {
            return t.Value;
        }
    }
}
