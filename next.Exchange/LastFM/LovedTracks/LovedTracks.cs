﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace next.Exchange.LastFM.LovedTracks
{
    public class LovedTracks : IEnumerable<LovedTrack>
    {
        private string lastFMUserName;
        private List<LovedTrack> entries = new List<LovedTrack>();
        
        public LovedTracks(string lastFMUserName, IEnumerable<LovedTrack> entries)
        {
            this.lastFMUserName = lastFMUserName;
            this.entries.AddRange(entries);
        }

        public static LovedTracks From(string lastFMUserName, List<DTO.Track> trackDtos)
        {
            var entries = new List<LovedTrack>();
            foreach (var trackDto in trackDtos)
            {
                entries.Add(new LovedTrack
                {
                    Artist = trackDto.artist.name,
                    Title = trackDto.name,
                    Track_MBID = trackDto.mbid,
                    Artist_MBID = trackDto.artist.mbid
                });
            }
            return new LovedTracks(lastFMUserName, entries);
        }

        public IEnumerator<LovedTrack> GetEnumerator()
        {
            return entries.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return entries.GetEnumerator();
        }
    }
}
