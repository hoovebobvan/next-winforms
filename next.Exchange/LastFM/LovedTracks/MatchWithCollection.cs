﻿using Kit;
using next.Store;
using next.Tracks;
using next.Store.TrackFiles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace next.Exchange.LastFM.LovedTracks
{
    public class MatchWithCollection 
    {
        private static Dictionary<string, string> artistNameDeviations = new Dictionary<string, string>
            {
                { "cocorosie", "coco-rosie" },
                { "erp", "e-r-p" },
                { "carl-a-finlow", "carl-finlow" },
                { "scsi-9", "scsi9" },
                { "4hero", "4-hero" }
            };

        public static Result Execute(TrackFileCollection collection, IEnumerable<LovedTrack> lovedTracks)
        {
            var exceptions = new List<Exception>();
            int entryCount = 0;
            var matches = new SortedSet<TrackFile>();
            var notFound = new List<string>();

            foreach (var entry in lovedTracks)
            {
                try
                {
                    string normalizedArtistName =
                        GetCanonicalNameExtension.NormalizeNamePart(entry.Artist);
                    string normalizedTrackName =
                        GetCanonicalNameExtension.NormalizeNamePart(entry.Title);

                    foreach (string stupidArtistPrefix in new string[] { "sk'p-", "monoiz-", "zeno-" })
                    {
                        if (normalizedTrackName.StartsWith(stupidArtistPrefix))
                            normalizedTrackName = normalizedTrackName.Replace(stupidArtistPrefix, "");
                    }

                    string canonicalName =
                        GetCanonicalNameExtension.GetCanonicalName(
                            normalizedArtistName, normalizedTrackName);

                    var sameNameTracks = collection.GetByCanonicalName(canonicalName);

                    if (!sameNameTracks.Any())
                    {
                        if (artistNameDeviations.ContainsKey(normalizedArtistName))
                            normalizedArtistName = artistNameDeviations[normalizedArtistName];

                        canonicalName = GetCanonicalNameExtension.GetCanonicalName(
                            normalizedArtistName, normalizedTrackName);

                        sameNameTracks = collection.GetByCanonicalName(canonicalName);

                        if (!sameNameTracks.Any())
                            notFound.Add(canonicalName);
                    }

                    foreach (var trackFile in sameNameTracks)
                        matches.Add(trackFile);

                    entryCount += 1;
                }
                catch (Exception ex)
                {
                    exceptions.Add(ex);
                }
            }

            return new Result(entryCount, matches, notFound, exceptions);
        }

        public class Result
        {
            private List<string> notFound;
            private ISet<TrackFile> matches;
            private List<Exception> exceptions;

            public Result(int entryCount, ISet<TrackFile> matches, List<string> notFound, List<Exception> exceptions)
            {
                this.EntryCount = entryCount;
                this.matches = matches;
                this.notFound = notFound;
                this.exceptions = exceptions;
            }

            public int EntryCount { get; private set; }
            public ISet<TrackFile> Matches { get { return matches; } }
            public IEnumerable<String> NotFound { get { return notFound.AsEnumerable(); } }
            public IEnumerable<Exception> Exceptions { get { return exceptions.AsEnumerable(); } }
        }
    }
}
