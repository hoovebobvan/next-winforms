﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace next.Exchange.LastFM.LovedTracks
{
    public class LovedTrack
    {
        public string Artist { get; internal set; }
        public string Title { get; internal set; }
        public string Track_MBID { get; internal set; }
        public string Artist_MBID { get; internal set; }
    }
}
