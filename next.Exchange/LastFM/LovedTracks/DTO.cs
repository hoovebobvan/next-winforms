﻿using RestSharp.Deserializers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace next.Exchange.LastFM.LovedTracks
{
    public class DTO
    {
        public class Date
        {
            [DeserializeAs(Name = "#text")]
            public string text { get; set; }
            public string uts { get; set; }
        }

        public class Artist
        {
            public string name { get; set; }
            public string mbid { get; set; }
            public string url { get; set; }
        }

        public class Image
        {
            [DeserializeAs(Name = "#text")]
            public string text { get; set; }
            public string size { get; set; }
        }

        public class Streamable
        {
            [DeserializeAs(Name = "#text")]
            public string text { get; set; }
            public string fulltrack { get; set; }
        }

        public class Track
        {
            public string name { get; set; }
            public string mbid { get; set; }
            public string url { get; set; }
            public Date date { get; set; }
            public Artist artist { get; set; }
            public List<Image> image { get; set; }
            public Streamable streamable { get; set; }
        }

        public class Attr
        {
            public string user { get; set; }
            public int page { get; set; }
            public int perPage { get; set; }
            public int totalPages { get; set; }
            public int total { get; set; }
        }

        public class Lovedtracks
        {
            public List<Track> track { get; set; }

            [DeserializeAs(Name="@attr")]
            public Attr attr { get; set; }
        }
        
        public Lovedtracks lovedtracks { get; set; }
    }
 }
