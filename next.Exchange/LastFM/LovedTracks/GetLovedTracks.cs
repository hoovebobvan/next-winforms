﻿using Kit;
using next.Exchange.LastFM.LovedTracks;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace next.Exchange.LastFM.LovedTracks
{
    public class GetLovedTracks 
    {
        public static readonly string method = "user.getlovedtracks";

        LastFM.RestClient client;

        public GetLovedTracks(LastFM.RestClient client)
        {
            if (client == null) throw new ArgumentNullException();
            this.client = client;
        }

        public Task<LovedTracks> Invoke(string username)
        {
            return Task.Run(() =>
            {
                var pages = FetchPages(username, 1000);
                var trackDtos = pages.Aggregate(new List<DTO.Track>(), (a, p) => a.Concat(p).ToList());
                return LovedTracks.From(username, trackDtos);
            });
        }

        public IEnumerable<List<DTO.Track>> FetchPages(string username, int limit)
        {
            int? lastPage = null;
            int page = 1;

            do
            {
                var request = new RestRequest();
                request.AddParameter("method", method, ParameterType.QueryString);
                request.AddParameter("username", username, ParameterType.QueryString);
                request.AddParameter("limit", limit, ParameterType.QueryString);
                request.AddParameter("page", page, ParameterType.QueryString);
                var result = client.Execute<DTO>(request);
                if (!lastPage.HasValue)
                {
                    lastPage = result?.Data?.lovedtracks?.attr?.totalPages;
                }
                yield return result.Data.lovedtracks.track;
            }
            while (lastPage.HasValue && ++page <= lastPage.Value);
        }
    }
}
