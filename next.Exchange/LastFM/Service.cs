﻿using next.Exchange.LastFM.Auth;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace next.Exchange.LastFM
{
    public class Service
    {
        private RestClient restClient;

        public Service(RestClient restClient)
        {
            if (restClient == null) throw new ArgumentNullException();
            this.restClient = restClient;
        }

        public Response<Token> GetToken()
        {
            var restResponse = restClient.GetToken();
            return Response<Token>.From(restResponse);
        }

        public Response<Session> GetSession(string token)
        {
            var restResponse = restClient.GetSession(token);
            var response = new Response<Session>();

            Response.Map(restResponse, response);

            if (restResponse.Data != null)
                response.Data = restResponse.Data.Session;

            return response;
        }

        public Uri GetConfirmationUri(string token)
        {
            return restClient.GetConfirmationUri(token);
        }

        public static Service Create()
        {
            var restClient = RestClient.Create();
            return new Service(restClient);
        }
    }
}
