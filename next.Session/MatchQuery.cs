﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace next.Session
{
    public class MatchQuery
    {
        private N100KeyMatchQuery n100Query;
        private KeyMatchQuery keyMatchQuery;

        public MatchQuery(N100KeyMatchQuery n100Query, KeyMatchQuery keyMatchQuery)
        {
            this.n100Query = n100Query;
            this.keyMatchQuery = keyMatchQuery;
        }

        public IEnumerable<SessionTrackMatch> Execute(SessionDataContext dataContext, MatchCriteria criteria)
        {
            if (criteria.MasterTrack == null)
            {
                return new List<SessionTrackMatch>();
            }
            else
            {
                if (criteria.N100)
                {
                    return n100Query.Execute(dataContext, criteria.Session, criteria.TTNeighbors)
                        .ToList()
                        .OrderByDescending(item => item.Score);
                }
                else
                {
                    return keyMatchQuery.Execute(dataContext, criteria.Session)
                        .ToList()
                        .OrderByDescending(item => item.Score);
                }
            }
        }
    }
}
