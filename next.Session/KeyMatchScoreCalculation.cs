﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using next.Modulo;
using Kit;

namespace next.Session
{
    public class KeyMatchScoreCalculation : BaseScoreCalculation, ICommand<double, SessionTrackMatch>
    {
        public KeyMatchScoreCalculation()
        { }

        public override double Execute(SessionTrackMatch trackMatch)
        {            
            double playedBeforeScore = trackMatch.PlayedBefore ? deltaPlayedBefore : 0.0;
            double lovedTrackScore = trackMatch.IsLovedTrack ? deltaLovedTrack : 0;
            double newTrackBonus = 0;
            
            if (trackMatch.Session.FavorNewTracks)
            {
                newTrackBonus = GetNewTrackScore(trackMatch);
            }

            double score = new double[] 
            {
                baseScore, 
                lovedTrackScore,
                newTrackBonus            
            }
            .Sum();
            
            return score;
        }

    }
}
