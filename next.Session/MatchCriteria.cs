﻿using next.Tracks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace next.Session
{
    public class MatchCriteria
    {
        public TrackFile MasterTrack { get; set; }
        public bool N100 { get; set; }
        public bool TTNeighbors { get; set; }
        public bool FavorNewTracks { get; set; }
        public double BPM { get; set; }
        public CruiseSession Session { get; set; }

        public MatchCriteria(CruiseSession session)
        {
            Session = session;
            MasterTrack = session.MasterTrack;
            TTNeighbors = session.TTNeighbors;
            N100 = session.N100;
            FavorNewTracks = session.FavorNewTracks;
            BPM = session.BPM;
        }
    }
}
