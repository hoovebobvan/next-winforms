﻿using Kit;
using next.Tonality;
using next.Tracks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace next.Session
{
    public class KeyMatchQuery
    {
        private ICommand<double, SessionTrackMatch> trackMatchScoreCalculation;
        private CentimeOffset maxOffset = new CentimeOffset(20);

        public KeyMatchQuery(ICommand<double, SessionTrackMatch> trackMatchScoreCalculation)
        {
            if (trackMatchScoreCalculation == null) throw new ArgumentNullException();

            this.trackMatchScoreCalculation = trackMatchScoreCalculation;
        }

        public IEnumerable<SessionTrackMatch> Execute(SessionDataContext dataContext, CruiseSession session)
        {
            var master = session.MasterTrack;

            if (master.N100Key != null)
            {
                var matches = new List<TrackFile>();

                var masterKey = DMFloatKey.FromDMKey(master.Key);

                foreach (var key in masterKey.GetCompatibleKeys())
                {
                    var sliceMatches = dataContext.KeyIndex.GetSlice(key, maxOffset);
                    foreach (var track in sliceMatches.Distinct())
                    {
                        yield return new SessionKeyMatch(session, track, trackMatchScoreCalculation);
                    }
                }
            }
        }
    }
}
