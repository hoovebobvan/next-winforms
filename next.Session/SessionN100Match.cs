﻿using Kit;
using next.Tonality;
using next.Tracks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace next.Session
{
    public class SessionN100Match : SessionTrackMatch 
    {
        public SessionN100Match(CruiseSession session, TrackFile matchTrackFile, ICommand<double, SessionTrackMatch> scoreCalculation)
            : base(session, matchTrackFile, scoreCalculation)
        { }


        public override DMFloatKey GetKey(TrackFile matchTrackFile)
        {
            return matchTrackFile.N100Key;
        }
    }
}
