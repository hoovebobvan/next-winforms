﻿using Kit;
using next.Modulo;
using next.Tonality;
using System;
using System.Linq;

namespace next.Session
{
    public class N100ScoreCalculation : BaseScoreCalculation, ICommand<double, SessionTrackMatch>
    {
        public const double centimeOffsetScoreFactor = 4.0;
        public const double neighbouringTonalRelativePenalty = 0.30;

        public override double Execute(SessionTrackMatch trackMatch)
        {
            double tempoOffsetScore = GetTempoOffsetScore(trackMatch);
            double centimeOffsetScore = GetCentimeOffsetScore(trackMatch);
            
            double playedBeforeScore = trackMatch.PlayedBefore ? deltaPlayedBefore : 0.0;
            double lovedTrackScore = trackMatch.Session.FavorLikes && trackMatch.IsLovedTrack ? deltaLovedTrack : 0;

            double newTrackBonus = 0;
            double neighbouringTonalRelativeScore = trackMatch.IsNeigbouringTonalRelative ? -neighbouringTonalRelativePenalty : 0;

            if (trackMatch.Session.FavorNewTracks)
            {
                newTrackBonus = GetNewTrackScore(trackMatch);
            }

            double score = new double[] 
            {
                baseScore, 
                tempoOffsetScore, 
                centimeOffsetScore, 
                lovedTrackScore,
                newTrackBonus,            
                neighbouringTonalRelativeScore
            }
            .Sum();
            
            return score;
        }

        protected double GetCentimeOffsetScore(SessionTrackMatch trackMatch)
        {
            double centimeOffsetScore = -1.0
                * centimeOffsetScoreFactor
                * Math.Abs((double)trackMatch.SemitoneOffset / Centime.Cent);

            return centimeOffsetScore;
        }

    }
}
