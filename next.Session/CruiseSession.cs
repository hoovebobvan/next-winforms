﻿using Kit.Expressions;
using next.Tracks;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;

namespace next.Session
{
    public class CruiseSession : INotifyPropertyChanged
    {
        private SortedSet<TrackFile> set = new SortedSet<TrackFile>();
        private IList<SessionTrack> playlist;

        public CruiseSession(BindingList<SessionTrack> playlist, TrackFile startTrack)
        {
            if (playlist == null) throw new ArgumentNullException();
            this.playlist = playlist;
            this.startTrack = startTrack;

            if (startTrack != null)
            {
                this.BPM = startTrack.BPM;
                PushMasterTrack(startTrack);
            }
        }

        private double bpm = 100;
        public double BPM 
        { 
            get { return this.bpm; }
            set
            {
                bool hasChanged = value != bpm;

                if (hasChanged && value > 0)
                {
                    this.bpm = value;
                    RaisePropertyChange(bpmName); 
                }                    
            }
        }

        private TrackFile masterTrack = null;
        public TrackFile MasterTrack
        {
            get { return masterTrack; }
            internal set 
            { 
                masterTrack = value;
                RaisePropertyChange(masterTrackName);
            }
        }

        public bool favorNewTracks = false;
        public bool FavorNewTracks
        {
            get { return favorNewTracks; }
            set
            {
                bool hasChanged = favorNewTracks != value;

                if (hasChanged)
                {
                    this.favorNewTracks = value;
                    RaisePropertyChange(favorNewTracksName);
                }
            }
        }

        public bool n100 = false;
        public bool N100
        {
            get { return n100; }
            set
            {
                bool hasChanged = n100 != value;
                if (hasChanged)
                {
                    this.n100 = value;
                    RaisePropertyChange(n100Name);
                }
            }
        }

        public bool ttNeighbors = false;
        public bool TTNeighbors
        {
            get { return ttNeighbors; }
            set
            {
                bool hasChanged = ttNeighbors != value;
                if (hasChanged)
                {
                    this.ttNeighbors = value;
                    RaisePropertyChange(ttNeighborsName);
                }
            }
        }

        public bool favorLikes = false;
        public bool FavorLikes
        {
            get { return favorLikes; }
            set
            {
                bool hasChanged = favorLikes != value;
                if (hasChanged)
                {
                    this.favorLikes = value;
                    RaisePropertyChange(favorLikesName);
                }
            }
        }

        public void PushMasterTrack(TrackFile trackFile)
        {
            if (MasterTrack != trackFile)
            {
                playlist.Add(new SessionTrack(trackFile));
                MasterTrack = trackFile;
            }
        }

        public bool IsSessionTrack(TrackFile trackFile)
        {
            return set.Contains(trackFile);
        }

        public bool PlaylistContains(TrackFile trackFile)
        {
            return playlist.Contains(trackFile);
        }

        public IList<TrackFile> PlayList { get { return this.playlist as IList<TrackFile>; } }

        private void RaisePropertyChange(string name)
        {
            var handlerCopy = PropertyChanged;
            if (handlerCopy != null)
            {
                handlerCopy(this, new PropertyChangedEventArgs(name));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public static readonly string masterTrackName;
        public static readonly string bpmName;
        public static readonly string favorNewTracksName;
        public static readonly string n100Name;
        public static readonly string ttNeighborsName;
        public static readonly string favorLikesName;

        private TrackFile startTrack;

        static CruiseSession()
        {
            Expression<Func<CruiseSession, object>> getMasterTrack = cs => cs.MasterTrack;
            Expression<Func<CruiseSession, object>> getBPM = cs => cs.BPM;
            Expression<Func<CruiseSession, object>> getFavorNewTracks = cs => cs.FavorNewTracks;
            Expression<Func<CruiseSession, object>> getN100 = cs => cs.N100;
            Expression<Func<CruiseSession, object>> getTTNeighbors = cs => cs.TTNeighbors;
            Expression<Func<CruiseSession, object>> getFavorLikes = cs => cs.FavorLikes;


            masterTrackName = getMasterTrack.GetMemberName();
            bpmName = getBPM.GetMemberName();
            favorNewTracksName = getFavorNewTracks.GetMemberName();
            n100Name = getN100.GetMemberName();
            ttNeighborsName = getTTNeighbors.GetMemberName();
            favorLikesName = getFavorLikes.GetMemberName();
        }

    }
}
