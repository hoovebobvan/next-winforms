﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using next.Modulo;
using Kit;
using next.Tonality;

namespace next.Session
{
    public abstract class BaseScoreCalculation : ICommand<double, SessionTrackMatch>
    {
        public static readonly double baseScore = 3.0;
        public static readonly double tempoOffsetPenaltyFactor = -0.5;
        public static readonly double deltaLovedTrack = 0.3;
        public static readonly double deltaPlayedBefore = -2.0;
        public static readonly double newTrackBonus = 0.50;

        protected double GetTempoOffsetScore(SessionTrackMatch trackMatch)
        {
            // When the tempofactor is 2-to-the-power-of-n fold, tracks will run in sync
            // The deviation from n is much you have to compensate for pitch-wise
            /*
             
            x  : Math.Pow(x, 2)  
            0,1: -3,32
            0,2: -2,32
            0,3: -1,74
            0,4: -1,32
            0,5: -1
            0,6: -0,74
            0,7: -0,51
            0,8: -0,32
            0,9: -0,15
            1: 0
            1,1: 0,14
            1,2: 0,26
            1,3: 0,38
            1,4: 0,49
            1,5: 0,58
            1,6: 0,68
            1,7: 0,77
            1,8: 0,85
            1,9: 0,93
            */

            var tempoOffsetSTMod12 = Math.Log(trackMatch.TempoFactor, DMPitchedKey.SemitoneFactor).Modulo(12);

            var stDistancePhase = tempoOffsetSTMod12 > 6 ? 12 - tempoOffsetSTMod12 : tempoOffsetSTMod12;

            // given we play a record at it's original bpm, typically pitching 1 semitone (around 5.9%)
            // pithcing 2 semitones, the track may start losing intent

            //// 1 semitone is +- 6%, this is about the maximum you would want to deviate from the orignial temp
            //// 2 semitones tempo offset will no longer sound allrite          

            double score = tempoOffsetPenaltyFactor * Math.Pow(stDistancePhase, 1.1);

            return score;
        }

        protected double GetNewTrackScore(SessionTrackMatch trackMatch)
        {
            if (trackMatch.Session.FavorNewTracks)
            {
                if (trackMatch.MatchTrack.ImportDate.HasValue)
                {
                    var age = DateTime.Now.Subtract(trackMatch.MatchTrack.ImportDate.Value);
                    double ageDays = Math.Min(age.Days, 30.0 * 24.0); // approx 24 months

                    double penalty = Math.Pow(ageDays, 2.0) / Math.Pow(30.0 * 24.0, 2.0); // powers of 2
                    return (1.0 - penalty) * newTrackBonus;
                }
            }
            return 0;
        }

        public abstract double Execute(SessionTrackMatch arg);
    }
}
