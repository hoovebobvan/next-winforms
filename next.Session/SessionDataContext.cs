﻿using next.Store;
using next.Store.KeyIndex;
using next.Store.TrackFiles;
using next.Tonality;
using next.Tracks;
using System;
using System.IO;

namespace next.Session
{
    public class SessionDataContext : IDisposable
    {
        private TrackFileCollection collection;
        private DMFloatKeyIndex<TrackFile> n100Index;
        private DMFloatKeyIndexConnector n100IndexConnector;
        private DMFloatKeyIndex<TrackFile> keyIndex;
        private DMFloatKeyIndexConnector keyIndexConnector;

        public SessionDataContext(TrackFileCollection collection)
        {
            if (collection == null) throw new ArgumentNullException();
            this.collection = collection;
            
            this.n100Index = new DMFloatKeyIndex<TrackFile>(t => t.N100Key);
            this.n100IndexConnector = new DMFloatKeyIndexConnector(collection, n100Index);

            this.keyIndex = new DMFloatKeyIndex<TrackFile>(t => DMFloatKey.FromDMKey(t.Key));
            this.keyIndexConnector = new DMFloatKeyIndexConnector(collection, keyIndex);
        }

        public DMFloatKeyIndex<TrackFile> N100Index { get { return n100Index; } }

        public DMFloatKeyIndex<TrackFile> KeyIndex { get { return keyIndex; } }
        
        public TrackFile GetByFileInfo(FileInfo fileInfo)
        {
            return collection.GetByFileInfo(fileInfo);
        }

        public void Dispose()
        {
            this.n100Index.Reset();
            this.n100IndexConnector.Dispose();
            this.keyIndex.Reset();
            this.keyIndexConnector.Dispose();
        }
    }
}
