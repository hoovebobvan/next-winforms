﻿using Kit;
using next.Tonality;
using next.Tracks;
using System.Collections.Generic;
using System.Linq;

namespace next.Session
{
    public class N100KeyMatchQuery
    {
        private ICommand<double, SessionTrackMatch> trackMatchScoreCalculation;
        private CentimeOffset maxOffset;

        public N100KeyMatchQuery(ICommand<double, SessionTrackMatch> trackMatchScoreCalculation, CentimeOffset maxOffset)
        {
            // TODO: Complete member initialization
            this.trackMatchScoreCalculation = trackMatchScoreCalculation;
            this.maxOffset = maxOffset;
        }

        public IEnumerable<SessionTrackMatch> Execute(SessionDataContext dataContext, CruiseSession session, bool includeTTNeighbors)
        {
            var master = session.MasterTrack;

            if (master.N100Key != null)
            {
                var matches = new List<TrackFile>();

                foreach (var key in master.N100Key.GetCompatibleKeys(includeTTNeighbors))
                {
                    var sliceMatches = dataContext.N100Index.GetSlice(key, maxOffset);
                    foreach (var track in sliceMatches.Distinct())
                    {
                        yield return new SessionN100Match(session, track, trackMatchScoreCalculation);
                    }
                }
            }
        }
    }
}
