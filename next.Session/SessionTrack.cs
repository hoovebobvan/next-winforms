﻿using next.Tracks;
using System;

namespace next.Session
{
    // TODO : Get rid of this class as wel as the not so useful YayNay feature
    public class SessionTrack : TrackFile
    {
        public SessionTrack(TrackFile track)
            : base(
                  track.FileInfo,
                  track.Artist,
                  track.Title,
                  track.ReleaseTitle,
                  track.ReleaseTrackId,
                  track.BPM,
                  track.Key,
                  track.ImportDate,
                  track.Likes,
                  track.Tags)
        {
        }

        public YayNay YayNay { get; internal set; }
    }
}
