﻿
namespace next.Session
{
    // TODO : Replace score with this object so the calculation
    //          can be exposed to the user
    public class TrackMatchScore
    {
        public double TempoFactorPenalty { get; private set; }
        public double CentimeOffsetPenalty { get; private set; }
        public double LovedTrackBonus { get; private set; }
        public double YayNayDifference { get; private set; }
        public double Score { get; private set; }
    }
}
