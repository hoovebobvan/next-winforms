﻿
using Kit;
using System;
using System.Diagnostics;
using next.Modulo;
using next.Tonality;
using next.Tracks;

namespace next.Session
{
    public abstract class SessionTrackMatch 
    {
        public enum CircleOfFifthsDeltas { Equal, TonalityToggle, Plus7, Minus7, TonalityToggleNeighbor, Unknown }

        protected CruiseSession session;
        protected TrackFile matchTrackFile;
        protected TrackFile matchedToTrack;
        protected CentimeOffset? semitoneOffset;
        protected double tempoFactor;
        protected double score;
        protected bool playedBefore;
        protected ICommand<double, SessionTrackMatch> scoreCalculation;
        protected CircleOfFifthsDeltas circleOfFifthsDelta;

        public SessionTrackMatch(CruiseSession session, TrackFile matchTrackFile, ICommand<double, SessionTrackMatch> scoreCalculation)
        {
            this.session = session;
            this.matchTrackFile = matchTrackFile;
            this.scoreCalculation = scoreCalculation;

            this.matchedToTrack = matchTrackFile;
            if (session.MasterTrack.N100PhasePerCent.HasValue && matchTrackFile.N100PhasePerCent.HasValue)
            {
                semitoneOffset = session.MasterTrack.N100PhasePerCent.Value.GetOffsetTo(matchTrackFile.N100PhasePerCent.Value);
            }
            this.circleOfFifthsDelta = CalculateCircleOfFifthsDelta(GetKey(session.MasterTrack), GetKey(matchTrackFile));
            this.tempoFactor = matchTrackFile.BPM / session.BPM;
            this.playedBefore = session.PlaylistContains(matchTrackFile);
            this.score = scoreCalculation.Execute(this);
        }

        public abstract DMFloatKey GetKey(TrackFile matchTrackFile);

        protected CircleOfFifthsDeltas CalculateCircleOfFifthsDelta(DMFloatKey originKey, DMFloatKey matchedKey)
        {
            // TODO: Shenanigans!!! When finding matches the intended offset is know, and it should be stored there and then

            if (originKey == null || matchedKey == null)
                return CircleOfFifthsDeltas.Unknown;

            bool tonalityChanged = originKey.Tonality != matchedKey.Tonality;

            var offset = (matchedKey.RootNote - originKey.RootNote);
            int discreteOffset = ((int)Math.Round(offset)).Modulo(12);

            Debug.WriteLine(String.Format("offset: {0}, discrete_offset: {1}, tonality_toggle {2}", offset, discreteOffset, tonalityChanged));

            if (!tonalityChanged)
            {
                switch (discreteOffset)
                {
                    case 0:
                        return CircleOfFifthsDeltas.Equal;
                    case 7:
                        return CircleOfFifthsDeltas.Plus7;
                    case 5:
                        return CircleOfFifthsDeltas.Minus7;
                    default:
                        return CircleOfFifthsDeltas.Unknown;
                }
            }
            else
            { 
                switch (discreteOffset)
                {
                    case -3:
                    case 3:
                        return CircleOfFifthsDeltas.TonalityToggle;
                    default:
                        // better check
                        return CircleOfFifthsDeltas.TonalityToggleNeighbor;
//                        return CircleOfFifthsDeltas.Unknown;
                }              
            }
        }

        public CruiseSession Session { get { return session; } }
        public TrackFile OriginTrack { get { return session.MasterTrack; } }
        public double TargetBPM { get { return session.BPM; } }
        public TrackFile MatchTrack { get { return matchTrackFile; } }
        public TrackFile MatchedToTrack { get { return matchedToTrack; } }
        public CentimeOffset? SemitoneOffset { get { return semitoneOffset; } }
        public CircleOfFifthsDeltas CircleOfFifthsDelta { get { return this.circleOfFifthsDelta;  } }
        public bool IsNeigbouringTonalRelative { get { return CircleOfFifthsDelta == CircleOfFifthsDeltas.TonalityToggleNeighbor; } }
        public double TempoFactor { get { return tempoFactor; } }
        public bool PlayedBefore { get { return playedBefore; } }
        public double Score { get { return score; } }
        public bool IsLovedTrack { get { return MatchTrack.IsLiked; } }
    }
}
