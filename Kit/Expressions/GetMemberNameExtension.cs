﻿using System;
using System.Linq.Expressions;

namespace Kit.Expressions
{
    public static class MemberNameExtensions
    {
        // ref 2013-07 : http://joelabrahamsson.com/getting-property-and-method-names-using-static-reflection-in-c/
        // changed to allow for lambda expressions

        public static string GetMemberName(this Expression expression)
        {
            if (expression == null) throw new ArgumentNullException();

            var lambdaExpression = expression as LambdaExpression;
            if (lambdaExpression != null)
                return GetMemberName(lambdaExpression.Body);


            if (expression is MemberExpression)
            {
                // Reference type property or field
                var memberExpression = (MemberExpression)expression;
                return memberExpression.Member.Name;
            }

            if (expression is MethodCallExpression)
            {
                // Reference type method
                var methodCallExpression =
                    (MethodCallExpression)expression;
                return methodCallExpression.Method.Name;
            }

            var unaryExpression = expression as UnaryExpression;
            if (unaryExpression != null)
            {
                if (unaryExpression.Operand is MethodCallExpression)
                {
                    var methodExpression =
                        (MethodCallExpression)unaryExpression.Operand;
                    return methodExpression.Method.Name;
                }

                return ((MemberExpression)unaryExpression.Operand)
                            .Member.Name;
            }

            throw new ArgumentException("Invalid expression");
        }

    }
}
