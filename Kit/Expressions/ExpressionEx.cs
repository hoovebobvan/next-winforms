﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace Kit.Expressions
{
    public static class ExpressionEx
    {
        public static BinaryExpression Assign(Expression left, Expression right)
        {
            var assign = typeof(Assigner<>).MakeGenericType(left.Type).GetMethod("Assign");

            var assignExpr = Expression.Add(left, right, assign);

            return assignExpr;
        }

        private static class Assigner<T>
        {
            public static T Assign(ref T left, T right)
            {
                return (left = right);
            }
        }

        public static Expression<Action<TClass, TProperty>> SetterFromGetter<TClass, TProperty>(
            GetterExpression<TClass, TProperty> getter)
        {
            Expression<Func<TClass, TProperty>> expression = getter;
            var memberExpression = (MemberExpression)expression.Body;
            var parameterTClass = expression.Parameters.First();
            var parameterTProperty = Expression.Parameter(typeof(TProperty), "y");

            var setterExpression = Expression.Lambda<Action<TClass, TProperty>>(
                    ExpressionEx.Assign(memberExpression, parameterTProperty),
                    parameterTClass,
                    parameterTProperty
                );

            return setterExpression;
        }


    }
}
