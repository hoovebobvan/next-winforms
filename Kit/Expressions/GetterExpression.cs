﻿using System;
using System.Linq.Expressions;

namespace Kit.Expressions
{
    /// <summary>
    /// A Func of A to B is not necessarily a Getter.
    /// Shape of a getter :     (TClass x) => x.SomeProperty
    /// Example of non-getter:  (string x) => return 5
    /// 
    /// This class can serve to ensure you're working with getters, and also provide some information
    /// that you'll typically need when working with getters 
    /// </summary>
    /// <typeparam name="TClass"></typeparam>
    /// <typeparam name="TProperty"></typeparam>
    public class GetterExpression
    {
        public GetterExpression(Expression expression)
        {
            if (expression == null) throw new ArgumentNullException();
          
            try
            {
                this.MemberName = expression.GetMemberName();
            }
            catch
            {
                throw new ArgumentException("Expression is not a getter");
            }
            this.Expression = expression;
        }
        
        public string MemberName { get; protected set; }
        public Expression Expression { get; protected set; }
    }

    public class GetterExpression<TClass, TProperty> : GetterExpression
    {
        public GetterExpression(Expression<Func<TClass, TProperty>> expression)
            : base(expression)
        {
            this.Expression = expression;
            this.Getter = expression.Compile();
        }

        public static implicit operator Expression<Func<TClass, TProperty>>(GetterExpression<TClass, TProperty> se)
        {
            return se.Expression;
        }

        public static explicit operator GetterExpression<TClass, TProperty>(Expression<Func<TClass, TProperty>> se)
        {
            return new GetterExpression<TClass, TProperty>(se);
        }

        public new Expression<Func<TClass, TProperty>> Expression { get; private set; }
        public Func<TClass, TProperty> Getter { get; private set; }

        public virtual Action<TClass, TProperty> GetSetter()
        {
            try
            {
                return ExpressionEx.SetterFromGetter(this).Compile();
            }
            catch
            {
                throw new ArgumentException("cannot derive setter from getter", "getter");
            }

        }
    }
}
