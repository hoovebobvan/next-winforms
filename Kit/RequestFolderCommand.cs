﻿using System;
using System.IO;
using System.Windows.Forms;

namespace Kit
{
    public class RequestFolderCommand 
    {
        public void Execute()
        {
            var dialog = new OpenFileDialog();
            dialog.FileName = "Filename will be ignored";
            dialog.CheckPathExists = true;
            dialog.CheckFileExists = false;
            dialog.ReadOnlyChecked = true;

            if (DialogResult.OK == dialog.ShowDialog())
            {
                var directory = new FileInfo(dialog.FileName).Directory;
                if (directory != null && directory.Exists)
                    Selected.Raise(directory);
            }
        }

        public event Action<DirectoryInfo> Selected;
    }
}
