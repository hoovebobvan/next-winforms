﻿
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Kit
{
    public class AcceptIndicator : Panel
    {
        public enum States { Neutral, Changing, Accept, Deny }

        private Dictionary<States, Image> imagesPerState = new Dictionary<States, Image>
        {
            { States.Neutral, KitResources.cancel_16_grey },
            { States.Changing, KitResources.loading_16 },
            { States.Accept, KitResources.accept_16 },
            { States.Deny, KitResources.cancel_16 }
        };

        public AcceptIndicator() : base()
        {
            this.BackgroundImageLayout = ImageLayout.Center;

            this.State = States.Neutral;
            this.Padding = new Padding(2);
            this.Size = this.BackgroundImage.Size + new Size(this.Padding.Left + this.Padding.Right, this.Padding.Top + this.Padding.Bottom);
            this.Invalidate();
        }

        public States State
        {
            set 
            {
                this.BackgroundImage = imagesPerState[value];
            }
        }

        protected override Padding DefaultMargin
        {
            get { return new Padding(0, 0, 3, DefaultFont.Height); }
        }
    }
}
