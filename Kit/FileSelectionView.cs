﻿using System.IO;

namespace Kit
{
    public class FileSelectionView : SelectionView
    {    
        public FileSelectionView()
            : base()
        { }

        public FileInfo FileInfo
        {
            set
            {
                this.Text = value == null ? "" : value.FullName;
            }
        }
    }
}
