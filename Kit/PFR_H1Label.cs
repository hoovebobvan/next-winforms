﻿

namespace Kit
{
    [System.ComponentModel.DesignerCategory("")]
    public class PFR_H1Label : PFR_Label
    {
        public static readonly float h1FontSizeFactor = 1.25F;
        
        public PFR_H1Label() : base(h1FontSizeFactor)
        { }
    }
}