﻿using System;
using System.Windows.Forms;

namespace Kit
{
    public static class InvokeExtensions
    {
        // Allows for direct invocation of lambdas, sou you'll type:
        //      this.Invoke(() => buttonSomething.Enabled = value);
        // Instead of:
        //      Invoke(new Action(() => buttonSomething.Enabled = value));
        public static void Invoke(this Control Control, Action Action)
        {
            Control.Invoke(Action);
        }

        public static void CallOrInvokeIfRequired(this Control control, Action action)
        {
            if (control.InvokeRequired)
            {
                control.Invoke(action);
            }
            else
            {
                action();
            }
        }
    }
}
