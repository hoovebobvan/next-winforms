﻿
namespace Kit
{
    public interface ICommand
    {
        void ExecuteAsync();
    }

    public interface ICommandAction<Tin>
    {
        void Execute(Tin arg);
    }

    public interface ICommand<TOut>
    {
        TOut Execute();
    }

    public interface ICommand<TOut, Tin>
    {
        TOut Execute(Tin arg);
    }

    public interface ICommand<TOut, T1in, T2in>
    {
        TOut Execute(T1in arg1, T2in arg2);
    }

}
