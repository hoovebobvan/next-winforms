﻿using Kit.Expressions;
using System;
using System.Linq.Expressions;
using System.Windows.Forms;

namespace Kit
{
    public static class BindingFactory
    {
        public static Binding Create<TSet, TGet>(
            Expression<Func<TSet, object>> propSetter,
            object dataSource,
            Expression<Func<TGet, object>> propGetter) 
        {
            return new Binding(propSetter.GetMemberName(), dataSource, propSetter.GetMemberName());
        }

        public static Binding Create<TSet, TGet>(
            Expression<Func<TSet, object>> propSetter,
            object dataSource,
            Expression<Func<TGet, object>> propGetter,
            bool formattingEnabled) 
        {
            return new Binding(propSetter.GetMemberName(), dataSource, propSetter.GetMemberName(), formattingEnabled);
        }

        public static Binding Create<TSet, TGet>(
            Expression<Func<TSet, object>> propSetter,
            object dataSource,
            Expression<Func<TGet, object>> propGetter,
            bool formattingEnabled,
            DataSourceUpdateMode dataSourceUpdateMode)            
        {
            return new Binding(propSetter.GetMemberName(), dataSource, propSetter.GetMemberName(), formattingEnabled, dataSourceUpdateMode);
        }

    }
}
