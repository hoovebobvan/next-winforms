﻿using System;
using System.IO;
using System.Reactive.Linq;

namespace Kit
{
    public class RequestFolderInteraction : IDisposable, IObservable<DirectoryInfo>
    {
        RequestFolderCommand requestFolderCommand;
        IObservable<DirectoryInfo> observableResource;

        public RequestFolderInteraction(RequestFolderCommand requestFolderCommand)
        {
            if (requestFolderCommand == null) throw new ArgumentNullException();
            this.requestFolderCommand = requestFolderCommand;

            observableResource = Observable.FromEvent<DirectoryInfo>(
                h => requestFolderCommand.Selected += h,
                h => requestFolderCommand.Selected -= h);                 
        }

        public void Invoke()
        {
            requestFolderCommand.Execute();
        }

        public void Dispose()
        {
            observableResource = null;
            this.requestFolderCommand = null;
        }

        public IDisposable Subscribe(IObserver<DirectoryInfo> observer)
        {
            return observableResource.Subscribe(observer);
        }
    }
}
