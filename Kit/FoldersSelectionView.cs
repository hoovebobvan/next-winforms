﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace Kit
{
    public partial class FoldersSelectionView : UserControl
    {
        private SortableBindingList<DirectoryInfo> items = new SortableBindingList<DirectoryInfo>();

        public FoldersSelectionView()
        {
            InitializeComponent();
            dataGridView1.DataSource = items;
            items.ListChanged += (e, a) => this.ListChanged.Raise();
        }

        [EditorBrowsable(EditorBrowsableState.Always)]
        [Browsable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        [Bindable(true)]
        public int ButtonMargin
        {
            get
            {
                return this.pfR_Button1.Margin.Left;
            }
            set
            {
                var targetMargin = this.pfR_Button1.Margin;
                targetMargin.Left = value;
                this.pfR_Button1.Margin = targetMargin;
            }
        }


        public event Action ListChanged;

        public void AddFolder(DirectoryInfo directoryInfo)
        {
            items.Add(directoryInfo);
        }

        public event Action OnUserAddFolder;
        public event Action<DirectoryInfo> OnUserDeleteFolder;

        private void pfR_Button1_Click(object sender, EventArgs e)
        {
            OnUserAddFolder.Raise();
        }

        public IEnumerable<DirectoryInfo> Folders { get { return items.AsEnumerable(); } }
    }
}
