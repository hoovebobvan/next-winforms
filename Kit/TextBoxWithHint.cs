﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace Kit
{
    public class TextBoxWithHint : TextBox
    {
        public static readonly char enterKeyChar = (Char)Keys.Enter;

        public static Color DefaultHintColor = Color.LightGray;

        [EditorBrowsable(EditorBrowsableState.Always)]
        [Browsable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        [Bindable(true)]
        public string Hint
        {
            get;
            set;
        }

        private bool _hintEnabled = true;

        // TODO : Rename to HintEnabled
        [EditorBrowsable(EditorBrowsableState.Always)]
        [Browsable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        [Bindable(true)]
        public bool HintEnabled
        {
            get { return _hintEnabled; }
            set
            {
                if (value != _hintEnabled)
                {
                    if (value && !_hintEnabled)
                        _foreColorBackup = ForeColor;

                    _hintEnabled = value;
                }
            }
        }

        bool _hintVisible;

        [EditorBrowsable(EditorBrowsableState.Always)]
        [Browsable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        [Bindable(true)]
        public bool HintVisible
        {
            get { return _hintVisible; }
            set 
            {
                _hintVisible = value;
                if (HintEnabled)
                {
                    ForeColor = _hintVisible ? hintColor : _foreColorBackup;
                    Text = _hintVisible ? Hint : "";

                    this.Invalidate();
                }
            }
        }

        private Color _foreColorBackup;

        // nodig?
        public override string Text
        {
            get             
            {
                return base.Text;
            }
            set 
            {
                base.Text = value; 
            }
        }

        public string ActualText { get { return HintVisible ? "" : Text; } }

        public override Color ForeColor
        {
            get { return base.ForeColor; }
            set
            {
                base.ForeColor = value;
            }
        }

        private Color textColor = DefaultForeColor;

        [EditorBrowsable(EditorBrowsableState.Always)]
        [Browsable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        [Bindable(true)]
        public Color TextColor
        {
            get { return this.textColor; }
            set { this.textColor = value; }
        }


        private Color hintColor = DefaultHintColor;

        [EditorBrowsable(EditorBrowsableState.Always)]
        [Browsable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        [Bindable(true)]
        public Color HintColor 
        {
            get { return this.hintColor; }
            set 
            {
                this.hintColor = value;
                this.RefreshValue();
            }
        }

        protected override void OnEnter(EventArgs a)
        {
            HintVisible = false;
            base.OnEnter(a);
        }

        protected override void OnLeave(EventArgs e)
        {
            base.OnLeave(e);
            
            RefreshValue();

            if (String.IsNullOrWhiteSpace(Text))
                HintVisible = true;
        }

        protected override void OnKeyPress(KeyPressEventArgs e)
        {
            base.OnKeyPress(e);

            if (e.KeyChar == enterKeyChar)
            {
                RefreshValue();
            }
        }

        private void RefreshValue()
        {
            ValueChanged.Raise(Text);
        }

        public event Action<string> ValueChanged;
    }
}
