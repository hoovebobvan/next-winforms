﻿using System;

namespace Kit
{
    public interface IViewDecorator<T> : IDisposable
    {
        void Attach(T target);
        void Detach(T target);
    }
}
