﻿using System.Collections.Generic;
using System.Xml;

namespace Kit.XML
{
    public static class XmlExtension
    {
        public static XmlElement FindElement(this XmlElement xmlElement, string nodeName)
        {
            var nodes = xmlElement.GetElementsByTagName(nodeName);
            return nodes.Count >= 1 ? nodes[0] as XmlElement : null;
        }

        public static string GetTagValue(this XmlElement document, string tagName)
        {
            var candidateNodes = document.GetElementsByTagName(tagName);
            if (candidateNodes.Count > 0)
            {
                var candidateNode = candidateNodes[0];
                if (candidateNode.FirstChild != null)
                    return candidateNode.FirstChild.Value;
            }
            return null;
        }

        public static IEnumerable<string> GetTagValues(this XmlElement document, string tagName)
        {
            var candidateNodes = document.GetElementsByTagName(tagName);
            foreach(var candidate in candidateNodes)
            {
                var candidateNode = candidateNodes[0];
                if (candidateNode.FirstChild != null)
                    yield return candidateNode.FirstChild.Value;
            }
        }

    }
}
