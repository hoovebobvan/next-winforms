﻿using System.IO;
using System.Windows.Forms;

namespace Kit
{
    public class RequestFileCommand 
    {
        private string title;
        private string extensionFilter;

        public RequestFileCommand(string title, string extensionFilter = "")
        {
            this.title = title;
            this.extensionFilter = extensionFilter;
        }

        public FileInfo ExecuteDialog()
        {
            var dialog = new OpenFileDialog();
            dialog.Title = title;
            dialog.CheckPathExists = true;
            dialog.CheckFileExists = false;
            dialog.ReadOnlyChecked = true;
            dialog.Filter = extensionFilter;

            FileInfo fileInfo = null;
            if (DialogResult.OK == dialog.ShowDialog())
            {
                try
                {
                    fileInfo = new FileInfo(dialog.FileName);
                }
                catch { /* TODO */ }
            }

            return fileInfo;
        }
    }
}
