﻿using System;
using System.Collections.Generic;

namespace Kit
{
    public class CompositeViewDecorator<T> : IViewDecorator<T>, IEnumerable<IViewDecorator<T>>
    {
        private List<IViewDecorator<T>> decorators = new List<IViewDecorator<T>>();

        public void Add(IViewDecorator<T> item)
        {
            decorators.Add(item);
        }

        public void Add(Action<T> attach, Action<T> detach)
        {
            decorators.Add(new ViewDecorator<T>(attach, detach));
        }

        public void Attach(T view)
        {
            foreach (var decorator in decorators)
                decorator.Attach(view);
        }

        public void Detach(T view)
        {
            foreach (var decorator in decorators)
                decorator.Detach(view);
        }

        public void Dispose()
        {
            foreach (var decorator in decorators)
                decorator.Dispose();
            
            decorators.Clear();
        }

        public IEnumerator<IViewDecorator<T>> GetEnumerator()
        {
            return decorators.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return decorators.GetEnumerator();
        }
    }
}
