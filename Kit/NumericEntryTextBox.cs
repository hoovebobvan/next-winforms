﻿using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace Kit
{
    public class NumericEntryTextBox : TextBox
    {
        public static readonly char backspaceKeyChar = (Char)Keys.Back;
        public static readonly char enterKeyChar = (Char)Keys.Enter;

        protected override void OnKeyPress(KeyPressEventArgs e)
        {
            base.OnKeyPress(e);

            if (!IsKeyCharAllowed(e.KeyChar))
            {
                e.Handled = true;
            }

            if (e.KeyChar == enterKeyChar)
            {
                RefreshValue();
            }
        }

        private bool IsKeyCharAllowed(Char input)
        {
            return false // unless
                || Char.IsDigit(input)
                || input == backspaceKeyChar
                || input == enterKeyChar;
        }

        protected override void OnLeave(EventArgs e)
        {
            base.OnLeave(e);
            RefreshValue();
        }

        protected override void OnCreateControl()
        {
            base.OnCreateControl();
            this.Text = DefaultValue.ToString();
        }

        int defaultValue = 0;

        [EditorBrowsable(EditorBrowsableState.Always)]
        [Browsable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        [Bindable(true)]
        public int DefaultValue
        {
            get { return defaultValue; }
            set
            {
                defaultValue = value;
                Invalidate();
            }
        }

        private int value;

        private void RefreshValue()
        {
            int oldValue = value;

            if (!int.TryParse(Text, out value))
            {
                value = DefaultValue;
                this.Text = DefaultValue.ToString();
            }

            if (oldValue != value)
            {
                ValueChanged.Raise(value);
            }
        }

        public event Action<int> ValueChanged;
    }
}
