﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reactive.Linq;

namespace Kit
{
    public class FoldersSelectionInteraction : IDisposable, IObservable<IEnumerable<DirectoryInfo>>
    {
        private FoldersSelectionView view;
        private RequestFolderInteraction requestFolderInteraction;

        public FoldersSelectionInteraction(
            FoldersSelectionView view, 
            RequestFolderInteraction requestFolderInteraction, 
            IEnumerable<DirectoryInfo> musicFolders)
        {
            if (view == null) throw new ArgumentNullException();
            if (requestFolderInteraction == null) throw new ArgumentNullException();

            this.view = view;
            this.requestFolderInteraction = requestFolderInteraction;

            if (musicFolders != null)
            {
                foreach(var folder in musicFolders)
                {
                    view.AddFolder(folder);
                }
            }

            this.foldersResource = Observable
            .FromEvent(
                h => view.ListChanged += h,
                h => view.ListChanged -= h
            )
            .Select(_ => view.Folders);

            Attach();
        }

        private void Attach()
        {
            view.OnUserAddFolder += OnAddFolder;
        }

        private void Detach()
        {
            view.OnUserAddFolder -= OnAddFolder;
        }

        private void OnAddFolder()
        {
            using (var folderSubscription = requestFolderInteraction.Subscribe(AddFolder))
            {
                requestFolderInteraction.Invoke();
            }
        }

        private void AddFolder(DirectoryInfo folderInfo)
        {
            if (!view.Folders.Contains(folderInfo))
            {
                view.AddFolder(folderInfo);
            }
        }

        private IObservable<IEnumerable<DirectoryInfo>> foldersResource;

        public void Dispose()
        {
            Detach();
        }

        public IDisposable Subscribe(IObserver<IEnumerable<DirectoryInfo>> observer)
        {
            return foldersResource.Subscribe(observer);
        }
    }
}
