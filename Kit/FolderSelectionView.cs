﻿using System.IO;

namespace Kit
{
    public class FolderSelectionView : SelectionView
    {
        public FolderSelectionView()
            : base()
        {
        }

        public DirectoryInfo DirectoryInfo
        {
            set
            {
                this.Text = value == null ? "" : value.FullName;
            }
        }
    }
}
