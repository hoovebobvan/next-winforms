﻿using System.Drawing;
using System.Windows.Forms;

namespace Kit
{
    public class AutoSizeButton : Button
    {
        public AutoSizeButton()
            : base()
        {
            this.AutoSize = true;
            this.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            CreateControl();
        }

        public override Size MinimumSize
        {
            get
            {
                return new Size(5 * Font.Height, base.MinimumSize.Height);
            }
        }
    }
}
