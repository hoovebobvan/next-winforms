﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace Kit
{
    public class PFR_Checkbox : CheckBox
    {
        private float fontSizeFactor = 1.0F;

        public PFR_Checkbox() 
            : base()
        { }

        protected override void OnParentChanged(EventArgs e)
        {
            base.OnParentChanged(e);
            ScaleToParentFont();
        }

        protected override void OnParentFontChanged(EventArgs e)
        {
            base.OnParentFontChanged(e);
            ScaleToParentFont();
        }

        protected override void OnControlAdded(ControlEventArgs e)
        {
            base.OnControlAdded(e);
            ScaleToParentFont();
        }

        [Browsable(false)]
        [Bindable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public override Font Font
        {
            get
            {
                return base.Font;
            }
            set
            {
                base.Font = value;
            }
        }

        [EditorBrowsable(EditorBrowsableState.Always)]
        [Browsable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        [Bindable(true)]
        public float FontSizeFactor
        {
            get { return fontSizeFactor; }
            set
            {
                fontSizeFactor = value;
                ScaleToParentFont();
            }
        }

        private void ScaleToParentFont()
        {
            if (Parent != null)
            {
                var parentFont = Parent.Font;
                Font = new Font(parentFont.Name, fontSizeFactor * parentFont.SizeInPoints);
            }
        }

    }
}
