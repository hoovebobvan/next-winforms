﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Kit
{
    // ref 2011-02 : http://www.codeproject.com/KB/linq/bindinglist_sortable.aspx?msg=3752895#xx3752895xx

    public class SortableBindingList<T> : BindingList<T>
    {

        // reference to the list provided at the time of instantiation        
        List<T> originalList;
        ListSortDirection sortDirection;
        PropertyDescriptor sortProperty;

        // function that refereshes the contents        
        // of the base classes collection of elements        
        Action<SortableBindingList<T>, List<T>>
            populateBaseList = (a, b) => a.ResetItems(b);

        /// <summary>        
        /// Compares two objects based on a specific propery value        
        /// </summary>        
        class PropertyCompare : IComparer<T>
        {

            PropertyDescriptor _property;           // The propery value            

            ListSortDirection _direction;           // The direction of compare ascendinf or descending   
            public PropertyCompare(PropertyDescriptor property, ListSortDirection direction)
            {
                _property = property;
                _direction = direction;
            }

            public int Compare(T comp1, T comp2)
            {
                var value1 = _property.GetValue(comp1) as IComparable;
                var value2 = _property.GetValue(comp2) as IComparable;

                // begin: 2011-03 bvh : values may be null, per default we'll make those the smallest
                if (value1 == null && value2 == null)
                {
                    return 0;
                }
                else if (value1 == null)
                {
                    return -1;
                }
                else if (value2 == null)
                {
                    return 1;
                }
                // end 2011-03 bvh

                else if (_direction == ListSortDirection.Ascending)
                {
                    return value1.CompareTo(value2);
                }
                else
                {
                    return value2.CompareTo(value1);
                }
            }
        }

        public SortableBindingList()
        {
            originalList = new List<T>();
        }

        public SortableBindingList(IEnumerable<T> enumerable)
        {
            originalList = new List<T>(enumerable);
            populateBaseList(this, originalList);
        }

        public SortableBindingList(List<T> list)
        {
            originalList = list;
            populateBaseList(this, originalList);
        }

        protected override void ApplySortCore(PropertyDescriptor prop,
            ListSortDirection direction)
        {
            PropertyCompare comp = new PropertyCompare(prop, sortDirection);
            List<T> sortedList = new List<T>(this);
            sortedList.Sort(comp);
            ResetItems(sortedList);
            ResetBindings();
            sortDirection = sortDirection == ListSortDirection.Ascending ?
                    ListSortDirection.Descending
                : ListSortDirection.Ascending;
        }

        protected override void RemoveSortCore()
        {
            ResetItems(originalList);
        }

        private void ResetItems(List<T> items)
        {
            base.ClearItems();
            for (int i = 0; i < items.Count; i++)
            {
                base.InsertItem(i, items[i]);
            }
        }

        protected override bool SupportsSortingCore
        {
            get
            {
                // indeed we do      
                return true;
            }
        }

        protected override ListSortDirection SortDirectionCore
        {
            get
            {
                return sortDirection;
            }
        }

        protected override PropertyDescriptor SortPropertyCore
        {
            get
            {
                return sortProperty;
            }
        }

        protected override void OnListChanged(ListChangedEventArgs e)
        {
            base.OnListChanged(e);
            originalList = new List<T>(base.Items);
        }
    }
}
