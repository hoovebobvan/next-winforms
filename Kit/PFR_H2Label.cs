﻿
namespace Kit
{
    [System.ComponentModel.DesignerCategory("")]
    public class PFR_H2Label : PFR_Label
    {
        private static float h2FontSizeFactor = 1.125F;

        public PFR_H2Label() : base(h2FontSizeFactor)
        { }
    }
}
