﻿using System;

namespace Kit
{
    public class ViewDecorator<T> : IViewDecorator<T>
    {
        private Action<T> attach;
        private Action<T> detach;

        public ViewDecorator(Action<T> attach, Action<T> detach)
        {
            if (attach == null || detach == null) throw new ArgumentNullException();
            this.attach = attach;
            this.detach = detach;
        }

        public void Attach(T view)
        {
            attach(view);
        }

        public void Detach(T view)
        {
            detach(view);
        }

        public void Dispose()
        {
            attach = null;
            detach = null;
        }
    }
}
