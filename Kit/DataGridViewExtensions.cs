﻿using System.Windows.Forms;

namespace Kit
{
    public static class DataGridViewExtensions
    {
        public static void HideSelectionColor(this DataGridView dataGridView1)
        {
            // assign 'regular' colors
            dataGridView1.DefaultCellStyle.SelectionForeColor = dataGridView1.DefaultCellStyle.ForeColor;
            dataGridView1.DefaultCellStyle.SelectionBackColor = dataGridView1.DefaultCellStyle.BackColor;
        }

        public static void HideSelectionColorUnlessClicked(this DataGridView dataGridView1)
        {
            DataGridViewCellEventHandler onCellClick = null;

            // remember selection colors
            var selectionBackColor = dataGridView1.DefaultCellStyle.SelectionBackColor;
            var selectionForeColor = dataGridView1.DefaultCellStyle.SelectionForeColor;

            // assign 'regular' colors
            dataGridView1.DefaultCellStyle.SelectionForeColor = dataGridView1.DefaultCellStyle.ForeColor;
            dataGridView1.DefaultCellStyle.SelectionBackColor = dataGridView1.DefaultCellStyle.BackColor;

            // undo all this on the first cell click, and detach handler
            onCellClick = (sender, args) =>
            {
                dataGridView1.DefaultCellStyle.SelectionBackColor = selectionBackColor;
                dataGridView1.DefaultCellStyle.SelectionForeColor = selectionForeColor;
                dataGridView1.CellClick -= onCellClick;
            };

            dataGridView1.CellClick += onCellClick;
        }
    }
}
