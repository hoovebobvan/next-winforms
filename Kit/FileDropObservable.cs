﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reactive.Subjects;
using System.Windows.Forms;

namespace Kit
{
    // TODO : Check if this can still be put to use

    public class FileDropObservable : IObservable<IEnumerable<FileInfo>>, IDisposable
    {
        private Subject<IEnumerable<FileInfo>> subject = new Subject<IEnumerable<FileInfo>>();

        public void AttachOnLoad(Form control, Action attach)
        {
            EventHandler formLoad = null;
            formLoad = (s, a) =>
                {
                    attach();
                    control.Load -= formLoad;
                };
            control.Load += formLoad;
        }

        public void Attach(Form control)
        {
            control.DragDrop += OnDragDrop;
            control.DragEnter += OnDragEnter;
        }

        public void Detach(Form control)
        {
            control.DragDrop -= OnDragDrop;
            control.DragEnter -= OnDragEnter;
        }

        public void OnDragDrop(object sender, DragEventArgs e)
        {
            var dropped = new List<FileInfo>();

            try
            {
                string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
                if (files != null)
                {
                    foreach (var path in files)
                    {
                        var fileInfo = new FileInfo(path);
                        if (fileInfo.Exists)
                            dropped.Add(fileInfo);
                    }
                }
            }
            catch
            { }

            if (dropped.Any())
            {
                subject.OnNext(dropped);
            }
        }

        void OnDragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop)) e.Effect = DragDropEffects.All;
        }

        public IDisposable Subscribe(IObserver<IEnumerable<FileInfo>> observer)
        {
            return subject.Subscribe(observer);
        }

        public void Dispose()
        {
            subject.OnCompleted();
            subject = null;
        }
    }
}

