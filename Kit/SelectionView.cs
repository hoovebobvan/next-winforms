﻿using System;
using System.Windows.Forms;

namespace Kit
{
    public class SelectionView : UserControl
    {
        private PFR_TextBox pfR_TextBox1;
        private TableLayoutPanel tableLayoutPanel1;
        private AutoSizeButton pfR_Button1;

        public SelectionView()
        {
            InitializeComponent();
        }

        public event Action UserSelect;

        public override string Text
        {
            set { this.pfR_TextBox1.Text = value ?? ""; }
        }

        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.pfR_Button1 = new Kit.AutoSizeButton();
            this.pfR_TextBox1 = new Kit.PFR_TextBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.pfR_Button1, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.pfR_TextBox1, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(452, 35);
            this.tableLayoutPanel1.TabIndex = 3;
            // 
            // pfR_Button1
            // 
            this.pfR_Button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pfR_Button1.AutoSize = true;
            this.pfR_Button1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.pfR_Button1.Location = new System.Drawing.Point(374, 3);
            this.pfR_Button1.MinimumSize = new System.Drawing.Size(75, 0);
            this.pfR_Button1.Name = "pfR_Button1";
            this.pfR_Button1.Size = new System.Drawing.Size(75, 27);
            this.pfR_Button1.TabIndex = 1;
            this.pfR_Button1.Text = "select";
            this.pfR_Button1.UseVisualStyleBackColor = true;
            this.pfR_Button1.Click += new System.EventHandler(this.pfR_Button1_Click);
            // 
            // pfR_TextBox1
            // 
            this.pfR_TextBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pfR_TextBox1.FontSizeFactor = 1F;
            this.pfR_TextBox1.Location = new System.Drawing.Point(2, 6);
            this.pfR_TextBox1.Margin = new System.Windows.Forms.Padding(2, 6, 15, 15);
            this.pfR_TextBox1.Name = "pfR_TextBox1";
            this.pfR_TextBox1.Size = new System.Drawing.Size(354, 22);
            this.pfR_TextBox1.TabIndex = 2;
            // 
            // SelectionView
            // 
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "SelectionView";
            this.Size = new System.Drawing.Size(458, 41);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        private void pfR_Button1_Click(object sender, EventArgs e)
        {
            UserSelect.Raise();
        }
    }
}
