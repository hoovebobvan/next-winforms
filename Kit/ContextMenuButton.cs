﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Kit
{
    public class ContextMenuButton : AutoSizeButton
    {
        protected override void OnClick(EventArgs e)
        {
            base.OnClick(e);
            ContextMenuStrip.Show(this, new Point(0, Height), ToolStripDropDownDirection.BelowRight);
        }
    }
}