﻿using System;

namespace Kit
{
    public static class EventExtensions
    {
        /// <summary>
        /// These methods help avoiding null reference exceptions (1)
        /// as well as a threading issue where the list of handlers may
        /// change while you're emitting an event (2)
        /// 
        /// In the latter case, the event handler is responsible for correctly
        /// handling events raised while unsubscribing. 
        /// 
        /// (1) ref 2011-04: http://blogs.msdn.com/b/ericlippert/archive/2009/04/29/events-and-races.aspx
        /// (2) ref 2011-04: http://stackoverflow.com/questions/786383/c-events-and-thread-safety
        /// </summary>
        public static void Raise<T>(this EventHandler<T> handler, object sender, T args)
            where T : EventArgs
        {
            EventHandler<T> handlerCopy = handler;
            if (handlerCopy != null) handlerCopy(sender, args);
        }

        public static void Raise(this EventHandler handler, object sender, EventArgs args)
        {
            EventHandler handlerCopy = handler;
            if (handlerCopy != null) handlerCopy(sender, args);
        }

        // The extensions on Action are provided for 'event Action' fields
        public static void Raise(this Action action)
        {
            Action actionCopy = action;
            if (actionCopy != null) actionCopy();
        }

        public static void Raise<T>(this Action<T> action, T arg)
        {
            Action<T> actionCopy = action;
            if (actionCopy != null) actionCopy(arg);
        }

        public static void Raise<T, U>(this Action<T, U> action, T tArg, U uArg)
        {
            Action<T, U> actionCopy = action;
            if (actionCopy != null) actionCopy(tArg, uArg);
        }
    }
}
