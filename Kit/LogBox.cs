﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Kit
{
    public class LogBox : RichTextBox
    {
        public LogBox()
        {
            Font = new Font(FontFamily.GenericMonospace, this.Font.Size);
            this.ReadOnly = true;
        }
        
        public void Log(string message)
        {
            Log(message, this.ForeColor);
        }

        public void LogMany(IEnumerable<string> messages)
        {
            SuspendLayout();
            Log(string.Join(Environment.NewLine, messages));
            ResumeLayout(true);
        }

        public void Log(string message, Color color)
        {
            int startPosition = this.Text.Length;
            this.AppendText(message + Environment.NewLine);
            int endPosition = this.Text.Length;

            if (color != this.ForeColor)
            {
                this.SelectionStart = startPosition;
                this.SelectionLength = endPosition - startPosition;
                this.SelectionColor = color;
            }

            // Scroll along
            this.SelectionLength = 0;
            this.SelectionStart = this.Text.Length;
            this.ScrollToCaret();
        }
    }
}
