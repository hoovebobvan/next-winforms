﻿using System;
using System.IO;
using System.Reactive.Subjects;

namespace Kit
{
    public class FileSelectionInteraction : IObservable<FileInfo>, IDisposable
    {
        private RequestFileCommand requestFileCommand;
        private FileSelectionView view;
        private Subject<FileInfo> fileInfoSubject = new Subject<FileInfo>();

        public FileSelectionInteraction(FileSelectionView view, RequestFileCommand requestFileCommand)
        {
            this.view = view;
            this.requestFileCommand = requestFileCommand;

            view.UserSelect += OnUserSelect;
        }

        private void OnUserSelect()
        {
            var fileInfo = requestFileCommand.ExecuteDialog();
            if (fileInfo != null)
            {
                fileInfoSubject.OnNext(fileInfo);
            }
            view.FileInfo = fileInfo;
        }

        public IDisposable Subscribe(IObserver<FileInfo> observer)
        {
            return fileInfoSubject.Subscribe(observer);
        }

        public void Dispose()
        {
            view.UserSelect -= OnUserSelect;
            fileInfoSubject.OnCompleted();
            fileInfoSubject.Dispose();
        }
    }
}
