﻿using System.Windows.Forms;

namespace Kit
{
    public static class PutControlExtension
    {
        public static void PutControl(this Form form, Control control)
        {
            form.Controls.Clear();
            form.Controls.Add(control);
            form.ClientSize = control.Size;
            control.Dock = DockStyle.Fill;
        }
    }
}
