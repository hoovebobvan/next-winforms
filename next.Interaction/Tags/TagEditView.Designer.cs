﻿namespace next.Interaction.Tags
{
    partial class TagEditView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.textBoxName = new next.Interaction.Theme.ThemedTextBox();
            this.buttonSaveTags = new next.Interaction.Theme.ThemedButton();
            this.buttonSaveFanLikes = new next.Interaction.Theme.ThemedButton();
            this.trackListView1 = new next.Interaction.Tags.TrackListView();
            this.tableLayoutPanel1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 66.66666F));
            this.tableLayoutPanel1.Controls.Add(this.textBoxName, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanel1, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.trackListView1, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(422, 225);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoSize = true;
            this.flowLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.flowLayoutPanel1.Controls.Add(this.buttonSaveTags);
            this.flowLayoutPanel1.Controls.Add(this.buttonSaveFanLikes);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(143, 3);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(176, 31);
            this.flowLayoutPanel1.TabIndex = 1;
            // 
            // textBoxName
            // 
            this.textBoxName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.textBoxName.FontSizeFactor = 1F;
            this.textBoxName.Location = new System.Drawing.Point(3, 3);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(134, 20);
            this.textBoxName.TabIndex = 0;
            // 
            // buttonSaveTags
            // 
            this.buttonSaveTags.AutoSize = true;
            this.buttonSaveTags.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonSaveTags.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.buttonSaveTags.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gray;
            this.buttonSaveTags.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSaveTags.ForeColor = System.Drawing.Color.Silver;
            this.buttonSaveTags.Location = new System.Drawing.Point(3, 3);
            this.buttonSaveTags.MinimumSize = new System.Drawing.Size(65, 0);
            this.buttonSaveTags.Name = "buttonSaveTags";
            this.buttonSaveTags.Size = new System.Drawing.Size(71, 25);
            this.buttonSaveTags.TabIndex = 0;
            this.buttonSaveTags.Text = "Save Tags";
            this.buttonSaveTags.UseVisualStyleBackColor = true;
            this.buttonSaveTags.Click += new System.EventHandler(this.buttonSaveTags_Click);
            // 
            // buttonSaveFanLikes
            // 
            this.buttonSaveFanLikes.AutoSize = true;
            this.buttonSaveFanLikes.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonSaveFanLikes.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.buttonSaveFanLikes.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gray;
            this.buttonSaveFanLikes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSaveFanLikes.ForeColor = System.Drawing.Color.Silver;
            this.buttonSaveFanLikes.Location = new System.Drawing.Point(80, 3);
            this.buttonSaveFanLikes.MinimumSize = new System.Drawing.Size(65, 0);
            this.buttonSaveFanLikes.Name = "buttonSaveFanLikes";
            this.buttonSaveFanLikes.Size = new System.Drawing.Size(93, 25);
            this.buttonSaveFanLikes.TabIndex = 1;
            this.buttonSaveFanLikes.Text = "Save Fan Likes";
            this.buttonSaveFanLikes.UseVisualStyleBackColor = true;
            this.buttonSaveFanLikes.Click += new System.EventHandler(this.buttonSaveFanLikes_Click);
            // 
            // trackListView1
            // 
            this.trackListView1.AllowDrop = true;
            this.tableLayoutPanel1.SetColumnSpan(this.trackListView1, 2);
            this.trackListView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.trackListView1.Location = new System.Drawing.Point(3, 40);
            this.trackListView1.Name = "trackListView1";
            this.trackListView1.Size = new System.Drawing.Size(416, 182);
            this.trackListView1.TabIndex = 2;
            // 
            // TagEditView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "TagEditView";
            this.Size = new System.Drawing.Size(422, 225);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Theme.ThemedTextBox textBoxName;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private Theme.ThemedButton buttonSaveTags;
        private TrackListView trackListView1;
        private Theme.ThemedButton buttonSaveFanLikes;
    }
}
