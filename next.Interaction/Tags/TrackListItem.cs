﻿using next.Tracks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace next.Interaction.Tags
{
    public class TrackListItem : IComparable<TrackListItem>
    {
        private TrackFile track;

        public TrackListItem(TrackFile track)
        {
            this.track = track;
        }

        public string Artist_Title { get { return track.Artist_Title; } }
        public string Release { get { return track.ReleaseTitle; } }

        public TrackFile TrackFile { get { return track; } }

        public int CompareTo(TrackListItem obj)
        {
            return track.CompareTo(obj?.TrackFile);
        }

        public override bool Equals(object obj)
        {
            return track.Equals(obj);
        }

        public override int GetHashCode()
        {
            return track.GetHashCode();
        }
    }
}
