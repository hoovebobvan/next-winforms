﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using next.Store.TrackFiles;
using System.IO;
using next.Tracks;
using System.Windows.Forms;

namespace next.Interaction.Tags
{
    public class TagsInteraction : IDisposable
    {
        private TrackFileCollection collection;
        private TagEditView view;
        private TrackListView trackListView;

        public TagsInteraction(TrackFileCollection collection, TagEditView view)
        {
            this.collection = collection;
            this.view = view;
            this.trackListView = view.TrackListView;

            trackListView.FilesDropped += TrackListView_FilesDropped;
            view.SaveTagsRequest += View_SaveTagsRequest;
            view.SaveFanLikesRequest += View_SaveFanLikesRequest;
        }

        private void TrackListView_FilesDropped(List<string> paths)
        {
            var knownTracks = paths
                .Select(p => collection.GetByPath(p))
                .OfType<TrackFile>()
                .Select(t => new TrackListItem(t))
                .ToList();

            var trackSet = new SortedSet<TrackListItem>(knownTracks);

            trackListView.AddItems(trackSet);                        
        }

        private void View_SaveFanLikesRequest()
        {
            var tracks = trackListView.Items.Select(i => i.TrackFile).ToList();
            string name = view.TagName;

            if (string.IsNullOrEmpty(name))
            {
                MessageBox.Show("Name should not be empty");
            }
            else
            {
                foreach (var track in tracks)
                    track.AddLike(name);
            }

        }

        private void View_SaveTagsRequest()
        {
        }

        public void Dispose()
        {
            trackListView.FilesDropped -= TrackListView_FilesDropped;
            view.SaveTagsRequest -= View_SaveTagsRequest;
            view.SaveFanLikesRequest -= View_SaveFanLikesRequest;
        }
    }
}
