﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Kit;

namespace next.Interaction.Tags
{
    public partial class TrackListView : UserControl
    {
        private BindingList<TrackListItem> tracks = new BindingList<TrackListItem>();
        
        public TrackListView()
        {
            InitializeComponent();
            dataGridView1.HideSelectionColorUnlessClicked();

            dataGridView1.DataSource = tracks;

            this.DragEnter += OnDragEnter;
            this.DragDrop += OnDragDrop;
        }

        public event Action<List<string>> FilesDropped;

        public void AddItems(ISet<TrackListItem> items)
        {
            if (items != null && items.Any())
            {
                foreach (var track in tracks)
                {
                    items.Add(track);
                }

                tracks.Clear();

                foreach (var track in items)
                    tracks.Add(track);
            }
        }

        public IEnumerable<TrackListItem> Items { get { return tracks.AsEnumerable(); } }

        public void OnDragDrop(object sender, DragEventArgs e)
        {
            string[] files = null;

            try
            {
                files = (string[])e.Data.GetData(DataFormats.FileDrop);
            }
            catch
            { }

            if (files != null)
                FilesDropped.Raise(files.ToList());
        }

        void OnDragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop)) e.Effect = DragDropEffects.All;
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                DragEnter -= OnDragEnter;
                DragDrop -= OnDragDrop;
                components?.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
