﻿using next.Store.TrackFiles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kit;

namespace next.Interaction.Tags
{
    public class TagsInvoker
    {
        private TrackFileCollection collection;

        public TagsInvoker(TrackFileCollection collection)
        {
            this.collection = collection;
        }

        public void Invoke()
        {
            var form = new BaseForm();
            form.Text = "Next - Edit Tags (beta)";
            var view = new TagEditView();

            form.PutControl(view);

            var interaction = new TagsInteraction(collection, view);

            form.FormClosed += (s, a) => interaction.Dispose();
            form.Show();
        }
    }
}
