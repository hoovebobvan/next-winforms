﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using next.Tracks;
using Kit;

namespace next.Interaction.Tags
{
    public partial class TagEditView : UserControl
    {
        public TagEditView()
        {
            InitializeComponent();
        }

        public event Action SaveTagsRequest;
        public event Action SaveFanLikesRequest;

        private void buttonSaveTags_Click(object sender, EventArgs e)
        {
            SaveTagsRequest.Raise();
        }

        private void buttonSaveFanLikes_Click(object sender, EventArgs e)
        {
            SaveFanLikesRequest.Raise();
        }

        public TrackListView TrackListView { get { return trackListView1; } }

        public string TagName { get { return textBoxName.Text; } }
    }
}
