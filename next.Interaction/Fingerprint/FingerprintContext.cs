﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using next.Store.AcoustIndex;
using System.Threading.Tasks;
using System.IO;
using next.Store;
using next.Store.TrackFiles;

namespace next.Interaction.Fingerprint
{
    public class FingerprintContext
    {
        private Func<DirectoryInfo> getDataDirectory;

        public FingerprintCollection FingerprintCollection { get; private set; }
        public SaveFingerpintCollection SaveCollection { get; private set; }
        public LoadFingerprintCollection LoadCollection { get; private set; }
        public TrackFileCollection TrackFileCollection { get; set; }

        public async Task<bool> LoadAsync()
        {
            try
            {
                await LoadCollection.Execute(
                    TaskScheduler.Default,
                    new FingerprintCollection(),
                    getDataDirectory());

                return true;
            }
            catch
            {
                return false;
            }
        }

        public FingerprintContext(
            TrackFileCollection trackCollection,
            Func<DirectoryInfo> getDataDirectory,
            SaveFingerpintCollection saveCollection,
            LoadFingerprintCollection loadCollection)
        {
            this.getDataDirectory = getDataDirectory;
            this.TrackFileCollection = trackCollection;
            this.SaveCollection = saveCollection;
            this.LoadCollection = loadCollection;
        }


        public static FingerprintContext CreateDefault(TrackFileCollection trackCollection, Func<DirectoryInfo> getDataDirectory)
        {
            var loadCollection = new LoadFingerprintCollection();
            var saveCollection = new SaveFingerpintCollection();
            var context = new FingerprintContext(trackCollection, getDataDirectory, saveCollection, loadCollection);

            return context;
        }
    }
}
