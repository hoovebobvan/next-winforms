﻿namespace next.Interaction.Fingerprint
{
    partial class BatchFingerprintView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.themedH1Label1 = new next.Interaction.Theme.ThemedH1Label();
            this.themedLabel1 = new next.Interaction.Theme.ThemedLabel();
            this.themedLabel2 = new next.Interaction.Theme.ThemedLabel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.pauseContinueButton = new next.Interaction.Theme.ThemedButton();
            this.tableLayoutPanel1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.themedH1Label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.themedLabel1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.themedLabel2, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanel1, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(772, 512);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // themedH1Label1
            // 
            this.themedH1Label1.AutoSize = true;
            this.themedH1Label1.FontSizeFactor = 1.25F;
            this.themedH1Label1.ForeColor = System.Drawing.Color.Silver;
            this.themedH1Label1.Location = new System.Drawing.Point(3, 5);
            this.themedH1Label1.Margin = new System.Windows.Forms.Padding(3, 5, 3, 10);
            this.themedH1Label1.Name = "themedH1Label1";
            this.themedH1Label1.Size = new System.Drawing.Size(149, 20);
            this.themedH1Label1.TabIndex = 0;
            this.themedH1Label1.Text = "Batch fingerpinting";
            // 
            // themedLabel1
            // 
            this.themedLabel1.AutoSize = true;
            this.themedLabel1.FontSizeFactor = 1F;
            this.themedLabel1.ForeColor = System.Drawing.Color.Silver;
            this.themedLabel1.Location = new System.Drawing.Point(3, 46);
            this.themedLabel1.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.themedLabel1.Name = "themedLabel1";
            this.themedLabel1.Size = new System.Drawing.Size(48, 17);
            this.themedLabel1.TabIndex = 1;
            this.themedLabel1.Text = "queue";
            // 
            // themedLabel2
            // 
            this.themedLabel2.AutoSize = true;
            this.themedLabel2.FontSizeFactor = 1F;
            this.themedLabel2.ForeColor = System.Drawing.Color.Silver;
            this.themedLabel2.Location = new System.Drawing.Point(389, 46);
            this.themedLabel2.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.themedLabel2.Name = "themedLabel2";
            this.themedLabel2.Size = new System.Drawing.Size(40, 17);
            this.themedLabel2.TabIndex = 2;
            this.themedLabel2.Text = "done";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoSize = true;
            this.flowLayoutPanel1.Controls.Add(this.pauseContinueButton);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(389, 3);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(380, 35);
            this.flowLayoutPanel1.TabIndex = 3;
            // 
            // pauseContinueButton
            // 
            this.pauseContinueButton.AutoSize = true;
            this.pauseContinueButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.pauseContinueButton.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.pauseContinueButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gray;
            this.pauseContinueButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.pauseContinueButton.ForeColor = System.Drawing.Color.Silver;
            this.pauseContinueButton.Location = new System.Drawing.Point(302, 3);
            this.pauseContinueButton.MinimumSize = new System.Drawing.Size(75, 0);
            this.pauseContinueButton.Name = "pauseContinueButton";
            this.pauseContinueButton.Size = new System.Drawing.Size(75, 29);
            this.pauseContinueButton.TabIndex = 0;
            this.pauseContinueButton.Text = "Pause";
            this.pauseContinueButton.UseVisualStyleBackColor = true;
            // 
            // BatchFingerprintView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "BatchFingerprintView";
            this.Size = new System.Drawing.Size(772, 512);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Theme.ThemedH1Label themedH1Label1;
        private Theme.ThemedLabel themedLabel1;
        private Theme.ThemedLabel themedLabel2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private Theme.ThemedButton pauseContinueButton;
    }
}
