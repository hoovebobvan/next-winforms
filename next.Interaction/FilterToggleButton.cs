﻿using Kit;
using next.Interaction.Properties;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace next.Interaction
{
    public class FilterToggleButton : Panel
    {
        private Image onImage = Resources.right_arrow_on;
        private Image offImage = Resources.right_arrow_off;
        private bool state = false;


        public FilterToggleButton()
            : base()
        {
            this.BackgroundImageLayout = ImageLayout.Center;
            base.Size = new Size(onImage.Size.Width + Font.Height, onImage.Size.Height + (int)Math.Round(Font.Size / 2));
            this.Text = "";
            this.State = false;
            this.BackgroundImage = offImage;
            this.Invalidate();
            this.Click += new EventHandler(FilterToggleButton_Click);
        }

        void FilterToggleButton_Click(object sender, EventArgs e)
        {
            this.State = !this.State;
        }

        public bool State
        {
            set
            {
                if (this.state != value)
                {
                    this.state = value;
                    this.BackgroundImage = value ? onImage : offImage;
                    StateChanged.Raise(value);
                }
            }
            get
            {
                return this.state;
            }
        }

        protected override Padding DefaultMargin
        {
            get { return new Padding(0, 0, 3, DefaultFont.Height); }
        }

        public event Action<bool> StateChanged;

    }
}
