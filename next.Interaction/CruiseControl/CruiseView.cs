﻿using Kit;
using next.Session;
using next.Tracks;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;

namespace next.Interaction.CruiseControl
{
    public partial class CruiseView : UserControl
    {
        private BindingSource sessionBindingSource;

        public CruiseView()
        {
            InitializeComponent();

            SetupBinding();
        }

        private void SetupBinding()
        {
            sessionBindingSource = new BindingSource();
            sessionBindingSource.DataSource = typeof(CruiseSession);
            currentBPMControl1.SetupBinding(sessionBindingSource);
            masterTrackView1.SetupBinding(sessionBindingSource);
            favorNewCheckbox.CheckedChanged += favorNewCheckbox_CheckedChanged;
            favorLikesCheckBox.CheckedChanged += FavorLikesCheckBox_CheckedChanged;
            ttNeighborCheckbox.CheckedChanged += TtNeighborCheckbox_CheckedChanged;
            
        }

        // TODO : Inject this 
        public void AddBinding(CruiseSession session)
        {
            sessionBindingSource.Add(session);
        }

        public BindingList<SessionTrack> PlaylistBindingSource
        {
            set { this.playListControl1.DataSource = value; }
        }

        public TrackFile MasterTrack 
        {
            get { return this.masterTrackView1.MasterTrack; }
            set { this.masterTrackView1.MasterTrack = value; }
        }

        public IEnumerable<MatchingTrackViewModel> MatchingTracks
        {
            set
            {
                this.trackListView1.Matches = value;
            }
        }

        public event Action UserRequestsBPMFromMaster;

        private void buttonBPMFromMaster_Click(object sender, EventArgs e)
        {
            UserRequestsBPMFromMaster.Raise();
        }

        public EnterBPMControl EnterBPMControl { get { return this.enterBPMControl1; } }

        public MasterTrackView MasterTrackView { get { return this.masterTrackView1; } }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                sessionBindingSource.Dispose();
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        public event Action N100CheckChanged;

        private void n100CheckBox_CheckedChanged(object sender, EventArgs e)
        {
            N100CheckChanged.Raise();
        }

        public event Action TTNeighborCheckChanged;

        private void TtNeighborCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            if (N100Check)
                TTNeighborCheckChanged.Raise();
        }

        public event Action FavorNewCheckChanged;
        
        void favorNewCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            FavorNewCheckChanged.Raise();
        }

        public event Action FavorLikesChanged;

        private void FavorLikesCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            FavorLikesChanged.Raise();
        }

        public bool FavorNewCheck { get { return favorNewCheckbox.Checked; } }

        public bool FavorLikes { get { return favorLikesCheckBox.Checked; } }

        public bool N100Check { get { return n100CheckBox.Checked; } }

        public bool TTNeighborCheck { get { return N100Check && ttNeighborCheckbox.Checked; } }

    }
}
