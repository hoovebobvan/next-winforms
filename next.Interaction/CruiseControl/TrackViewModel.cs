﻿using next.Tonality;
using next.Tracks;
using System;
using System.IO;

namespace next.Interaction.CruiseControl
{
    public class TrackViewModel
    {
        TrackFile track;
        private string tonality;

        public TrackViewModel(TrackFile track)
        {
            if (track == null) throw new ArgumentNullException();
            this.track = track;

            if (track.N100Key != null)
                tonality = track.N100Key.Tonality.ToString();
        }

        public string Tonality { get { return tonality; } }
        public double BPM { get { return track.BPM; } }
        public DMFloatKey N100Key { get { return track.N100Key; } }
        public string TrackName { get { return track.Artist_Title; } }
        public string Release { get { return track.ReleaseTitle; } }
        public string OpenKeyCode { get { return track.OpenKeyCode; } }
        public FileInfo FileInfo { get { return track.FileInfo; } }
        public TrackFile SourceTrack { get { return track; } }
    }
}
