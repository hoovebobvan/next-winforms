﻿namespace next.Interaction.CruiseControl
{
    partial class MatchingTracksView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.matchingTrackViewModelBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.scoreDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tonalStepTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bPMDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.n100KeyDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.deltaCentimeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.openKeyCodeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.artistTitleDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.releaseTitleDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IsLovedTrackColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fileInfoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.playedBeforeDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ButtonYayColumn = new System.Windows.Forms.DataGridViewButtonColumn();
            this.NayButtonColumn = new System.Windows.Forms.DataGridViewButtonColumn();
            this.n100PhasePerCentDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.matchingTrackViewModelBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowDrop = true;
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            this.dataGridView1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.Gray;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle2.Padding = new System.Windows.Forms.Padding(0, 0, 0, 2);
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.scoreDataGridViewTextBoxColumn,
            this.tonalStepTextBoxColumn,
            this.bPMDataGridViewTextBoxColumn,
            this.n100KeyDataGridViewTextBoxColumn,
            this.deltaCentimeDataGridViewTextBoxColumn,
            this.openKeyCodeDataGridViewTextBoxColumn,
            this.artistTitleDataGridViewTextBoxColumn,
            this.releaseTitleDataGridViewTextBoxColumn,
            this.IsLovedTrackColumn,
            this.fileInfoDataGridViewTextBoxColumn,
            this.playedBeforeDataGridViewCheckBoxColumn,
            this.ButtonYayColumn,
            this.NayButtonColumn,
            this.n100PhasePerCentDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.matchingTrackViewModelBindingSource;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.EnableHeadersVisualStyles = false;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(829, 277);
            this.dataGridView1.TabIndex = 0;
            // 
            // matchingTrackViewModelBindingSource
            // 
            this.matchingTrackViewModelBindingSource.DataSource = typeof(next.Interaction.CruiseControl.MatchingTrackViewModel);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn1.DataPropertyName = "DeltaCentime";
            this.dataGridViewTextBoxColumn1.HeaderText = "d_ct";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.ToolTipText = "Centime offset";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn2.DataPropertyName = "N100Key";
            this.dataGridViewTextBoxColumn2.HeaderText = "n100";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.ToolTipText = "key normalized to 100 bpm";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn3.DataPropertyName = "OpenKeyCode";
            this.dataGridViewTextBoxColumn3.HeaderText = "okc";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.ToolTipText = "Open Keycode (Traktor)";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn4.DataPropertyName = "YayNay";
            this.dataGridViewTextBoxColumn4.HeaderText = "y/n";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.ToolTipText = "Yay or Nay";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "N100PhasePerCent";
            this.dataGridViewTextBoxColumn5.HeaderText = "N100PhasePerCent";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Visible = false;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.dataGridViewTextBoxColumn6.DataPropertyName = "DeltaCentime";
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn6.DefaultCellStyle = dataGridViewCellStyle11;
            this.dataGridViewTextBoxColumn6.HeaderText = "dct";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.ToolTipText = "Centime offset";
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn7.DataPropertyName = "N100Key";
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle12.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.dataGridViewTextBoxColumn7.DefaultCellStyle = dataGridViewCellStyle12;
            this.dataGridViewTextBoxColumn7.HeaderText = "n100";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.ToolTipText = "key normalized to 100 bpm";
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn8.DataPropertyName = "OpenKeyCode";
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle13.Padding = new System.Windows.Forms.Padding(5, 0, 10, 0);
            this.dataGridViewTextBoxColumn8.DefaultCellStyle = dataGridViewCellStyle13;
            this.dataGridViewTextBoxColumn8.HeaderText = "okc";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.ToolTipText = "Open Keycode (Traktor)";
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "N100PhasePerCent";
            this.dataGridViewTextBoxColumn9.HeaderText = "N100PhasePerCent";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            this.dataGridViewTextBoxColumn9.Visible = false;
            // 
            // scoreDataGridViewTextBoxColumn
            // 
            this.scoreDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.scoreDataGridViewTextBoxColumn.DataPropertyName = "Score";
            dataGridViewCellStyle3.Format = "N2";
            dataGridViewCellStyle3.NullValue = null;
            this.scoreDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle3;
            this.scoreDataGridViewTextBoxColumn.HeaderText = "s";
            this.scoreDataGridViewTextBoxColumn.Name = "scoreDataGridViewTextBoxColumn";
            this.scoreDataGridViewTextBoxColumn.ReadOnly = true;
            this.scoreDataGridViewTextBoxColumn.ToolTipText = "Score";
            this.scoreDataGridViewTextBoxColumn.Width = 37;
            // 
            // tonalStepTextBoxColumn
            // 
            this.tonalStepTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.tonalStepTextBoxColumn.DataPropertyName = "TonalStep";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.tonalStepTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle4;
            this.tonalStepTextBoxColumn.HeaderText = "δ";
            this.tonalStepTextBoxColumn.Name = "tonalStepTextBoxColumn";
            this.tonalStepTextBoxColumn.ReadOnly = true;
            this.tonalStepTextBoxColumn.Width = 38;
            // 
            // bPMDataGridViewTextBoxColumn
            // 
            this.bPMDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.bPMDataGridViewTextBoxColumn.DataPropertyName = "BPM";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.Format = "N2";
            dataGridViewCellStyle5.NullValue = null;
            dataGridViewCellStyle5.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.bPMDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle5;
            this.bPMDataGridViewTextBoxColumn.HeaderText = "bpm";
            this.bPMDataGridViewTextBoxColumn.Name = "bPMDataGridViewTextBoxColumn";
            this.bPMDataGridViewTextBoxColumn.ReadOnly = true;
            this.bPMDataGridViewTextBoxColumn.Width = 52;
            // 
            // n100KeyDataGridViewTextBoxColumn
            // 
            this.n100KeyDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.n100KeyDataGridViewTextBoxColumn.DataPropertyName = "N100Key";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle6.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.n100KeyDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle6;
            this.n100KeyDataGridViewTextBoxColumn.HeaderText = "n100";
            this.n100KeyDataGridViewTextBoxColumn.Name = "n100KeyDataGridViewTextBoxColumn";
            this.n100KeyDataGridViewTextBoxColumn.ReadOnly = true;
            this.n100KeyDataGridViewTextBoxColumn.ToolTipText = "key normalized to 100 bpm";
            this.n100KeyDataGridViewTextBoxColumn.Width = 56;
            // 
            // deltaCentimeDataGridViewTextBoxColumn
            // 
            this.deltaCentimeDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.deltaCentimeDataGridViewTextBoxColumn.DataPropertyName = "DeltaCentime";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.deltaCentimeDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle7;
            this.deltaCentimeDataGridViewTextBoxColumn.HeaderText = "δc";
            this.deltaCentimeDataGridViewTextBoxColumn.Name = "deltaCentimeDataGridViewTextBoxColumn";
            this.deltaCentimeDataGridViewTextBoxColumn.ReadOnly = true;
            this.deltaCentimeDataGridViewTextBoxColumn.ToolTipText = "Centime offset";
            this.deltaCentimeDataGridViewTextBoxColumn.Width = 44;
            // 
            // openKeyCodeDataGridViewTextBoxColumn
            // 
            this.openKeyCodeDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.openKeyCodeDataGridViewTextBoxColumn.DataPropertyName = "OpenKeyCode";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle8.Padding = new System.Windows.Forms.Padding(5, 0, 10, 0);
            this.openKeyCodeDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle8;
            this.openKeyCodeDataGridViewTextBoxColumn.HeaderText = "okc";
            this.openKeyCodeDataGridViewTextBoxColumn.Name = "openKeyCodeDataGridViewTextBoxColumn";
            this.openKeyCodeDataGridViewTextBoxColumn.ReadOnly = true;
            this.openKeyCodeDataGridViewTextBoxColumn.ToolTipText = "Open Keycode (Traktor)";
            this.openKeyCodeDataGridViewTextBoxColumn.Width = 50;
            // 
            // artistTitleDataGridViewTextBoxColumn
            // 
            this.artistTitleDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.artistTitleDataGridViewTextBoxColumn.DataPropertyName = "Artist_Title";
            this.artistTitleDataGridViewTextBoxColumn.FillWeight = 60F;
            this.artistTitleDataGridViewTextBoxColumn.HeaderText = "artist - title";
            this.artistTitleDataGridViewTextBoxColumn.Name = "artistTitleDataGridViewTextBoxColumn";
            this.artistTitleDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // releaseTitleDataGridViewTextBoxColumn
            // 
            this.releaseTitleDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.releaseTitleDataGridViewTextBoxColumn.DataPropertyName = "ReleaseTitle";
            this.releaseTitleDataGridViewTextBoxColumn.FillWeight = 40F;
            this.releaseTitleDataGridViewTextBoxColumn.HeaderText = "release";
            this.releaseTitleDataGridViewTextBoxColumn.Name = "releaseTitleDataGridViewTextBoxColumn";
            this.releaseTitleDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // IsLovedTrackColumn
            // 
            this.IsLovedTrackColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.IsLovedTrackColumn.DataPropertyName = "IsLovedTrack";
            dataGridViewCellStyle9.ForeColor = System.Drawing.Color.Red;
            this.IsLovedTrackColumn.DefaultCellStyle = dataGridViewCellStyle9;
            this.IsLovedTrackColumn.HeaderText = "♥";
            this.IsLovedTrackColumn.Name = "IsLovedTrackColumn";
            this.IsLovedTrackColumn.ReadOnly = true;
            this.IsLovedTrackColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.IsLovedTrackColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.IsLovedTrackColumn.Width = 21;
            // 
            // fileInfoDataGridViewTextBoxColumn
            // 
            this.fileInfoDataGridViewTextBoxColumn.DataPropertyName = "FileInfo";
            this.fileInfoDataGridViewTextBoxColumn.HeaderText = "FileInfo";
            this.fileInfoDataGridViewTextBoxColumn.Name = "fileInfoDataGridViewTextBoxColumn";
            this.fileInfoDataGridViewTextBoxColumn.ReadOnly = true;
            this.fileInfoDataGridViewTextBoxColumn.Visible = false;
            // 
            // playedBeforeDataGridViewCheckBoxColumn
            // 
            this.playedBeforeDataGridViewCheckBoxColumn.DataPropertyName = "PlayedBefore";
            this.playedBeforeDataGridViewCheckBoxColumn.HeaderText = "PlayedBefore";
            this.playedBeforeDataGridViewCheckBoxColumn.Name = "playedBeforeDataGridViewCheckBoxColumn";
            this.playedBeforeDataGridViewCheckBoxColumn.ReadOnly = true;
            this.playedBeforeDataGridViewCheckBoxColumn.Visible = false;
            // 
            // ButtonYayColumn
            // 
            this.ButtonYayColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.ButtonYayColumn.HeaderText = "Yay";
            this.ButtonYayColumn.Name = "ButtonYayColumn";
            this.ButtonYayColumn.ReadOnly = true;
            this.ButtonYayColumn.Width = 31;
            // 
            // NayButtonColumn
            // 
            this.NayButtonColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.NayButtonColumn.HeaderText = "Nay";
            this.NayButtonColumn.Name = "NayButtonColumn";
            this.NayButtonColumn.ReadOnly = true;
            this.NayButtonColumn.Width = 32;
            // 
            // n100PhasePerCentDataGridViewTextBoxColumn
            // 
            this.n100PhasePerCentDataGridViewTextBoxColumn.DataPropertyName = "N100PhasePerCent";
            this.n100PhasePerCentDataGridViewTextBoxColumn.HeaderText = "N100PhasePerCent";
            this.n100PhasePerCentDataGridViewTextBoxColumn.Name = "n100PhasePerCentDataGridViewTextBoxColumn";
            this.n100PhasePerCentDataGridViewTextBoxColumn.ReadOnly = true;
            this.n100PhasePerCentDataGridViewTextBoxColumn.Visible = false;
            // 
            // MatchingTracksView
            // 
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.dataGridView1);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "MatchingTracksView";
            this.Size = new System.Drawing.Size(829, 277);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.matchingTrackViewModelBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.BindingSource matchingTrackViewModelBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn scoreDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tonalStepTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn bPMDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn n100KeyDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn deltaCentimeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn openKeyCodeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn artistTitleDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn releaseTitleDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn IsLovedTrackColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fileInfoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn playedBeforeDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewButtonColumn ButtonYayColumn;
        private System.Windows.Forms.DataGridViewButtonColumn NayButtonColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn n100PhasePerCentDataGridViewTextBoxColumn;
    }
}
