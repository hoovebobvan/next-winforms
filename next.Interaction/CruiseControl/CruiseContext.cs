﻿using next.Session;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace next.Interaction.CruiseControl
{
    public class CruiseContext 
    {
        public CruiseContext(
            SessionDataContext dataContext, 
            BindingList<SessionTrack> bindingPlaylist,
            CruiseSession session, 
            MatchQuery matchQuery)
        {
            if (new object[] { dataContext, bindingPlaylist, session, matchQuery }
                .Any(x => x == null))
            {
                throw new ArgumentNullException();
            }

            this.TrackData = dataContext;
            this.BindingPlaylist = bindingPlaylist;
            this.Session = session;
            this.MatchQuery = matchQuery;
        }

        public async Task<IEnumerable<SessionTrackMatch>> FindMatchesAsync(MatchCriteria criteria)
        {
            return await Task.Run(() => MatchQuery.Execute(TrackData, criteria));
        }

        public SessionDataContext TrackData { get; private set; }
        public BindingList<SessionTrack> BindingPlaylist { get; private set; }
        public CruiseSession Session { get; private set; }
        public MatchQuery MatchQuery { get; private set; }
    }
}
