﻿using Kit;
using next.Session;
using System;
using System.Windows.Forms;

namespace next.Interaction.CruiseControl
{
    public partial class CurrentBPMControl : UserControl
    {
        private static string textTemplate = "Current BPM {0}";

        public CurrentBPMControl()
        {
            InitializeComponent();
        }

        public void SetupBinding(BindingSource bindingSource)
        {
            var binding = BindingFactory.Create<CurrentBPMControl, CruiseSession>(
                c => c.BPM,
                bindingSource,
                c => c.BPM,
                false,
                DataSourceUpdateMode.OnPropertyChanged);

            DataBindings.Add(binding);
        }

        private double bpm = 1.0;
        public double BPM
        {
            get
            {
                return bpm;
            }
            set
            {
                this.bpm = value;
                this.pfR_Label1.Text = String.Format(textTemplate, value.ToString("000.00"));
            }
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
