﻿using Kit;
using System;
using System.Windows.Forms;

namespace next.Interaction.CruiseControl
{
    public partial class EnterBPMControl : TextBoxWithHint
    {
        public EnterBPMControl()
            : base()
        {
            this.Hint = "enter bpm";

            this.KeyPress += EnterBPMControl_KeyPress;
        }

        void EnterBPMControl_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return)
            {
                double bpm = 0;
                if (!String.IsNullOrWhiteSpace(Text) 
                    && double.TryParse(this.Text, out bpm))
                {
                    UserEnteredBPM.Raise(bpm);
                }
                this.Text = "";
            }
        }

        public event Action<double> UserEnteredBPM;

    }
}
