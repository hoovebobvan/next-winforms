﻿using Kit;
using next.Session;
using next.Tracks;
using System;
using System.Collections.Specialized;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace next.Interaction.CruiseControl
{
    public partial class MasterTrackView : UserControl
    {
        private string originalBMPTemplate;
        private string openKeyTemplate;
        private string n100Template;

        public MasterTrackView()
        {
            InitializeComponent();

            originalBMPTemplate = originalBPMLabel.Text;
            openKeyTemplate = openKeyLabel.Text;
            n100Template = n100Label.Text;

            var monospaced = new Font(FontFamily.GenericMonospace, Font.SizeInPoints);
            originalBPMLabel.Font = monospaced;
            openKeyLabel.Font = monospaced;
            n100Label.Font = monospaced;

            Invalidate();
           
            this.DragEnter += OnDragEnter;
            this.DragDrop += OnDragDrop;
            this.MouseDown += MasterTrackView_MouseDown;
            this.titleLabel.MouseDown += MasterTrackView_MouseDown;
        }

        void masterTrackBindingSource_BindingComplete(object sender, BindingCompleteEventArgs e)
        {
            // Check if the data source has been updated, and that no error has occured. 
            if (e.BindingCompleteContext ==
                BindingCompleteContext.DataSourceUpdate && e.Exception == null)

                // If not, end the current edit.
                e.Binding.BindingManagerBase.EndCurrentEdit();
        }

        void MasterTrackView_MouseDown(object sender, MouseEventArgs e)
        {
            var trackToDrag = masterTrack;

            if (trackToDrag != null)
            {
                var dataObject = new DataObject(DataFormats.FileDrop, trackToDrag.FileInfo);
                var filepaths = new StringCollection();
                filepaths.Add(trackToDrag.FileInfo.FullName);
                dataObject.SetFileDropList(filepaths);
                DoDragDrop(dataObject, DragDropEffects.Copy);
            }
        }

        public void SetupBinding(BindingSource bindingSource)
        {
            var masterTrackBinding = BindingFactory.Create<CruiseView, CruiseSession>(
                c => c.MasterTrack,
                bindingSource,
                s => s.MasterTrack,
                true,
                DataSourceUpdateMode.OnPropertyChanged);

            DataBindings.Add(masterTrackBinding);
        }

        private TrackFile masterTrack;
        public TrackFile MasterTrack
        {
            get { return masterTrack; }
            set
            {
                masterTrack = value;
                TrackViewModel viewModel = value == null ? null : new TrackViewModel(value);
                titleLabel.Text = viewModel == null ? "n/a" : viewModel.TrackName;
                originalBPMLabel.Text = viewModel == null ? "_" : String.Format(originalBMPTemplate, viewModel.BPM.ToString("00.000"));
                openKeyLabel.Text = viewModel == null ? "_" : String.Format(openKeyTemplate, viewModel.OpenKeyCode);
                n100Label.Text = viewModel == null ? "_" : String.Format(n100Template, viewModel.N100Key);
            }
        }

        void OnDragDrop(object sender, DragEventArgs e)
        {
            string filepath = null;
            try
            {
                string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
                if (files.Length > 0)
                    filepath = files[0];
            }
            catch
            { }

            if (filepath != null)
            {
                UserDropsFile.Raise(new FileInfo(filepath));
            }
        }

        void OnDragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop)) e.Effect = DragDropEffects.All;
        }

        public event Action<FileInfo> UserDropsFile;


        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                // TODO : Is this really necessary?
                this.DragEnter -= OnDragEnter;
                this.DragDrop -= OnDragDrop;
                this.MouseDown -= MasterTrackView_MouseDown;
                this.titleLabel.MouseDown -= MasterTrackView_MouseDown;
                
                components.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}
