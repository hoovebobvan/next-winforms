﻿using next.Session;
using System;
using System.IO;
using System.Linq;
using Kit;
using next.Tracks;

namespace next.Interaction.CruiseControl
{
    public class CruiseInteraction : IDisposable
    {
        private CruiseContext context;
        private CruiseSession session;
        private CruiseView view;

        public CruiseInteraction(CruiseContext context, CruiseView view)
        {
            if (context == null || view == null) throw new ArgumentNullException();
            this.context = context;
            this.session = context.Session;
            this.view = view;
        }

        public void Init()
        {
            view.UserRequestsBPMFromMaster += SetMasterAsCurrentBPM;
            view.EnterBPMControl.UserEnteredBPM += OnUserChangedBPM;
            view.MasterTrackView.UserDropsFile += OnFileDrop;
            view.N100CheckChanged += GetMatches;
            view.FavorNewCheckChanged += GetMatches;
            view.TTNeighborCheckChanged += GetMatches;
            view.FavorLikesChanged += GetMatches;
            view.AddBinding(session);
            view.PlaylistBindingSource = context.BindingPlaylist;

            GetMatches();
        }

        public void OnFileDrop(FileInfo fileInfo)
        {
            view.CallOrInvokeIfRequired(() =>
                {
                    var trackFile = context.TrackData.GetByFileInfo(fileInfo);
                    if (trackFile != null)
                    {
                        OnTrackDrop(trackFile);
                    }
                });
        }

        public void OnTrackDrop(TrackFile masterTrack)
        {
            session.PushMasterTrack(masterTrack);
            GetMatches();
        }

        public async void GetMatches()
        {
            session.N100 = view.N100Check;
            session.TTNeighbors = view.TTNeighborCheck;
            session.FavorNewTracks = view.FavorNewCheck;
            session.FavorLikes = view.FavorLikes;

            var matches = await context.FindMatchesAsync(new MatchCriteria(session));

            view.MatchingTracks = matches.Select(trackMatch => new MatchingTrackViewModel(trackMatch));

        }

        public void SetMasterAsCurrentBPM()
        {
            var masterTrack = session.MasterTrack;
            if (masterTrack != null)
            {
                session.BPM = masterTrack.BPM;
            }
            GetMatches();
        }

        public void OnUserChangedBPM(double bpm)
        {
            session.BPM = bpm;
            GetMatches();
        }

        public void Dispose()
        {
            view.UserRequestsBPMFromMaster -= SetMasterAsCurrentBPM;
            view.EnterBPMControl.UserEnteredBPM -= OnUserChangedBPM;
            view.MasterTrackView.UserDropsFile -= OnFileDrop;
            view.N100CheckChanged -= GetMatches;
            view.FavorNewCheckChanged -= GetMatches;
        }
    }
}
