﻿namespace next.Interaction.CruiseControl
{
    partial class SessionTracksView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.n100KeyDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bPMDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.openKeyCodeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Artist_Title = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.releaseTitleDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fileInfoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.releaseTrackIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.yayNayDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.keyDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.n100PhasePerCentDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sessionTrackBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sessionTrackBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCellsExceptHeaders;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.Gray;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle1.Padding = new System.Windows.Forms.Padding(0, 0, 0, 2);
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.n100KeyDataGridViewTextBoxColumn,
            this.bPMDataGridViewTextBoxColumn,
            this.openKeyCodeDataGridViewTextBoxColumn,
            this.Artist_Title,
            this.releaseTitleDataGridViewTextBoxColumn,
            this.fileInfoDataGridViewTextBoxColumn,
            this.releaseTrackIdDataGridViewTextBoxColumn,
            this.yayNayDataGridViewTextBoxColumn,
            this.keyDataGridViewTextBoxColumn,
            this.n100PhasePerCentDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.sessionTrackBindingSource;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.EnableHeadersVisualStyles = false;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(520, 205);
            this.dataGridView1.TabIndex = 0;
            // 
            // n100KeyDataGridViewTextBoxColumn
            // 
            this.n100KeyDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.n100KeyDataGridViewTextBoxColumn.DataPropertyName = "N100Key";
            this.n100KeyDataGridViewTextBoxColumn.HeaderText = "n100";
            this.n100KeyDataGridViewTextBoxColumn.Name = "n100KeyDataGridViewTextBoxColumn";
            this.n100KeyDataGridViewTextBoxColumn.ReadOnly = true;
            this.n100KeyDataGridViewTextBoxColumn.ToolTipText = "normalized to 100 bpm keycode";
            this.n100KeyDataGridViewTextBoxColumn.Width = 56;
            // 
            // bPMDataGridViewTextBoxColumn
            // 
            this.bPMDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.bPMDataGridViewTextBoxColumn.DataPropertyName = "BPM";
            dataGridViewCellStyle2.Format = "N2";
            dataGridViewCellStyle2.NullValue = null;
            this.bPMDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle2;
            this.bPMDataGridViewTextBoxColumn.HeaderText = "bpm";
            this.bPMDataGridViewTextBoxColumn.Name = "bPMDataGridViewTextBoxColumn";
            this.bPMDataGridViewTextBoxColumn.ReadOnly = true;
            this.bPMDataGridViewTextBoxColumn.ToolTipText = "original bpm";
            this.bPMDataGridViewTextBoxColumn.Width = 52;
            // 
            // openKeyCodeDataGridViewTextBoxColumn
            // 
            this.openKeyCodeDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.openKeyCodeDataGridViewTextBoxColumn.DataPropertyName = "OpenKeyCode";
            this.openKeyCodeDataGridViewTextBoxColumn.HeaderText = "okc";
            this.openKeyCodeDataGridViewTextBoxColumn.Name = "openKeyCodeDataGridViewTextBoxColumn";
            this.openKeyCodeDataGridViewTextBoxColumn.ReadOnly = true;
            this.openKeyCodeDataGridViewTextBoxColumn.ToolTipText = "Open Keycode (Traktor)";
            this.openKeyCodeDataGridViewTextBoxColumn.Width = 50;
            // 
            // Artist_Title
            // 
            this.Artist_Title.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Artist_Title.DataPropertyName = "Artist_Title";
            this.Artist_Title.FillWeight = 60F;
            this.Artist_Title.HeaderText = "artist - title";
            this.Artist_Title.Name = "Artist_Title";
            this.Artist_Title.ReadOnly = true;
            // 
            // releaseTitleDataGridViewTextBoxColumn
            // 
            this.releaseTitleDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.releaseTitleDataGridViewTextBoxColumn.DataPropertyName = "ReleaseTitle";
            this.releaseTitleDataGridViewTextBoxColumn.FillWeight = 40F;
            this.releaseTitleDataGridViewTextBoxColumn.HeaderText = "release";
            this.releaseTitleDataGridViewTextBoxColumn.Name = "releaseTitleDataGridViewTextBoxColumn";
            this.releaseTitleDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // fileInfoDataGridViewTextBoxColumn
            // 
            this.fileInfoDataGridViewTextBoxColumn.DataPropertyName = "FileInfo";
            this.fileInfoDataGridViewTextBoxColumn.HeaderText = "FileInfo";
            this.fileInfoDataGridViewTextBoxColumn.Name = "fileInfoDataGridViewTextBoxColumn";
            this.fileInfoDataGridViewTextBoxColumn.ReadOnly = true;
            this.fileInfoDataGridViewTextBoxColumn.Visible = false;
            // 
            // releaseTrackIdDataGridViewTextBoxColumn
            // 
            this.releaseTrackIdDataGridViewTextBoxColumn.DataPropertyName = "ReleaseTrackId";
            this.releaseTrackIdDataGridViewTextBoxColumn.HeaderText = "ReleaseTrackId";
            this.releaseTrackIdDataGridViewTextBoxColumn.Name = "releaseTrackIdDataGridViewTextBoxColumn";
            this.releaseTrackIdDataGridViewTextBoxColumn.ReadOnly = true;
            this.releaseTrackIdDataGridViewTextBoxColumn.Visible = false;
            // 
            // yayNayDataGridViewTextBoxColumn
            // 
            this.yayNayDataGridViewTextBoxColumn.DataPropertyName = "YayNay";
            this.yayNayDataGridViewTextBoxColumn.HeaderText = "YayNay";
            this.yayNayDataGridViewTextBoxColumn.Name = "yayNayDataGridViewTextBoxColumn";
            this.yayNayDataGridViewTextBoxColumn.ReadOnly = true;
            this.yayNayDataGridViewTextBoxColumn.Visible = false;
            // 
            // keyDataGridViewTextBoxColumn
            // 
            this.keyDataGridViewTextBoxColumn.DataPropertyName = "Key";
            this.keyDataGridViewTextBoxColumn.HeaderText = "Key";
            this.keyDataGridViewTextBoxColumn.Name = "keyDataGridViewTextBoxColumn";
            this.keyDataGridViewTextBoxColumn.ReadOnly = true;
            this.keyDataGridViewTextBoxColumn.Visible = false;
            // 
            // n100PhasePerCentDataGridViewTextBoxColumn
            // 
            this.n100PhasePerCentDataGridViewTextBoxColumn.DataPropertyName = "N100PhasePerCent";
            this.n100PhasePerCentDataGridViewTextBoxColumn.HeaderText = "N100PhasePerCent";
            this.n100PhasePerCentDataGridViewTextBoxColumn.Name = "n100PhasePerCentDataGridViewTextBoxColumn";
            this.n100PhasePerCentDataGridViewTextBoxColumn.ReadOnly = true;
            this.n100PhasePerCentDataGridViewTextBoxColumn.Visible = false;
            // 
            // sessionTrackBindingSource
            // 
            this.sessionTrackBindingSource.DataSource = typeof(next.Session.SessionTrack);
            // 
            // SessionTracksView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.dataGridView1);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "SessionTracksView";
            this.Size = new System.Drawing.Size(520, 205);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sessionTrackBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.BindingSource sessionTrackBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn n100KeyDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn bPMDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn openKeyCodeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn Artist_Title;
        private System.Windows.Forms.DataGridViewTextBoxColumn releaseTitleDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fileInfoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn releaseTrackIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn yayNayDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn keyDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn n100PhasePerCentDataGridViewTextBoxColumn;
    }
}
