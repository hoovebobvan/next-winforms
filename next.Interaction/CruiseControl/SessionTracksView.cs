﻿using Kit;
using next.Session;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;

namespace next.Interaction.CruiseControl
{
    public partial class SessionTracksView : UserControl
    {
        private IList<SessionTrack> playList;

        public SessionTracksView()
        {
            InitializeComponent();

            this.dataGridView1.HideSelectionColorUnlessClicked();

            // makes sure the playlist scrolls along
            dataGridView1.RowsAdded += DataGridView1_RowsAdded; 
            dataGridView1.MouseDown += dataGridView1_MouseDown;
        }

        private void DataGridView1_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            dataGridView1.FirstDisplayedScrollingRowIndex = dataGridView1.RowCount - 1;
        }

        public BindingList<SessionTrack> DataSource
        {
            set
            {
                playList = value;
                dataGridView1.DataSource = value;
            }
        }

        void dataGridView1_MouseDown(object sender, MouseEventArgs e)
        {
            var hitTest = dataGridView1.HitTest(e.X, e.Y);
            var rowIndex = hitTest.RowIndex;
            var colIndex = hitTest.ColumnIndex;

            if (rowIndex >= 0)
            {
                var gridRow = dataGridView1.Rows[rowIndex];

                var tracks = new List<SessionTrack>();

                if (gridRow.Selected)
                {
                    var selectedIndices = new List<int>();
                    for (int i = 0; i < dataGridView1.RowCount; i++)
                    {
                        if (dataGridView1.Rows[i].Selected)
                        {
                            selectedIndices.Add(i);
                        }
                    }
                    tracks = selectedIndices.Select(ix => playList[ix]).ToList();
                }
                else
                {
                    tracks.Add(playList[rowIndex]);
                }

                var dataObject = new DataObject(DataFormats.FileDrop);
                var filepaths = new StringCollection();

                filepaths.AddRange(tracks.Select(t => t.FileInfo.FullName).ToArray());

                dataObject.SetFileDropList(filepaths);
                DoDragDrop(dataObject, DragDropEffects.All);
            }
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                dataGridView1.RowsAdded -= DataGridView1_RowsAdded;
                dataGridView1.MouseDown += dataGridView1_MouseDown;

                components?.Dispose();
            }
            base.Dispose(disposing);
        }


    }
}
