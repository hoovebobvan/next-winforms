﻿using Kit;
using next.Store;
using next.Store.FollowUps;
using next.Store.TrackFiles;
using next.Tracks;
using System;

namespace next.Interaction.CruiseControl
{
    public class CruiseInvoker : ICommand, ICommandAction<TrackFile>
    {
        private CruiseContextFactory cruiseContextFactory;
        private TrackFileCollection collection;
        private Store.FollowUps.FollowUpCollection followUps;

        public CruiseInvoker(CruiseContextFactory cruiseContextFactory, TrackFileCollection collection, FollowUpCollection followUps)
        {
            if (cruiseContextFactory == null || collection == null)
                throw new ArgumentNullException();

            this.cruiseContextFactory = cruiseContextFactory;
            this.collection = collection;
            this.followUps = followUps;
        }

        public void ExecuteAsync()
        {
            Execute(startTrack: null);
        }

        public void Execute(TrackFile startTrack = null)
        {
            var cruiseForm = new CruiseForm();
            CruiseInteraction cruiseInteraction = null;

            cruiseForm.Load += (s, e) =>
            {
                var cruiseContext = cruiseContextFactory.Create(collection, startTrack);
                cruiseInteraction = new CruiseInteraction(cruiseContext, cruiseForm.cruiseView1);
                cruiseInteraction.Init();
            };

            cruiseForm.FormClosed += (s, a) =>
            {
                if (cruiseInteraction != null)
                {
                    cruiseInteraction.Dispose();
                }
            };

            cruiseForm.Show();

        }
    }
}
