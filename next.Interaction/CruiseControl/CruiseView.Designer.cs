﻿namespace next.Interaction.CruiseControl
{
    partial class CruiseView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;



        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonBPMFromMaster = new System.Windows.Forms.Button();
            this.n100CheckBox = new Kit.PFR_Checkbox();
            this.favorNewCheckbox = new Kit.PFR_Checkbox();
            this.ttNeighborCheckbox = new Kit.PFR_Checkbox();
            this.favorLikesCheckBox = new Kit.PFR_Checkbox();
            this.currentBPMControl1 = new next.Interaction.CruiseControl.CurrentBPMControl();
            this.enterBPMControl1 = new next.Interaction.CruiseControl.EnterBPMControl();
            this.playListControl1 = new next.Interaction.CruiseControl.SessionTracksView();
            this.trackListView1 = new next.Interaction.CruiseControl.MatchingTracksView();
            this.masterTrackView1 = new next.Interaction.CruiseControl.MasterTrackView();
            this.SuspendLayout();
            // 
            // buttonBPMFromMaster
            // 
            this.buttonBPMFromMaster.AutoSize = true;
            this.buttonBPMFromMaster.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.buttonBPMFromMaster.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonBPMFromMaster.ForeColor = System.Drawing.Color.Silver;
            this.buttonBPMFromMaster.Location = new System.Drawing.Point(178, 133);
            this.buttonBPMFromMaster.Margin = new System.Windows.Forms.Padding(3, 3, 10, 3);
            this.buttonBPMFromMaster.Name = "buttonBPMFromMaster";
            this.buttonBPMFromMaster.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.buttonBPMFromMaster.Size = new System.Drawing.Size(146, 29);
            this.buttonBPMFromMaster.TabIndex = 12;
            this.buttonBPMFromMaster.Text = "set BPM <<";
            this.buttonBPMFromMaster.UseVisualStyleBackColor = true;
            this.buttonBPMFromMaster.Click += new System.EventHandler(this.buttonBPMFromMaster_Click);
            // 
            // n100CheckBox
            // 
            this.n100CheckBox.AutoSize = true;
            this.n100CheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.n100CheckBox.FontSizeFactor = 1F;
            this.n100CheckBox.ForeColor = System.Drawing.Color.Silver;
            this.n100CheckBox.Location = new System.Drawing.Point(178, 168);
            this.n100CheckBox.Name = "n100CheckBox";
            this.n100CheckBox.Size = new System.Drawing.Size(47, 17);
            this.n100CheckBox.TabIndex = 13;
            this.n100CheckBox.Text = "n100";
            this.n100CheckBox.UseVisualStyleBackColor = true;
            this.n100CheckBox.CheckedChanged += new System.EventHandler(this.n100CheckBox_CheckedChanged);
            // 
            // favorNewCheckbox
            // 
            this.favorNewCheckbox.AutoSize = true;
            this.favorNewCheckbox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.favorNewCheckbox.FontSizeFactor = 1F;
            this.favorNewCheckbox.ForeColor = System.Drawing.Color.Silver;
            this.favorNewCheckbox.Location = new System.Drawing.Point(254, 191);
            this.favorNewCheckbox.Name = "favorNewCheckbox";
            this.favorNewCheckbox.Size = new System.Drawing.Size(70, 17);
            this.favorNewCheckbox.TabIndex = 14;
            this.favorNewCheckbox.Text = "favor new";
            this.favorNewCheckbox.UseVisualStyleBackColor = true;
            // 
            // ttNeighborCheckbox
            // 
            this.ttNeighborCheckbox.AutoSize = true;
            this.ttNeighborCheckbox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ttNeighborCheckbox.FontSizeFactor = 1F;
            this.ttNeighborCheckbox.ForeColor = System.Drawing.Color.Silver;
            this.ttNeighborCheckbox.Location = new System.Drawing.Point(178, 191);
            this.ttNeighborCheckbox.Name = "ttNeighborCheckbox";
            this.ttNeighborCheckbox.Size = new System.Drawing.Size(61, 17);
            this.ttNeighborCheckbox.TabIndex = 15;
            this.ttNeighborCheckbox.Text = "tt @ ± 1";
            this.ttNeighborCheckbox.UseVisualStyleBackColor = true;
            // 
            // favorLikesCheckBox
            // 
            this.favorLikesCheckBox.AutoSize = true;
            this.favorLikesCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.favorLikesCheckBox.FontSizeFactor = 1F;
            this.favorLikesCheckBox.ForeColor = System.Drawing.Color.Silver;
            this.favorLikesCheckBox.Location = new System.Drawing.Point(254, 168);
            this.favorLikesCheckBox.Name = "favorLikesCheckBox";
            this.favorLikesCheckBox.Size = new System.Drawing.Size(58, 17);
            this.favorLikesCheckBox.TabIndex = 14;
            this.favorLikesCheckBox.Text = "favor ♥";
            this.favorLikesCheckBox.UseVisualStyleBackColor = true;
            // 
            // currentBPMControl1
            // 
            this.currentBPMControl1.AutoSize = true;
            this.currentBPMControl1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.currentBPMControl1.BPM = 1D;
            this.currentBPMControl1.ForeColor = System.Drawing.Color.Silver;
            this.currentBPMControl1.Location = new System.Drawing.Point(9, 138);
            this.currentBPMControl1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.currentBPMControl1.Name = "currentBPMControl1";
            this.currentBPMControl1.Size = new System.Drawing.Size(103, 13);
            this.currentBPMControl1.TabIndex = 11;
            // 
            // enterBPMControl1
            // 
            this.enterBPMControl1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.enterBPMControl1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.enterBPMControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F);
            this.enterBPMControl1.Hint = "enter bpm...";
            this.enterBPMControl1.HintColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.enterBPMControl1.HintEnabled = true;
            this.enterBPMControl1.HintVisible = false;
            this.enterBPMControl1.Location = new System.Drawing.Point(33, 168);
            this.enterBPMControl1.Margin = new System.Windows.Forms.Padding(3, 10, 3, 3);
            this.enterBPMControl1.Name = "enterBPMControl1";
            this.enterBPMControl1.Size = new System.Drawing.Size(112, 12);
            this.enterBPMControl1.TabIndex = 10;
            this.enterBPMControl1.Text = "enter bpm...";
            this.enterBPMControl1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.enterBPMControl1.TextColor = System.Drawing.Color.Silver;
            // 
            // playListControl1
            // 
            this.playListControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.playListControl1.Location = new System.Drawing.Point(3, 10);
            this.playListControl1.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.playListControl1.Name = "playListControl1";
            this.playListControl1.Size = new System.Drawing.Size(919, 108);
            this.playListControl1.TabIndex = 7;
            // 
            // trackListView1
            // 
            this.trackListView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.trackListView1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.trackListView1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.trackListView1.Location = new System.Drawing.Point(3, 217);
            this.trackListView1.Margin = new System.Windows.Forms.Padding(0);
            this.trackListView1.Name = "trackListView1";
            this.trackListView1.Size = new System.Drawing.Size(922, 469);
            this.trackListView1.TabIndex = 6;
            // 
            // masterTrackView1
            // 
            this.masterTrackView1.AllowDrop = true;
            this.masterTrackView1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.masterTrackView1.BackColor = System.Drawing.Color.Gray;
            this.masterTrackView1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.masterTrackView1.ForeColor = System.Drawing.Color.Black;
            this.masterTrackView1.Location = new System.Drawing.Point(345, 133);
            this.masterTrackView1.Margin = new System.Windows.Forms.Padding(0, 15, 23, 15);
            this.masterTrackView1.MasterTrack = null;
            this.masterTrackView1.Name = "masterTrackView1";
            this.masterTrackView1.Size = new System.Drawing.Size(577, 69);
            this.masterTrackView1.TabIndex = 5;
            // 
            // CruiseView
            // 
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Controls.Add(this.ttNeighborCheckbox);
            this.Controls.Add(this.favorLikesCheckBox);
            this.Controls.Add(this.favorNewCheckbox);
            this.Controls.Add(this.n100CheckBox);
            this.Controls.Add(this.buttonBPMFromMaster);
            this.Controls.Add(this.currentBPMControl1);
            this.Controls.Add(this.enterBPMControl1);
            this.Controls.Add(this.playListControl1);
            this.Controls.Add(this.trackListView1);
            this.Controls.Add(this.masterTrackView1);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "CruiseView";
            this.Size = new System.Drawing.Size(925, 686);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private SessionTracksView playListControl1;
        public MatchingTracksView trackListView1;
        private EnterBPMControl enterBPMControl1;
        private CurrentBPMControl currentBPMControl1;
        public MasterTrackView masterTrackView1;
        private System.Windows.Forms.Button buttonBPMFromMaster;
        private Kit.PFR_Checkbox n100CheckBox;
        private Kit.PFR_Checkbox favorNewCheckbox;
        private Kit.PFR_Checkbox ttNeighborCheckbox;
        private Kit.PFR_Checkbox favorLikesCheckBox;
    }
}
