﻿using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace next.Interaction.CruiseControl
{
    public partial class CruiseForm : BaseForm
    {
        public CruiseForm()
        {
            InitializeComponent();
            this.FormClosing += new FormClosingEventHandler(CruiseForm_FormClosing);
        }

        void CruiseForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("Are you sure you wish to cancel the session?", "Close Application", MessageBoxButtons.YesNo) != DialogResult.Yes)
            {
                e.Cancel = true;
            } 
        }

        [Browsable(false)]
        [Bindable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public override Font Font
        {
            get { return base.Font; }
            set { base.Font = value; }
        }
    }
}
