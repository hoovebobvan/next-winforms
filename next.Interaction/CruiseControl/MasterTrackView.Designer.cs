﻿namespace next.Interaction.CruiseControl
{
    partial class MasterTrackView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.originalBPMLabel = new Kit.PFR_Label();
            this.openKeyLabel = new Kit.PFR_Label();
            this.n100Label = new Kit.PFR_Label();
            this.pfR_Label1 = new Kit.PFR_Label();
            this.titleLabel = new Kit.PFR_H1Label();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.originalBPMLabel);
            this.flowLayoutPanel1.Controls.Add(this.openKeyLabel);
            this.flowLayoutPanel1.Controls.Add(this.n100Label);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 32);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(351, 24);
            this.flowLayoutPanel1.TabIndex = 5;
            // 
            // originalBPMLabel
            // 
            this.originalBPMLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.originalBPMLabel.AutoSize = true;
            this.originalBPMLabel.FontSizeFactor = 1F;
            this.originalBPMLabel.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.originalBPMLabel.Location = new System.Drawing.Point(0, 0);
            this.originalBPMLabel.Margin = new System.Windows.Forms.Padding(0, 0, 15, 0);
            this.originalBPMLabel.Name = "originalBPMLabel";
            this.originalBPMLabel.Size = new System.Drawing.Size(61, 17);
            this.originalBPMLabel.TabIndex = 3;
            this.originalBPMLabel.Text = "bpm: {0}";
            this.originalBPMLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // openKeyLabel
            // 
            this.openKeyLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.openKeyLabel.AutoSize = true;
            this.openKeyLabel.FontSizeFactor = 1F;
            this.openKeyLabel.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.openKeyLabel.Location = new System.Drawing.Point(76, 0);
            this.openKeyLabel.Margin = new System.Windows.Forms.Padding(0, 0, 15, 0);
            this.openKeyLabel.Name = "openKeyLabel";
            this.openKeyLabel.Size = new System.Drawing.Size(39, 17);
            this.openKeyLabel.TabIndex = 4;
            this.openKeyLabel.Text = "-  {0}";
            this.openKeyLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // n100Label
            // 
            this.n100Label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.n100Label.AutoSize = true;
            this.n100Label.FontSizeFactor = 1F;
            this.n100Label.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.n100Label.Location = new System.Drawing.Point(130, 0);
            this.n100Label.Margin = new System.Windows.Forms.Padding(0, 0, 15, 0);
            this.n100Label.Name = "n100Label";
            this.n100Label.Size = new System.Drawing.Size(39, 17);
            this.n100Label.TabIndex = 5;
            this.n100Label.Text = "-  {0}";
            this.n100Label.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pfR_Label1
            // 
            this.pfR_Label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pfR_Label1.AutoSize = true;
            this.pfR_Label1.FontSizeFactor = 1F;
            this.pfR_Label1.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.pfR_Label1.Location = new System.Drawing.Point(366, 3);
            this.pfR_Label1.Margin = new System.Windows.Forms.Padding(0, 0, 20, 10);
            this.pfR_Label1.Name = "pfR_Label1";
            this.pfR_Label1.Size = new System.Drawing.Size(70, 17);
            this.pfR_Label1.TabIndex = 6;
            this.pfR_Label1.Text = "drop here";
            // 
            // titleLabel
            // 
            this.titleLabel.AutoSize = true;
            this.titleLabel.FontSizeFactor = 1.25F;
            this.titleLabel.Location = new System.Drawing.Point(0, 0);
            this.titleLabel.Margin = new System.Windows.Forms.Padding(0, 0, 17, 7);
            this.titleLabel.Name = "titleLabel";
            this.titleLabel.Size = new System.Drawing.Size(32, 20);
            this.titleLabel.TabIndex = 0;
            this.titleLabel.Text = "n/a";
            this.titleLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MasterTrackView
            // 
            this.AllowDrop = true;
            this.BackColor = System.Drawing.Color.Gray;
            this.Controls.Add(this.pfR_Label1);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.titleLabel);
            this.Margin = new System.Windows.Forms.Padding(0, 0, 23, 20);
            this.Name = "MasterTrackView";
            this.Size = new System.Drawing.Size(456, 65);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Kit.PFR_H1Label titleLabel;
        private Kit.PFR_Label openKeyLabel;
        private Kit.PFR_Label originalBPMLabel;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private Kit.PFR_Label pfR_Label1;
        private Kit.PFR_Label n100Label;

    }
}
