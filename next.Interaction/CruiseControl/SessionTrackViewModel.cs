﻿using next.Session;
using next.Tracks;

namespace next.Interaction.CruiseControl
{
    public class SessionTrackViewModel : TrackViewModel
    {
        public SessionTrackViewModel(TrackFile t)
            : base(t)
        { }

        public YayNay YayNay
        {
            get;
            set;
        }

    }
}
