﻿namespace next.Interaction.CruiseControl
{
    partial class CurrentBPMControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pfR_Label1 = new Kit.PFR_Label();
            this.SuspendLayout();
            // 
            // pfR_Label1
            // 
            this.pfR_Label1.AutoSize = true;
            this.pfR_Label1.Location = new System.Drawing.Point(-3, 0);
            this.pfR_Label1.Name = "pfR_Label1";
            this.pfR_Label1.Size = new System.Drawing.Size(108, 17);
            this.pfR_Label1.TabIndex = 0;
            this.pfR_Label1.Text = "Current bpm {0}";
            // 
            // CurrentBPMControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Controls.Add(this.pfR_Label1);
            this.Name = "CurrentBPMControl";
            this.Size = new System.Drawing.Size(120, 25);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Kit.PFR_Label pfR_Label1;
    }
}
