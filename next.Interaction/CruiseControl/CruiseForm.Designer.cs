﻿namespace next.Interaction.CruiseControl
{
    partial class CruiseForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CruiseForm));
            this.cruiseView1 = new next.Interaction.CruiseControl.CruiseView();
            this.SuspendLayout();
            // 
            // cruiseView1
            // 
            this.cruiseView1.AllowDrop = true;
            this.cruiseView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cruiseView1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.cruiseView1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cruiseView1.Location = new System.Drawing.Point(8, 9);
            this.cruiseView1.Margin = new System.Windows.Forms.Padding(0);
            this.cruiseView1.MasterTrack = null;
            this.cruiseView1.Name = "cruiseView1";
            this.cruiseView1.Padding = new System.Windows.Forms.Padding(4, 0, 0, 0);
            this.cruiseView1.Size = new System.Drawing.Size(774, 486);
            this.cruiseView1.TabIndex = 0;
            // 
            // CruiseForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(802, 502);
            this.Controls.Add(this.cruiseView1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "CruiseForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.Text = "u_cruise";
            this.ResumeLayout(false);

        }

        #endregion

        public CruiseControl.CruiseView cruiseView1;
    }
}

