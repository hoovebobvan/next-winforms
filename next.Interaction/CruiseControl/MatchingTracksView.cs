﻿using Kit;
using next.Session;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Drawing;
using System.Windows.Forms;

namespace next.Interaction.CruiseControl
{
    public partial class MatchingTracksView : UserControl
    {
        // private BindingSource tracksBindingSource = new BindingSource();
        private SortableBindingList<MatchingTrackViewModel> tracks = new SortableBindingList<MatchingTrackViewModel>();
        private Color darker;

        public MatchingTracksView()
        {
            InitializeComponent();

            var defaultFontSize = this.Font.SizeInPoints;
            var monospaced = new Font(FontFamily.GenericMonospace, 1.15F * defaultFontSize);

            scoreDataGridViewTextBoxColumn.DefaultCellStyle.Font = monospaced;
            tonalStepTextBoxColumn.DefaultCellStyle.Font = monospaced;
            bPMDataGridViewTextBoxColumn.DefaultCellStyle.Font = monospaced;
            n100KeyDataGridViewTextBoxColumn.DefaultCellStyle.Font = monospaced;
            deltaCentimeDataGridViewTextBoxColumn.DefaultCellStyle.Font = monospaced;
            openKeyCodeDataGridViewTextBoxColumn.DefaultCellStyle.Font = monospaced;

            dataGridView1.DefaultCellStyle.BackColor = Color.WhiteSmoke;
            dataGridView1.DefaultCellStyle.SelectionBackColor = dataGridView1.DefaultCellStyle.BackColor;
            dataGridView1.DefaultCellStyle.SelectionForeColor = dataGridView1.DefaultCellStyle.ForeColor;

            int darkerOffset = 20;
            darker = Color.FromArgb(Color.WhiteSmoke.R - darkerOffset, Color.WhiteSmoke.G - darkerOffset, Color.WhiteSmoke.B - darkerOffset);

            // tracksBindingSource.Add(tracks);
            dataGridView1.DataSource = tracks; // BindingSource;
            dataGridView1.MouseDown += new MouseEventHandler(dataGridView1_MouseDown);

            dataGridView1.CellClick += new DataGridViewCellEventHandler(dataGridView1_CellClick);
            dataGridView1.CellFormatting += new DataGridViewCellFormattingEventHandler(dataGridView1_CellFormatting);
        }

        private static Dictionary<YayNay, Color> colorsPerYayNay = new Dictionary<YayNay, Color>
        {
            { YayNay.Nay, Color.LightGray },
            { YayNay.Dunno, Color.WhiteSmoke },
            { YayNay.Yay, Color.Honeydew }
        };


        void dataGridView1_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.RowIndex >= 0 && e.RowIndex < tracks.Count)
            {
                var rowViewModel = tracks[e.RowIndex];

                var gridRow = dataGridView1.Rows[e.RowIndex];

                if (rowViewModel.IsLovedTrack)
                {
                    gridRow.DefaultCellStyle.BackColor = Color.Honeydew;
                }
                else
                {
                    if (rowViewModel.YayNay == YayNay.Yay || rowViewModel.YayNay == YayNay.Nay)
                    {
                        gridRow.DefaultCellStyle.BackColor = colorsPerYayNay[rowViewModel.YayNay];
                    }
                    else if (rowViewModel.TonalStep == "±T")
                    {
                        gridRow.DefaultCellStyle.BackColor = darker;
                    }
                }

                if (e.ColumnIndex == IsLovedTrackColumn.Index)
                {
                    e.Value = rowViewModel.IsLovedTrack ? "♥" : "";
                }

                gridRow.DefaultCellStyle.ForeColor = rowViewModel.PlayedBefore ? Color.DarkGray : dataGridView1.DefaultCellStyle.ForeColor;

                gridRow.DefaultCellStyle.SelectionBackColor = gridRow.DefaultCellStyle.BackColor;
                gridRow.DefaultCellStyle.SelectionForeColor = gridRow.DefaultCellStyle.ForeColor;
            }
        }

        void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.RowIndex < tracks.Count)
            {
                var rowViewModel = tracks[e.RowIndex];

                bool isYayButtonColumn = e.ColumnIndex == ButtonYayColumn.Index;
                bool isNayButtonColumn = e.ColumnIndex == NayButtonColumn.Index;
                bool isYayNayButtonColumn = isYayButtonColumn || isNayButtonColumn;
                
                if (isYayNayButtonColumn)
                {
                    var newYayNay = isYayButtonColumn ? YayNay.Yay : YayNay.Nay;
                    rowViewModel.YayNay = newYayNay;

                    // TODO : Consider solving this via databinding
                    dataGridView1.InvalidateRow(e.RowIndex);
                    dataGridView1.Refresh();
                }
            }
        }

        void dataGridView1_MouseDown(object sender, MouseEventArgs e)
        {
            var hitTest = dataGridView1.HitTest(e.X, e.Y);
            var rowIndex = hitTest.RowIndex;
            var colIndex = hitTest.ColumnIndex;

            if (rowIndex >= 0 && colIndex != ButtonYayColumn.Index && colIndex != NayButtonColumn.Index)
            {
                var trackToDrag = tracks[rowIndex];

                var dataObject = new DataObject(DataFormats.FileDrop, trackToDrag.FileInfo);
                var filepaths = new StringCollection();
                filepaths.Add(trackToDrag.FileInfo.FullName);
                dataObject.SetFileDropList(filepaths);
                DoDragDrop(dataObject, DragDropEffects.All);
            }
        }

        public IEnumerable<MatchingTrackViewModel> Matches
        {
            set
            {
                tracks.Clear();

                foreach (var track in value)
                {
                    tracks.Add(track);
                }
            }
        }

    }
}
