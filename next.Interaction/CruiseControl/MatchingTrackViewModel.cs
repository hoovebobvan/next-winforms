﻿using next.Session;
using next.Tonality;
using next.Tracks;
using System;
using System.Collections.Generic;
using System.IO;

namespace next.Interaction.CruiseControl
{
    public class MatchingTrackViewModel
    {
        private SessionTrackMatch trackMatch;
        private TrackFile track;
        private string tonalStep;

        public MatchingTrackViewModel(SessionTrackMatch trackMatch)
        {
            this.trackMatch = trackMatch;
            this.track = trackMatch.MatchTrack;

            this.tonalStep = deltaLookup.ContainsKey(trackMatch.CircleOfFifthsDelta)
                ? deltaLookup[trackMatch.CircleOfFifthsDelta]
                : "??";
        }

        private Dictionary<SessionTrackMatch.CircleOfFifthsDeltas, string> deltaLookup =
            new Dictionary<SessionTrackMatch.CircleOfFifthsDeltas, string>() 
            {
                { SessionTrackMatch.CircleOfFifthsDeltas.Equal, "--" },
                { SessionTrackMatch.CircleOfFifthsDeltas.Minus7, "-7" },
                { SessionTrackMatch.CircleOfFifthsDeltas.Plus7, "+7" },
                { SessionTrackMatch.CircleOfFifthsDeltas.TonalityToggle, "TT"},
                { SessionTrackMatch.CircleOfFifthsDeltas.TonalityToggleNeighbor, "±T" },
                { SessionTrackMatch.CircleOfFifthsDeltas.Unknown, "??"}
            };

        public double Score { get { return (float)trackMatch.Score; } }
        public CentimeOffset? DeltaCentime { get { return trackMatch.SemitoneOffset; } }
        public double BPM { get { return track.BPM; } }
        public DMFloatKey N100Key { get { return track.N100Key; } }
        public Centime? N100PhasePerCent { get { return track.N100PhasePerCent; } }
        public OpenKeyCode OpenKeyCode { get { return track.OpenKeyCode; } }
        public String TonalStep { get { return this.tonalStep;  } }
        public String Artist_Title { get { return track.Artist_Title; } }
        public String ReleaseTitle { get { return track.ReleaseTitle; } }
        public FileInfo FileInfo { get { return track.FileInfo; } }
        public Boolean IsLovedTrack { get { return track.IsLiked; } }

        public bool PlayedBefore { get { return trackMatch.PlayedBefore; } }

        // TODO : Remove
        public YayNay YayNay 
        { 
            get { return YayNay.Dunno; }
            set { } 
        }
    }
}
