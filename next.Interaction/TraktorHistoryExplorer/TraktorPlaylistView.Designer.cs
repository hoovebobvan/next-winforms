﻿namespace next.Interaction.TraktorHistoryExplorer
{
    partial class TraktorPlaylistView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dataGridView1 = new next.Interaction.Theme.ThemedDataGridView();
            this.artistTitleDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.releaseTitleDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.openKeyCodeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bPMDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EditFollowUp = new System.Windows.Forms.DataGridViewButtonColumn();
            this.traktorPlaylistItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.traktorPlaylistItemBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.Gray;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle1.Padding = new System.Windows.Forms.Padding(0, 0, 0, 2);
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.artistTitleDataGridViewTextBoxColumn,
            this.releaseTitleDataGridViewTextBoxColumn,
            this.openKeyCodeDataGridViewTextBoxColumn,
            this.bPMDataGridViewTextBoxColumn,
            this.EditFollowUp});
            this.dataGridView1.DataSource = this.traktorPlaylistItemBindingSource;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(615, 506);
            this.dataGridView1.TabIndex = 0;
            // 
            // artistTitleDataGridViewTextBoxColumn
            // 
            this.artistTitleDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.artistTitleDataGridViewTextBoxColumn.DataPropertyName = "Artist_Title";
            this.artistTitleDataGridViewTextBoxColumn.HeaderText = "Artist - Title";
            this.artistTitleDataGridViewTextBoxColumn.Name = "artistTitleDataGridViewTextBoxColumn";
            this.artistTitleDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // releaseTitleDataGridViewTextBoxColumn
            // 
            this.releaseTitleDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.releaseTitleDataGridViewTextBoxColumn.DataPropertyName = "ReleaseTitle";
            this.releaseTitleDataGridViewTextBoxColumn.HeaderText = "Release";
            this.releaseTitleDataGridViewTextBoxColumn.Name = "releaseTitleDataGridViewTextBoxColumn";
            this.releaseTitleDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // openKeyCodeDataGridViewTextBoxColumn
            // 
            this.openKeyCodeDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.openKeyCodeDataGridViewTextBoxColumn.DataPropertyName = "OpenKeyCode";
            this.openKeyCodeDataGridViewTextBoxColumn.HeaderText = "Key";
            this.openKeyCodeDataGridViewTextBoxColumn.Name = "openKeyCodeDataGridViewTextBoxColumn";
            this.openKeyCodeDataGridViewTextBoxColumn.ReadOnly = true;
            this.openKeyCodeDataGridViewTextBoxColumn.Width = 50;
            // 
            // bPMDataGridViewTextBoxColumn
            // 
            this.bPMDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.bPMDataGridViewTextBoxColumn.DataPropertyName = "BPM";
            this.bPMDataGridViewTextBoxColumn.HeaderText = "BPM";
            this.bPMDataGridViewTextBoxColumn.Name = "bPMDataGridViewTextBoxColumn";
            this.bPMDataGridViewTextBoxColumn.ReadOnly = true;
            this.bPMDataGridViewTextBoxColumn.Width = 55;
            // 
            // EditFollowUp
            // 
            this.EditFollowUp.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.EditFollowUp.HeaderText = "Follow up";
            this.EditFollowUp.Name = "EditFollowUp";
            this.EditFollowUp.ReadOnly = true;
            this.EditFollowUp.Text = "Edit";
            this.EditFollowUp.Width = 58;
            // 
            // traktorPlaylistItemBindingSource
            // 
            this.traktorPlaylistItemBindingSource.DataSource = typeof(next.Interaction.TraktorHistoryExplorer.TraktorPlaylistItem);
            // 
            // TraktorPlaylistView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.dataGridView1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "TraktorPlaylistView";
            this.Size = new System.Drawing.Size(615, 506);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.traktorPlaylistItemBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Theme.ThemedDataGridView dataGridView1;
        private System.Windows.Forms.BindingSource traktorPlaylistItemBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn artistTitleDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn releaseTitleDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn openKeyCodeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn bPMDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewButtonColumn EditFollowUp;
    }
}
