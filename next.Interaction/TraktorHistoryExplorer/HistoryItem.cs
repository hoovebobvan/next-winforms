﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace next.Interaction.TraktorHistoryExplorer
{
    public class HistoryItem
    {
        public static HistoryItem From(FileInfo fileInfo)
        {
            return new HistoryItem
            {
                FileName = fileInfo.Name,
                FileInfo = fileInfo
            };
        }

        public string FileName { get; private set; }
        public FileInfo FileInfo { get; private set; }
    }
}
