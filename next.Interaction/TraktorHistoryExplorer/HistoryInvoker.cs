﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kit;
using next.Exchange.Traktor;
using next.Interaction.FollowUps;

namespace next.Interaction.TraktorHistoryExplorer
{
    public class HistoryInvoker
    {
        private Func<DirectoryInfo> getHistoryDirectory;
        private PlaylistReader reader;
        private EditFollowUpInvoker editFollowUpInvoker;

        public HistoryInvoker(Func<DirectoryInfo> getHistoryDirectory, PlaylistReader reader, EditFollowUpInvoker editFollowUpInvoker)
        {
            this.getHistoryDirectory = getHistoryDirectory;
            this.reader = reader;
            this.editFollowUpInvoker = editFollowUpInvoker;
        }

        public void Invoke()
        {
            var form = new BaseForm();
            var view = new HistoryView();
            var interaction = new HistoryInteraction(view, getHistoryDirectory(), reader, editFollowUpInvoker);

            form.PutControl(view);

            form.FormClosed += (s, e) => interaction.Dispose(); 
            form.Show();
        }

    }
}
