﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Kit;
using next.FollowUps;


namespace next.Interaction.TraktorHistoryExplorer
{
    public partial class HistoryView : UserControl
    {
        private BindingList<HistoryItem> items = new BindingList<HistoryItem>();

        public HistoryView()
        {
            InitializeComponent();
            dataGridView1.HideSelectionColorUnlessClicked();
            dataGridView1.DataSource = items;
            dataGridView1.CellMouseDown += dataGridView1_CellMouseDown;
        }

        void dataGridView1_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex >= 0 && e.RowIndex < items.Count)
            {
                ItemSelected.Raise(items[e.RowIndex]);
            }
        }

        public event Action<HistoryItem> ItemSelected;

        public List<HistoryItem> HistoryItems
        {
            set
            {
                items.Clear();
                if (value != null)
                {
                    foreach (var item in value)
                        items.Add(item);
                }
            }
        }

        public List<TraktorPlaylistItem> Playlist
        {
            set
            {
               traktorPlaylistView1.Items = value;
            }
        }

        public bool TranscriptRequestEnabled
        {
            set { buttonTranscript.Enabled = value; }
        }

        public event Action TranscriptRequest;
        
        private void buttonTranscript_Click(object sender, EventArgs e)
        {
            TranscriptRequest.Raise();
        }
    }
}
