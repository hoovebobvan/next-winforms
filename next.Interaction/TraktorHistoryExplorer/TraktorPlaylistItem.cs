﻿using next.Tonality;
using next.Tracks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace next.Interaction.TraktorHistoryExplorer
{
    public class TraktorPlaylistItem
    {
        private TrackFile source;

        public TraktorPlaylistItem(TrackFile source)
        {
            this.source = source;
        }

        public string Artist_Title { get { return source.Artist_Title; } }
        public string ReleaseTitle { get { return source.ReleaseTitle; } }
        public OpenKeyCode OpenKeyCode { get { return source.OpenKeyCode; } }
        public double BPM { get { return source.BPM; } }
        public TrackFile TrackFile { get { return source; } }
    }
}