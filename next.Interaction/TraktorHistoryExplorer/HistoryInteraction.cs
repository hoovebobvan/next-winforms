﻿using next.Exchange.Traktor;
using next.Tracks;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Kit;
using next.Interaction.FollowUps;

namespace next.Interaction.TraktorHistoryExplorer
{
    public class HistoryInteraction : IDisposable
    {
        private HistoryView view;
        private DirectoryInfo historyDirectory;
        private PlaylistReader playlistReader;
        private List<TrackFile> currentTrackList;
        private EditFollowUpInvoker editFollowUpInvoker;

        public HistoryInteraction(HistoryView view, DirectoryInfo historyDirectory, PlaylistReader playlistReader, EditFollowUpInvoker editFollowUpInvoker)
        {
            this.view = view;
            this.historyDirectory = historyDirectory;
            this.playlistReader = playlistReader;
            this.editFollowUpInvoker = editFollowUpInvoker;

            view.ItemSelected += OnItemSelected;
            view.TranscriptRequest += OnTranscriptRequest;
            view.TranscriptRequestEnabled = false;

            LoadDirectory();
        }

        async void OnItemSelected(HistoryItem item)
        {
            var playlist = await playlistReader.Execute(item.FileInfo);
            view.Playlist = playlist.Select(tf => new TraktorPlaylistItem(tf)).ToList();
            currentTrackList = playlist;
            view.TranscriptRequestEnabled = true;
        }

        public async void LoadDirectory()
        {
            var historyFiles = await Task.Run(() => historyDirectory.GetFiles().ToList());
            var historyItems = historyFiles.Select(i => HistoryItem.From(i)).ToList();
            view.HistoryItems = historyItems;
        }

        private void OnTranscriptRequest()
        {
            if (currentTrackList != null)
            {
                var transcript =
                    String.Join(Environment.NewLine, currentTrackList.Select(t =>
                        String.Join(" :: ", new string[] { t.Artist, t.Title, t.ReleaseTitle })));

                var textBox = new TextBox()
                {
                    Multiline = true,
                    Width = 800,
                    Height = 400
                };

                textBox.Text = transcript;

                var form = new BaseForm();
                form.Text = "Playlist transcript";
                form.FormBorderStyle = FormBorderStyle.SizableToolWindow;
                form.PutControl(textBox);
                form.Show();
            }
        }

        
        public void Dispose()
        {
            view.ItemSelected -= OnItemSelected;
        }
    }
}
