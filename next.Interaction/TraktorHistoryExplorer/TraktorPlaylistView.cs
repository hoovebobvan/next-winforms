﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using next.Interaction.Theme;
using next.FollowUps;
using Kit;
using System.Collections.Specialized;

namespace next.Interaction.TraktorHistoryExplorer
{
    public partial class TraktorPlaylistView : ThemedUserControl
    {
        private BindingList<TraktorPlaylistItem> items = new BindingList<TraktorPlaylistItem>();

        public TraktorPlaylistView()
        {
            InitializeComponent();
            dataGridView1.HideSelectionColorUnlessClicked();
            dataGridView1.DataSource = items;

            dataGridView1.CellClick += dataGridView1_CellClick;
            dataGridView1.MouseDown += DataGridView1_MouseDown;
        }

        private void DataGridView1_MouseDown(object sender, MouseEventArgs e)
        {
            var hitTest = dataGridView1.HitTest(e.X, e.Y);
            var rowIndex = hitTest.RowIndex;
            var colIndex = hitTest.ColumnIndex;

            if (rowIndex >= 0)
            {
                var gridRow = dataGridView1.Rows[rowIndex];

                var tracks = new List<TraktorPlaylistItem>();

                if (gridRow.Selected)
                {
                    var selectedIndices = new List<int>();
                    for (int i = 0; i < dataGridView1.RowCount; i++)
                    {
                        if (dataGridView1.Rows[i].Selected)
                        {
                            selectedIndices.Add(i);
                        }
                    }
                    tracks = selectedIndices.Select(ix => items[ix]).ToList();
                }
                else
                {
                    tracks.Add(items[rowIndex]);
                }

                var dataObject = new DataObject(DataFormats.FileDrop);
                var filepaths = new StringCollection();

                var pathList = tracks
                    .Select(t => t.TrackFile?.FileInfo?.FullName)
                    .Where(p => !string.IsNullOrEmpty(p))
                    .ToArray();

                var stringCollection = new StringCollection();
                stringCollection.AddRange(pathList);

                dataObject.SetFileDropList(stringCollection);
                DoDragDrop(dataObject, DragDropEffects.All);
            }
        }

        void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.RowIndex < items.Count)
            {
                if (e.ColumnIndex == EditFollowUp.Index && e.RowIndex >= 1)
                {
                    var from = items[e.RowIndex - 1];
                    var to = items[e.RowIndex];
                    EditFollowUpRequest.Raise(new FollowUp(from.TrackFile, to.TrackFile));
                }
            }
        }

        public event Action<FollowUp> EditFollowUpRequest;

        public List<TraktorPlaylistItem> Items
        {
            set
            {
                items.Clear();
                if (value != null)
                {
                    foreach (var item in value)
                    {
                        items.Add(item);
                    }
                }
            }
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                dataGridView1.CellClick -= dataGridView1_CellClick;
                dataGridView1.MouseDown -= DataGridView1_MouseDown;

                components?.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
