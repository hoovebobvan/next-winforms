﻿namespace next.Interaction.TrackDetails
{
    partial class TrackDetailsView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.flowLayoutPanelButtons = new System.Windows.Forms.FlowLayoutPanel();
            this.buttonCalculateAcoustId = new next.Interaction.Theme.ThemedButton();
            this.buttonJSON = new next.Interaction.Theme.ThemedButton();
            this.trackPropertiesView1 = new next.Interaction.TrackDetails.TrackPropertiesView();
            this.pfR_Label1 = new Kit.PFR_Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.flowLayoutPanelButtons.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanelButtons, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.trackPropertiesView1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.pfR_Label1, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(663, 349);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // flowLayoutPanelButtons
            // 
            this.flowLayoutPanelButtons.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanelButtons.AutoSize = true;
            this.flowLayoutPanelButtons.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.flowLayoutPanelButtons.Controls.Add(this.buttonCalculateAcoustId);
            this.flowLayoutPanelButtons.Controls.Add(this.buttonJSON);
            this.flowLayoutPanelButtons.Location = new System.Drawing.Point(419, 2);
            this.flowLayoutPanelButtons.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.flowLayoutPanelButtons.Name = "flowLayoutPanelButtons";
            this.flowLayoutPanelButtons.Size = new System.Drawing.Size(241, 33);
            this.flowLayoutPanelButtons.TabIndex = 2;
            this.flowLayoutPanelButtons.WrapContents = false;
            // 
            // buttonCalculateAcoustId
            // 
            this.buttonCalculateAcoustId.AutoSize = true;
            this.buttonCalculateAcoustId.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonCalculateAcoustId.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCalculateAcoustId.ForeColor = System.Drawing.Color.Silver;
            this.buttonCalculateAcoustId.Location = new System.Drawing.Point(3, 2);
            this.buttonCalculateAcoustId.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonCalculateAcoustId.MinimumSize = new System.Drawing.Size(75, 0);
            this.buttonCalculateAcoustId.Name = "buttonCalculateAcoustId";
            this.buttonCalculateAcoustId.Size = new System.Drawing.Size(136, 29);
            this.buttonCalculateAcoustId.TabIndex = 2;
            this.buttonCalculateAcoustId.Text = "Calculate AcoustId";
            this.buttonCalculateAcoustId.UseVisualStyleBackColor = true;
            this.buttonCalculateAcoustId.Click += new System.EventHandler(this.buttonCalculateAcoustId_Click);
            // 
            // buttonJSON
            // 
            this.buttonJSON.AutoSize = true;
            this.buttonJSON.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonJSON.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonJSON.ForeColor = System.Drawing.Color.Silver;
            this.buttonJSON.Location = new System.Drawing.Point(145, 2);
            this.buttonJSON.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonJSON.MinimumSize = new System.Drawing.Size(75, 0);
            this.buttonJSON.Name = "buttonJSON";
            this.buttonJSON.Size = new System.Drawing.Size(93, 29);
            this.buttonJSON.TabIndex = 3;
            this.buttonJSON.Text = "show JSON";
            this.buttonJSON.UseVisualStyleBackColor = true;
            this.buttonJSON.Click += new System.EventHandler(this.buttonJSON_Click);
            // 
            // trackPropertiesView1
            // 
            this.trackPropertiesView1.AutoSize = true;
            this.trackPropertiesView1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel1.SetColumnSpan(this.trackPropertiesView1, 2);
            this.trackPropertiesView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.trackPropertiesView1.Location = new System.Drawing.Point(3, 52);
            this.trackPropertiesView1.Margin = new System.Windows.Forms.Padding(3, 15, 3, 2);
            this.trackPropertiesView1.Name = "trackPropertiesView1";
            this.trackPropertiesView1.Size = new System.Drawing.Size(657, 290);
            this.trackPropertiesView1.TabIndex = 0;
            // 
            // pfR_Label1
            // 
            this.pfR_Label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.pfR_Label1.AutoSize = true;
            this.pfR_Label1.FontSizeFactor = 1.4F;
            this.pfR_Label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.pfR_Label1.Location = new System.Drawing.Point(3, 0);
            this.pfR_Label1.Name = "pfR_Label1";
            this.pfR_Label1.Size = new System.Drawing.Size(115, 37);
            this.pfR_Label1.TabIndex = 2;
            this.pfR_Label1.Text = "Track details";
            this.pfR_Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // TrackDetailsView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Controls.Add(this.tableLayoutPanel1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "TrackDetailsView";
            this.Size = new System.Drawing.Size(663, 349);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.flowLayoutPanelButtons.ResumeLayout(false);
            this.flowLayoutPanelButtons.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private TrackPropertiesView trackPropertiesView1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Kit.PFR_Label pfR_Label1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanelButtons;
        private next.Interaction.Theme.ThemedButton buttonCalculateAcoustId;
        private next.Interaction.Theme.ThemedButton buttonJSON;
    }
}
