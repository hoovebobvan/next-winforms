﻿using next.Tracks;
using System.Windows.Forms;

namespace next.Interaction.TrackDetails
{
    public partial class TrackPropertiesView : UserControl
    {
        public TrackPropertiesView()
        {
            InitializeComponent();
        }

        public TrackFile TrackFile
        {
            set
            {
                textBoxTitle.Text = value == null ? "" : value.Title;
                textBoxArtist.Text = value == null ? "" : value.Artist;
                textBoxRelease.Text = value == null ? "" : value.ReleaseTitle;
                textBoxFilePath.Text = value == null ? "" : value.FileInfo.FullName;
                textBoxTempo.Text = value == null ? "" : value.BPM.ToString("0.000");
                textBoxTraktorKey.Text = value == null ? "" : value.OpenKeyCode.ToString();
                textBoxN100Key.Text = value == null ? "" : value.N100Key.ToString();
            }
        }
    }
}
