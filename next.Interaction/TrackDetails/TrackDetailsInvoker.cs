﻿using Kit;
using Newtonsoft.Json;
using next.Store;
using next.Store.TrackFiles;
using next.Tracks;
using System.Drawing;
using System.Windows.Forms;

namespace next.Interaction.TrackDetails
{
    public class TrackDetailsInvoker
    {
        public void Execute(TrackFile trackFile)
        {
            var form = new BaseForm();
            var view = new TrackDetailsView();

            form.PutControl(view);
            view.TrackFile = trackFile;

            view.ShowJsonRequest += view_ShowJsonRequest;

            form.FormClosed += (s, a) =>
                {
                    view.ShowJsonRequest -= view_ShowJsonRequest;
                };

            form.Show();
        }

        void view_ShowJsonRequest(TrackFile trackFile)
        {
            var textBox = new RichTextBox() { Width = 800, Height = 600 };
            textBox.Font = new Font(FontFamily.GenericMonospace, textBox.Font.SizeInPoints * 1.1F);
            var jsonDTO = TrackFileDTO.ToDTO(trackFile);
            string jsonText = JsonConvert.SerializeObject(jsonDTO, Formatting.Indented);
            textBox.Text = jsonText;
            var form = new BaseForm() { Text = "next: json representation" };
            form.PutControl(textBox);
            form.Show();
        }
    }
}
