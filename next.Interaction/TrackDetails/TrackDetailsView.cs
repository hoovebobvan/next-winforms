﻿using Kit;
using next.Tracks;
using System;
using System.Windows.Forms;

namespace next.Interaction.TrackDetails
{
    public partial class TrackDetailsView : UserControl
    {
        public TrackDetailsView()
        {
            InitializeComponent();
        }

        public event Action<TrackFile> AcoustIdRequest;
        public event Action<TrackFile> ShowJsonRequest;

        private void buttonCalculateAcoustId_Click(object sender, EventArgs e)
        {
            AcoustIdRequest.Raise(trackFile);
        }

        private void buttonJSON_Click(object sender, EventArgs e)
        {
            ShowJsonRequest.Raise(trackFile);
        }

        private TrackFile trackFile;
        public TrackFile TrackFile
        {
            get
            {
                return this.trackFile;
            }
            set
            {
                this.trackFile = value;
                this.trackPropertiesView1.TrackFile = value;
            }
        }
    }
}
