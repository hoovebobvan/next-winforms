﻿namespace next.Interaction.TrackDetails
{
    partial class TrackPropertiesView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.pfR_Label7 = new next.Interaction.Theme.ThemedLabel();
            this.textBoxN100Key = new next.Interaction.Theme.ThemedTextBox();
            this.textBoxTraktorKey = new next.Interaction.Theme.ThemedTextBox();
            this.textBoxTempo = new next.Interaction.Theme.ThemedTextBox();
            this.textBoxRelease = new next.Interaction.Theme.ThemedTextBox();
            this.textBoxTitle = new next.Interaction.Theme.ThemedTextBox();
            this.textBoxArtist = new next.Interaction.Theme.ThemedTextBox();
            this.pfR_Label6 = new next.Interaction.Theme.ThemedLabel();
            this.pfR_Label5 = new next.Interaction.Theme.ThemedLabel();
            this.pfR_Label1 = new next.Interaction.Theme.ThemedLabel();
            this.pfR_Label2 = new next.Interaction.Theme.ThemedLabel();
            this.pfR_Label3 = new next.Interaction.Theme.ThemedLabel();
            this.pfR_Label4 = new next.Interaction.Theme.ThemedLabel();
            this.textBoxFilePath = new next.Interaction.Theme.ThemedTextBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 22.0679F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 77.9321F));
            this.tableLayoutPanel1.Controls.Add(this.pfR_Label7, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.textBoxN100Key, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.textBoxTraktorKey, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.textBoxTempo, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.textBoxRelease, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.textBoxTitle, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.textBoxArtist, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.pfR_Label6, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.pfR_Label5, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.pfR_Label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.pfR_Label2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.pfR_Label3, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.pfR_Label4, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.textBoxFilePath, 1, 3);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 8;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(644, 239);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // pfR_Label7
            // 
            this.pfR_Label7.AutoSize = true;
            this.pfR_Label7.Dock = System.Windows.Forms.DockStyle.Left;
            this.pfR_Label7.FontSizeFactor = 1F;
            this.pfR_Label7.ForeColor = System.Drawing.Color.Silver;
            this.pfR_Label7.Location = new System.Drawing.Point(3, 101);
            this.pfR_Label7.Name = "pfR_Label7";
            this.pfR_Label7.Size = new System.Drawing.Size(88, 32);
            this.pfR_Label7.TabIndex = 1;
            this.pfR_Label7.Text = "File Location";
            this.pfR_Label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBoxN100Key
            // 
            this.textBoxN100Key.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.textBoxN100Key.Dock = System.Windows.Forms.DockStyle.Left;
            this.textBoxN100Key.FontSizeFactor = 1F;
            this.textBoxN100Key.Location = new System.Drawing.Point(147, 202);
            this.textBoxN100Key.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.textBoxN100Key.Name = "textBoxN100Key";
            this.textBoxN100Key.Size = new System.Drawing.Size(137, 22);
            this.textBoxN100Key.TabIndex = 11;
            // 
            // textBoxTraktorKey
            // 
            this.textBoxTraktorKey.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.textBoxTraktorKey.Dock = System.Windows.Forms.DockStyle.Left;
            this.textBoxTraktorKey.FontSizeFactor = 1F;
            this.textBoxTraktorKey.Location = new System.Drawing.Point(147, 170);
            this.textBoxTraktorKey.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.textBoxTraktorKey.Name = "textBoxTraktorKey";
            this.textBoxTraktorKey.Size = new System.Drawing.Size(137, 22);
            this.textBoxTraktorKey.TabIndex = 10;
            // 
            // textBoxTempo
            // 
            this.textBoxTempo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.textBoxTempo.Dock = System.Windows.Forms.DockStyle.Left;
            this.textBoxTempo.FontSizeFactor = 1F;
            this.textBoxTempo.Location = new System.Drawing.Point(147, 138);
            this.textBoxTempo.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.textBoxTempo.Name = "textBoxTempo";
            this.textBoxTempo.Size = new System.Drawing.Size(137, 22);
            this.textBoxTempo.TabIndex = 9;
            // 
            // textBoxRelease
            // 
            this.textBoxRelease.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.textBoxRelease.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxRelease.FontSizeFactor = 1F;
            this.textBoxRelease.Location = new System.Drawing.Point(147, 74);
            this.textBoxRelease.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.textBoxRelease.Name = "textBoxRelease";
            this.textBoxRelease.Size = new System.Drawing.Size(492, 22);
            this.textBoxRelease.TabIndex = 8;
            // 
            // textBoxTitle
            // 
            this.textBoxTitle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.textBoxTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxTitle.FontSizeFactor = 1F;
            this.textBoxTitle.Location = new System.Drawing.Point(147, 42);
            this.textBoxTitle.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.textBoxTitle.Name = "textBoxTitle";
            this.textBoxTitle.Size = new System.Drawing.Size(492, 22);
            this.textBoxTitle.TabIndex = 7;
            // 
            // textBoxArtist
            // 
            this.textBoxArtist.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.textBoxArtist.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxArtist.FontSizeFactor = 1F;
            this.textBoxArtist.Location = new System.Drawing.Point(147, 5);
            this.textBoxArtist.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.textBoxArtist.Name = "textBoxArtist";
            this.textBoxArtist.Size = new System.Drawing.Size(492, 22);
            this.textBoxArtist.TabIndex = 1;
            // 
            // pfR_Label6
            // 
            this.pfR_Label6.AutoSize = true;
            this.pfR_Label6.Dock = System.Windows.Forms.DockStyle.Left;
            this.pfR_Label6.FontSizeFactor = 1F;
            this.pfR_Label6.ForeColor = System.Drawing.Color.Silver;
            this.pfR_Label6.Location = new System.Drawing.Point(3, 197);
            this.pfR_Label6.Name = "pfR_Label6";
            this.pfR_Label6.Size = new System.Drawing.Size(70, 32);
            this.pfR_Label6.TabIndex = 6;
            this.pfR_Label6.Text = "N100 Key";
            this.pfR_Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pfR_Label5
            // 
            this.pfR_Label5.AutoSize = true;
            this.pfR_Label5.Dock = System.Windows.Forms.DockStyle.Left;
            this.pfR_Label5.FontSizeFactor = 1F;
            this.pfR_Label5.ForeColor = System.Drawing.Color.Silver;
            this.pfR_Label5.Location = new System.Drawing.Point(3, 165);
            this.pfR_Label5.Name = "pfR_Label5";
            this.pfR_Label5.Size = new System.Drawing.Size(82, 32);
            this.pfR_Label5.TabIndex = 5;
            this.pfR_Label5.Text = "Traktor Key";
            this.pfR_Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pfR_Label1
            // 
            this.pfR_Label1.Dock = System.Windows.Forms.DockStyle.Left;
            this.pfR_Label1.FontSizeFactor = 1F;
            this.pfR_Label1.ForeColor = System.Drawing.Color.Silver;
            this.pfR_Label1.Location = new System.Drawing.Point(3, 0);
            this.pfR_Label1.Name = "pfR_Label1";
            this.pfR_Label1.Size = new System.Drawing.Size(40, 37);
            this.pfR_Label1.TabIndex = 1;
            this.pfR_Label1.Text = "Artist";
            this.pfR_Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pfR_Label2
            // 
            this.pfR_Label2.AutoSize = true;
            this.pfR_Label2.Dock = System.Windows.Forms.DockStyle.Left;
            this.pfR_Label2.FontSizeFactor = 1F;
            this.pfR_Label2.ForeColor = System.Drawing.Color.Silver;
            this.pfR_Label2.Location = new System.Drawing.Point(3, 37);
            this.pfR_Label2.Name = "pfR_Label2";
            this.pfR_Label2.Size = new System.Drawing.Size(35, 32);
            this.pfR_Label2.TabIndex = 2;
            this.pfR_Label2.Text = "Title";
            this.pfR_Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pfR_Label3
            // 
            this.pfR_Label3.AutoSize = true;
            this.pfR_Label3.Dock = System.Windows.Forms.DockStyle.Left;
            this.pfR_Label3.FontSizeFactor = 1F;
            this.pfR_Label3.ForeColor = System.Drawing.Color.Silver;
            this.pfR_Label3.Location = new System.Drawing.Point(3, 69);
            this.pfR_Label3.Name = "pfR_Label3";
            this.pfR_Label3.Size = new System.Drawing.Size(60, 32);
            this.pfR_Label3.TabIndex = 3;
            this.pfR_Label3.Text = "Release";
            this.pfR_Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pfR_Label4
            // 
            this.pfR_Label4.AutoSize = true;
            this.pfR_Label4.Dock = System.Windows.Forms.DockStyle.Left;
            this.pfR_Label4.FontSizeFactor = 1F;
            this.pfR_Label4.ForeColor = System.Drawing.Color.Silver;
            this.pfR_Label4.Location = new System.Drawing.Point(3, 133);
            this.pfR_Label4.Name = "pfR_Label4";
            this.pfR_Label4.Size = new System.Drawing.Size(52, 32);
            this.pfR_Label4.TabIndex = 4;
            this.pfR_Label4.Text = "Tempo";
            this.pfR_Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBoxFilePath
            // 
            this.textBoxFilePath.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.textBoxFilePath.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxFilePath.FontSizeFactor = 1F;
            this.textBoxFilePath.Location = new System.Drawing.Point(147, 106);
            this.textBoxFilePath.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.textBoxFilePath.Name = "textBoxFilePath";
            this.textBoxFilePath.Size = new System.Drawing.Size(492, 22);
            this.textBoxFilePath.TabIndex = 12;
            // 
            // TrackPropertiesView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "TrackPropertiesView";
            this.Size = new System.Drawing.Size(644, 239);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private next.Interaction.Theme.ThemedLabel pfR_Label1;
        private next.Interaction.Theme.ThemedTextBox textBoxArtist;
        private next.Interaction.Theme.ThemedLabel pfR_Label6;
        private next.Interaction.Theme.ThemedLabel pfR_Label5;
        private next.Interaction.Theme.ThemedLabel pfR_Label2;
        private next.Interaction.Theme.ThemedLabel pfR_Label3;
        private next.Interaction.Theme.ThemedLabel pfR_Label4;
        private next.Interaction.Theme.ThemedTextBox textBoxN100Key;
        private next.Interaction.Theme.ThemedTextBox textBoxTraktorKey;
        private next.Interaction.Theme.ThemedTextBox textBoxTempo;
        private next.Interaction.Theme.ThemedTextBox textBoxRelease;
        private next.Interaction.Theme.ThemedTextBox textBoxTitle;
        private next.Interaction.Theme.ThemedLabel pfR_Label7;
        private next.Interaction.Theme.ThemedTextBox textBoxFilePath;
    }
}
