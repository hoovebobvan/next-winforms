﻿using Kit;
using next.Exchange.Traktor;
using next.Store;
using next.Store.TrackFiles;
using System;
using System.IO;

namespace next.Interaction.TraktorImport
{
    public class TraktorImportInvoker : ICommand
    {
        private Func<FileInfo> getNMLFileInfo;
        private TrackFileCollection masterCollection;

        public TraktorImportInvoker(Func<FileInfo> getNMLFileInfo, TrackFileCollection masterCollection)
        {
            if (getNMLFileInfo == null || masterCollection == null) throw new ArgumentNullException();
            this.getNMLFileInfo = getNMLFileInfo;
            this.masterCollection = masterCollection;
        }

        public void ExecuteAsync()
        {
            var form = new TraktorImportForm();

            var nmlFileInfo = getNMLFileInfo();
            var nmlEntryReader = new CollectionReader(nmlFileInfo);

            using (new TraktorImportInteraction(form, nmlEntryReader, masterCollection))
            {
                form.ShowDialog();
            }
        }
    }
}
