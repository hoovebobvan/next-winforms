﻿using next.Exchange.Traktor;
using next.Store;
using next.Store.TrackFiles;
using next.Tracks;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reactive.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace next.Interaction.TraktorImport
{
    public class TraktorImportInteraction : IDisposable
    {
        private TraktorImportForm form;
        private TraktorImportView view;
        private CollectionReader nmlEntryReader;
        private TrackFileCollection collection;
        private IDisposable updateSubscription;
        private ISet<string> allFileNames = new SortedSet<string>();

        public TraktorImportInteraction(TraktorImportForm form, CollectionReader nmlEntryReader, TrackFileCollection collection)
        {
            if (form == null || nmlEntryReader == null || collection == null)
            {
                throw new ArgumentNullException();
            }

            this.form = form;
            this.view = form.traktorImportView1;
            this.collection = collection;
            this.nmlEntryReader = nmlEntryReader;
            form.IsImportComplete = false;

            nmlEntryReader
                .Execute(TaskScheduler.Default)
                .ContinueWith(antecendent =>
                    {
                        if (antecendent.Exception != null)
                        {
                            MessageBox.Show(antecendent.Exception.Message);
                        }
                        else
                        {
                            updateSubscription = antecendent.Result
                                .Buffer(500)
                                .ObserveOn(form)
                                .Subscribe(OnReaderUpdate, OnError, OnCompleted);
                        }
                    });
        }

        // TODO : Refactor, this should be in an ProcessImport component 
        private void OnReaderUpdate(IEnumerable<CollectionReader.Update> updates)
        {
            // view.LogMany(updates.Select(update => update.Message));

            var errors = updates
                .OfType<CollectionReader.FailedReadEntryUpdate>()
                .Select(failed => failed.Message)
                .ToList();

            view.LogMany(errors);

            var trackBatch = updates
                .OfType<CollectionReader.EntryUpdate>()
                .Select(update => update.Entry)
                .Select(entry => new TrackFile(entry))
                .ToList();

            collection.UpsertMany(trackBatch);

            foreach (var t in trackBatch)
                allFileNames.Add(t.FilePath);

            view.Log("Submitted " + trackBatch.Count + " trackfiles", Color.Blue);
        }

        void OnError(Exception exception)
        {
            view.Log(exception.Message, Color.Red);
        }

        private void OnCompleted()
        {
            var outerSection = collection.GetOuterSection(allFileNames);

            if (outerSection.Any())
            {
                ProposeRemovalOfOutersection(outerSection.ToList());
            }

            form.IsImportComplete = true;
            view.Log("Import complete", Color.Green);
        }

        private void ProposeRemovalOfOutersection(List<TrackFile> outerSection)
        {
            var dialogResult = MessageBox.Show(
                string.Join(Environment.NewLine, new[]
                {
                    $"You have {outerSection.Count} tracks that are not in your Traktor collection",
                    "",
                    "Would you like to remove these tracks from your collection?"
                }),
                "Found collection mismatch",
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Question);

            if (dialogResult == DialogResult.Yes)
            {
                foreach (var t in outerSection)
                {
                    collection.Remove(t);
                }
            }
        }

        public void Dispose()
        {
            if (updateSubscription != null)
                updateSubscription.Dispose();
        }
    }
}
