﻿namespace next.Interaction.TraktorImport
{
    partial class TraktorImportView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.logBox = new Kit.LogBox();
            this.pfR_H1Label1 = new Kit.PFR_H1Label();
            this.SuspendLayout();
            // 
            // logBox
            // 
            this.logBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.logBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.logBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.logBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F);
            this.logBox.Location = new System.Drawing.Point(0, 31);
            this.logBox.Name = "logBox";
            this.logBox.ReadOnly = true;
            this.logBox.Size = new System.Drawing.Size(582, 315);
            this.logBox.TabIndex = 1;
            this.logBox.Text = "";
            // 
            // pfR_H1Label1
            // 
            this.pfR_H1Label1.AutoSize = true;
            this.pfR_H1Label1.FontSizeFactor = 1.15F;
            this.pfR_H1Label1.Location = new System.Drawing.Point(-3, 2);
            this.pfR_H1Label1.Margin = new System.Windows.Forms.Padding(0, 0, 15, 8);
            this.pfR_H1Label1.Name = "pfR_H1Label1";
            this.pfR_H1Label1.Size = new System.Drawing.Size(207, 18);
            this.pfR_H1Label1.TabIndex = 0;
            this.pfR_H1Label1.Text = "Import from Traktor Collection";
            // 
            // TraktorImportView
            // 
            this.Controls.Add(this.logBox);
            this.Controls.Add(this.pfR_H1Label1);
            this.Margin = new System.Windows.Forms.Padding(0, 0, 9, 7);
            this.Name = "TraktorImportView";
            this.Size = new System.Drawing.Size(582, 346);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Kit.PFR_H1Label pfR_H1Label1;
        private Kit.LogBox logBox;
    }
}
