﻿using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace next.Interaction.TraktorImport
{
    public partial class TraktorImportView : UserControl
    {
        public TraktorImportView()
        {
            InitializeComponent();
        }

        public void Log(string message)
        {
            this.logBox.Log(message);
        }

        public void LogMany(IEnumerable<string> messages)
        {
            this.logBox.LogMany(messages);
        }

        public void Log(string message, Color color)
        {
            this.logBox.Log(message, color);
        }
    }
}
