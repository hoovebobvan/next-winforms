﻿using System;

namespace next.Interaction.TraktorImport
{
    public partial class TraktorImportForm : next.Interaction.BaseForm
    {
        public TraktorImportForm()
        {
            InitializeComponent();
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            Close();
        }

        public bool IsImportComplete
        {
            set
            {
                buttonOk.Enabled = value;
                buttonAbort.Enabled = !value;
            }
        }
    }
}
