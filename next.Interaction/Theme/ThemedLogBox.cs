﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kit;

namespace next.Interaction.Theme
{
    public class ThemedLogBox : LogBox
    {
        public ThemedLogBox()
        {
            this.ForeColor = ThemeSettings.ForeColor;
            this.BackColor = ThemeSettings.TextEntryBackColor;
        }
    }
}
