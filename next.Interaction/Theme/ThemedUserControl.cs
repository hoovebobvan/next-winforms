﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace next.Interaction.Theme
{
    public class ThemedUserControl : UserControl
    {
        public ThemedUserControl()
            : base()
        {
        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // ThemedUserControl
            // 
            this.BackColor = ThemeSettings.BackColor;
            this.Name = "ThemedUserControl";
            this.ResumeLayout(false);

        }
    }
}
