﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace next.Interaction.Theme
{
    public class ThemeSettings
    {
        public static Color ForeColor = Color.Silver;
        public static Color BackColor = Color.FromArgb(64, 64, 64);

        public static Color H1ForeColor = ForeColor;
        public static float H1FontSizeFactor = 1.25F;
        
        public static Color TextEntryBackColor = Color.FromArgb(224, 224, 224);

        public static class Button
        {
            public static Color ForeColor = ThemeSettings.ForeColor;
            public static Color BorderColor = Color.Gray;
            public static Color MouseDownBackColor = Color.Gray;
            public static FlatStyle FlatStyle = FlatStyle.Flat;
            public static bool UseVisualStyleBackColor = true;
        }

        public static class DataGridView
        {
            public static Color BackgroundColor = Color.Gray;

            public static DataGridViewCellStyle ColumnHeaderCellStyle = new DataGridViewCellStyle
            {
                Alignment = DataGridViewContentAlignment.MiddleLeft,
                BackColor = Color.Silver,
                ForeColor = Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64))))),
                Padding = new System.Windows.Forms.Padding(0, 0, 0, 2),
                SelectionBackColor = SystemColors.Highlight,
                SelectionForeColor = SystemColors.HighlightText,
                WrapMode = DataGridViewTriState.True
            };
            
            public static DataGridViewCellStyle DefaultCellStyle = new DataGridViewCellStyle()
            {
                ForeColor = SystemColors.ControlText,
                BackColor = Color.FromArgb(224, 224, 224),
                SelectionBackColor = Color.FromArgb(192, 192, 255),
                SelectionForeColor = Color.Black,
                WrapMode = DataGridViewTriState.True
            };
        }
    }
}
