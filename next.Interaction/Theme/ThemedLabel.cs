﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kit;

namespace next.Interaction.Theme
{
    public class ThemedLabel : PFR_Label
    {
        public ThemedLabel()
        {
            this.ForeColor = ThemeSettings.ForeColor;
        }
    }
}
