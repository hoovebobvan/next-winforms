﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kit;

namespace next.Interaction.Theme
{
    public class ThemedTextBox : PFR_TextBox
    {
        public ThemedTextBox()
        {
            this.BackColor = ThemeSettings.TextEntryBackColor;
        }
    }
}
