﻿using Kit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace next.Interaction.Theme
{
    public class ThemedButton : AutoSizeButton
    {
        public ThemedButton()
        {
            this.FlatAppearance.BorderColor = ThemeSettings.Button.BorderColor;
            this.FlatAppearance.MouseDownBackColor = ThemeSettings.Button.MouseDownBackColor;
            this.FlatStyle = ThemeSettings.Button.FlatStyle; 
            this.ForeColor = ThemeSettings.Button.ForeColor; 
            this.UseVisualStyleBackColor = ThemeSettings.Button.UseVisualStyleBackColor;
        }
    }
}
