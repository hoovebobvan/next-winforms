﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace next.Interaction.Theme
{
    public class ThemedDataGridView : DataGridView
    {
        protected Font monospaced;

        public ThemedDataGridView()
        {
            var defaultFontSize = this.Font.SizeInPoints;
            monospaced = new Font(FontFamily.GenericMonospace, 1.15F * defaultFontSize);

            this.RowHeadersVisible = false;
            this.BackgroundColor = ThemeSettings.DataGridView.BackgroundColor;
            this.DefaultCellStyle = ThemeSettings.DataGridView.DefaultCellStyle;
            this.ColumnHeadersDefaultCellStyle = ThemeSettings.DataGridView.ColumnHeaderCellStyle;

            this.BorderStyle = BorderStyle.None;
        }
    }
}
