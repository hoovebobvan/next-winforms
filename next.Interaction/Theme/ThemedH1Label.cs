﻿using Kit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace next.Interaction.Theme
{
    public class ThemedH1Label : PFR_Label 
    {
        public static float h1fontSizeFactor = ThemeSettings.H1FontSizeFactor;
        
        public ThemedH1Label()
            : base(h1fontSizeFactor)
        {
            this.ForeColor = ThemeSettings.H1ForeColor;
        }
    }
}
