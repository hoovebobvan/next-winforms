﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace next.Interaction.Settings
{
    public class SettingsResource : IGetSettingsResource
    {
        private Properties.Settings settings;

        public SettingsResource(Properties.Settings settings)
        {
            if (settings == null) throw new ArgumentNullException();
            this.settings = settings;
        }

        public FileInfo CollectionJsonFileInfo
        {
            get
            {
                return new FileInfo(Path.Combine(DataFolder.FullName, "next-track-collection.json"));
            }
        }

        public FileInfo FollowUpsJsonFileInfo
        {
            get
            {
                return new FileInfo(Path.Combine(DataFolder.FullName, "next-followups-collection.json"));
            }
        }


        public FileInfo TraktorNMLFile
        {
            get
            {
                string path = settings.PathToCollectionFile;
                if (!String.IsNullOrEmpty(path))
                {
                    var pathInfo = new FileInfo(path);
                    return pathInfo;
                }
                return null;
            }
            set
            {
                settings.PathToCollectionFile = value == null ? "" : value.FullName;
                settings.Save();
            }
        }

        public DirectoryInfo TraktorHistoryFolder
        {
            get
            {
                if (TraktorNMLFile != null)
                {
                    string path = Path.Combine(TraktorNMLFile.DirectoryName, "History");
                    if (!String.IsNullOrEmpty(path))
                    {
                        return new DirectoryInfo(path);
                    }
                }
                return null;
            }
        }

        public DirectoryInfo DataFolder 
        { 
            get
            {
                string folderPath = settings.DataDirectory;
                return Directory.Exists(folderPath) ? new DirectoryInfo(folderPath) : null;
            }
            set
            {
                settings.DataDirectory = value == null ? "" : value.FullName;
                settings.Save();
            }
        }

        public IEnumerable<DirectoryInfo> MusicFolders
        {
            get
            {
                var allFolders = settings
                    .MusicFolders.Split(';').ToList()
                    .Where(folder => !String.IsNullOrWhiteSpace(folder))
                    .Select(fullname => new DirectoryInfo(fullname));

                return allFolders;
            }
            set
            {
                string concatenated = value == null ? "" 
                    : String.Join(";", value.Select(f => f.FullName));

                settings.MusicFolders = concatenated;
                settings.Save();
            }
        }

        public string LastFMUserName
        {
            get
            {
                return settings.LastFMUserName;
            }
            set
            {
                settings.LastFMUserName = value;
                settings.Save();
            }
        }
    }
}
