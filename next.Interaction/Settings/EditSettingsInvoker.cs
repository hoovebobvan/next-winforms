﻿using Kit;
using System;

namespace next.Interaction.Settings
{
    public class EditSettingsInvoker : ICommand
    {
        private SettingsResource settingsResource;
        private RequestFileCommand requestNMLFileCommand;
        private RequestFolderCommand requestDataFolderCommand;
        private RequestFolderInteraction requestFolderInteraction;

        public EditSettingsInvoker(
            SettingsResource settingsResource, 
            RequestFileCommand requestNMLFileCommand,
            RequestFolderCommand requestDataFolderCommand,
            RequestFolderInteraction requestFolderInteraction)
        {
            if (false // unless
                || settingsResource == null 
                || requestFolderInteraction == null 
                || requestNMLFileCommand == null
                || requestDataFolderCommand == null) 

                throw new ArgumentNullException();

            this.settingsResource = settingsResource;
            this.requestFolderInteraction = requestFolderInteraction;
            this.requestNMLFileCommand = requestNMLFileCommand;
            this.requestDataFolderCommand = requestDataFolderCommand;
        }

        public void ExecuteAsync()
        {
            // TODO : Make sure music folders are 'revived' on opening

            var form = new BaseForm();
            form.Text = "Next : settings";

            var settingsView = new SettingsView();
            var selectNMLFileView = settingsView.collectionFileSelectionView;
            selectNMLFileView.FileInfo = settingsResource.TraktorNMLFile;
            settingsView.dataFolderSelectionView.DirectoryInfo = settingsResource.DataFolder;

           
            form.PutControl(settingsView);

            using (var selectNMLFileInteraction = 
                new FileSelectionInteraction(selectNMLFileView, requestNMLFileCommand))
            using (var fileSubscription = 
                selectNMLFileInteraction.Subscribe(f => settingsResource.TraktorNMLFile = f))
            
            using (var selectDataFolderInteraction = 
                new RequestFolderInteraction(requestDataFolderCommand))
            using (var dataFolderSubscription = 
                selectDataFolderInteraction.Subscribe(f => settingsResource.DataFolder = f))
            
            using (var musicFoldersInteraction = new FoldersSelectionInteraction(
                settingsView.foldersSelectionView1, 
                requestFolderInteraction, 
                settingsResource.MusicFolders)
            )
            using (var musicFoldersSubscription = 
                musicFoldersInteraction.Subscribe(f => settingsResource.MusicFolders = f))
            {
                form.ShowDialog();
            }
        }

    }
}
