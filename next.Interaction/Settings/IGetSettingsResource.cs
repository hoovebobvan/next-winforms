﻿using System.Collections.Generic;
using System.IO;
namespace next.Interaction.Settings
{
    interface IGetSettingsResource
    {
        IEnumerable<DirectoryInfo> MusicFolders { get; }
        FileInfo TraktorNMLFile { get; }
        DirectoryInfo TraktorHistoryFolder { get; }
    }
}
