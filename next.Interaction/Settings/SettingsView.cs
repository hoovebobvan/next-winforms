﻿using Kit;
using System.Windows.Forms;

namespace next.Interaction.Settings
{
    public partial class SettingsView : UserControl
    {
        public FoldersSelectionView foldersSelectionView1;
        private PFR_Label pfR_Label1;
        private TableLayoutPanel tableLayoutPanel1;
        private PFR_Label pfR_Label3;
        public FolderSelectionView dataFolderSelectionView;
        private FlowLayoutPanel flowLayoutPanel2;
        private PFR_Label pfR_Label2;
        public FileSelectionView collectionFileSelectionView;

        public SettingsView()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            this.foldersSelectionView1 = new Kit.FoldersSelectionView();
            this.collectionFileSelectionView = new Kit.FileSelectionView();
            this.pfR_Label1 = new Kit.PFR_Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.pfR_Label3 = new Kit.PFR_Label();
            this.dataFolderSelectionView = new Kit.FolderSelectionView();
            this.pfR_Label2 = new Kit.PFR_Label();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.tableLayoutPanel1.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // foldersSelectionView1
            // 
            this.foldersSelectionView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.foldersSelectionView1.AutoSize = true;
            this.foldersSelectionView1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.foldersSelectionView1.ButtonMargin = 10;
            this.foldersSelectionView1.Location = new System.Drawing.Point(3, 194);
            this.foldersSelectionView1.Name = "foldersSelectionView1";
            this.foldersSelectionView1.Size = new System.Drawing.Size(711, 163);
            this.foldersSelectionView1.TabIndex = 1;
            // 
            // collectionFileSelectionView
            // 
            this.collectionFileSelectionView.AutoSize = true;
            this.collectionFileSelectionView.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.collectionFileSelectionView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.collectionFileSelectionView.Location = new System.Drawing.Point(3, 33);
            this.collectionFileSelectionView.Name = "collectionFileSelectionView";
            this.collectionFileSelectionView.Size = new System.Drawing.Size(711, 42);
            this.collectionFileSelectionView.TabIndex = 2;
            // 
            // pfR_Label1
            // 
            this.pfR_Label1.AutoSize = true;
            this.pfR_Label1.FontSizeFactor = 1.2F;
            this.pfR_Label1.Location = new System.Drawing.Point(3, 10);
            this.pfR_Label1.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.pfR_Label1.Name = "pfR_Label1";
            this.pfR_Label1.Size = new System.Drawing.Size(185, 20);
            this.pfR_Label1.TabIndex = 3;
            this.pfR_Label1.Text = "Traktor collection filepath";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.collectionFileSelectionView, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanel2, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.pfR_Label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.pfR_Label3, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.foldersSelectionView1, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.dataFolderSelectionView, 0, 3);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 7;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(717, 360);
            this.tableLayoutPanel1.TabIndex = 5;
            // 
            // pfR_Label3
            // 
            this.pfR_Label3.AutoSize = true;
            this.pfR_Label3.FontSizeFactor = 1.2F;
            this.pfR_Label3.Location = new System.Drawing.Point(3, 88);
            this.pfR_Label3.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.pfR_Label3.Name = "pfR_Label3";
            this.pfR_Label3.Size = new System.Drawing.Size(108, 20);
            this.pfR_Label3.TabIndex = 3;
            this.pfR_Label3.Text = "Data directory";
            // 
            // dataFolderSelectionView
            // 
            this.dataFolderSelectionView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataFolderSelectionView.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.dataFolderSelectionView.Location = new System.Drawing.Point(3, 111);
            this.dataFolderSelectionView.Name = "dataFolderSelectionView";
            this.dataFolderSelectionView.Size = new System.Drawing.Size(711, 41);
            this.dataFolderSelectionView.TabIndex = 4;
            // 
            // pfR_Label2
            // 
            this.pfR_Label2.AutoSize = true;
            this.pfR_Label2.FontSizeFactor = 1.2F;
            this.pfR_Label2.Location = new System.Drawing.Point(3, 10);
            this.pfR_Label2.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.pfR_Label2.Name = "pfR_Label2";
            this.pfR_Label2.Size = new System.Drawing.Size(102, 20);
            this.pfR_Label2.TabIndex = 4;
            this.pfR_Label2.Text = "Music folders";
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.AutoSize = true;
            this.flowLayoutPanel2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.flowLayoutPanel2.Controls.Add(this.pfR_Label2);
            this.flowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(3, 158);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(108, 30);
            this.flowLayoutPanel2.TabIndex = 1;
            // 
            // SettingsView
            // 
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "SettingsView";
            this.Size = new System.Drawing.Size(717, 360);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.flowLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel2.PerformLayout();
            this.ResumeLayout(false);

        }

    }
}
