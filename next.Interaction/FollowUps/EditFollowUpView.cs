﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using next.FollowUps;
using Kit;

namespace next.Interaction.FollowUps
{
    public partial class EditFollowUpView : UserControl
    {
        public EditFollowUpView()
        {
            InitializeComponent();
        }

        private FollowUp initialFollowUp;

        public FollowUp FollowUp
        {
            get
            {
                return new FollowUp(initialFollowUp.From, initialFollowUp.To, Rating, textBoxRemarks.Text);
            }
            set
            {
                if (value != null)
                {
                    this.initialFollowUp = value;
                    textBoxFrom.Text = value.From.Artist_Title + " - " + value.From.ReleaseTitle;
                    textBoxTo.Text = value.To.Artist_Title + " - " + value.To.ReleaseTitle;
                    Rating = value.Rating;
                    textBoxRemarks.Text = value.Remarks;
                }
            }
        }

        public Rating Rating
        {
            get
            {
                if (radioButtonPoor.Checked) return Rating.Negative;
                if (radioButtonOk.Checked) return Rating.Ok;
                if (radioButtonGood.Checked) return Rating.Good;
                return Rating.Unknown;
            }
            set
            {
                radioButtonPoor.Checked = value == Rating.Negative;
                radioButtonOk.Checked = value == Rating.Ok;
                radioButtonGood.Checked = value == Rating.Good;
            }
        }

        public event Action OkRequest;

        private void buttonOk_Click(object sender, EventArgs e)
        {
            OkRequest.Raise();
        }

    }
}
