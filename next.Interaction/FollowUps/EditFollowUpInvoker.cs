﻿using next.FollowUps;
using next.Store.FollowUps;
using next.Tracks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kit;

namespace next.Interaction.FollowUps
{
    public class EditFollowUpInvoker
    {
        private FollowUpCollection collection;

        public EditFollowUpInvoker(FollowUpCollection collection)
        {
            this.collection = collection;
        }

        public void Execute(TrackFile from, TrackFile to)
        {
            var followUp = collection.Get(from, to) ?? new FollowUp(from, to, Rating.Unknown);

            var form = new BaseForm();
            var view = new EditFollowUpView();
            form.PutControl(view);

            view.FollowUp = followUp;

            Action onOk = () =>
                {
                    collection.UpSert(view.FollowUp);
                    form.Close();
                };
            
            view.OkRequest += onOk;

            form.Show();
        }
    }
}
