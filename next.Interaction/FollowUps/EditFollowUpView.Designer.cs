﻿namespace next.Interaction.FollowUps
{
    partial class EditFollowUpView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.pfR_Label6 = new Kit.PFR_Label();
            this.pfR_Label7 = new Kit.PFR_Label();
            this.pfR_Label9 = new Kit.PFR_Label();
            this.pfR_Label1 = new Kit.PFR_Label();
            this.textBoxFrom = new next.Interaction.Theme.ThemedTextBox();
            this.textBoxTo = new next.Interaction.Theme.ThemedTextBox();
            this.textBoxRemarks = new next.Interaction.Theme.ThemedTextBox();
            this.buttonOk = new Kit.AutoSizeButton();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.radioButtonPoor = new System.Windows.Forms.RadioButton();
            this.radioButtonOk = new System.Windows.Forms.RadioButton();
            this.radioButtonGood = new System.Windows.Forms.RadioButton();
            this.pfR_H1Label1 = new Kit.PFR_H1Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.pfR_Label1, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.pfR_Label9, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.pfR_Label7, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.pfR_Label6, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.textBoxFrom, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.textBoxTo, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.textBoxRemarks, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.buttonOk, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanel1, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.pfR_H1Label1, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 6;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(497, 168);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // pfR_Label6
            // 
            this.pfR_Label6.AutoSize = true;
            this.pfR_Label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pfR_Label6.FontSizeFactor = 1F;
            this.pfR_Label6.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.pfR_Label6.Location = new System.Drawing.Point(3, 37);
            this.pfR_Label6.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.pfR_Label6.Name = "pfR_Label6";
            this.pfR_Label6.Size = new System.Drawing.Size(66, 16);
            this.pfR_Label6.TabIndex = 5;
            this.pfR_Label6.Text = "From";
            this.pfR_Label6.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // pfR_Label7
            // 
            this.pfR_Label7.AutoSize = true;
            this.pfR_Label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pfR_Label7.FontSizeFactor = 1F;
            this.pfR_Label7.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.pfR_Label7.Location = new System.Drawing.Point(3, 63);
            this.pfR_Label7.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.pfR_Label7.Name = "pfR_Label7";
            this.pfR_Label7.Size = new System.Drawing.Size(66, 16);
            this.pfR_Label7.TabIndex = 6;
            this.pfR_Label7.Text = "To";
            this.pfR_Label7.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // pfR_Label9
            // 
            this.pfR_Label9.AutoSize = true;
            this.pfR_Label9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pfR_Label9.FontSizeFactor = 1F;
            this.pfR_Label9.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.pfR_Label9.Location = new System.Drawing.Point(3, 89);
            this.pfR_Label9.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.pfR_Label9.Name = "pfR_Label9";
            this.pfR_Label9.Size = new System.Drawing.Size(66, 19);
            this.pfR_Label9.TabIndex = 8;
            this.pfR_Label9.Text = "Rating";
            this.pfR_Label9.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // pfR_Label1
            // 
            this.pfR_Label1.AutoSize = true;
            this.pfR_Label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pfR_Label1.FontSizeFactor = 1F;
            this.pfR_Label1.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.pfR_Label1.Location = new System.Drawing.Point(20, 118);
            this.pfR_Label1.Margin = new System.Windows.Forms.Padding(20, 5, 3, 0);
            this.pfR_Label1.Name = "pfR_Label1";
            this.pfR_Label1.Size = new System.Drawing.Size(49, 21);
            this.pfR_Label1.TabIndex = 9;
            this.pfR_Label1.Text = "Remarks";
            this.pfR_Label1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // textBoxFrom
            // 
            this.textBoxFrom.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.textBoxFrom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxFrom.FontSizeFactor = 1F;
            this.textBoxFrom.Location = new System.Drawing.Point(75, 35);
            this.textBoxFrom.Name = "textBoxFrom";
            this.textBoxFrom.ReadOnly = true;
            this.textBoxFrom.Size = new System.Drawing.Size(419, 20);
            this.textBoxFrom.TabIndex = 10;
            // 
            // textBoxTo
            // 
            this.textBoxTo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.textBoxTo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxTo.FontSizeFactor = 1F;
            this.textBoxTo.Location = new System.Drawing.Point(75, 61);
            this.textBoxTo.Name = "textBoxTo";
            this.textBoxTo.ReadOnly = true;
            this.textBoxTo.Size = new System.Drawing.Size(419, 20);
            this.textBoxTo.TabIndex = 11;
            // 
            // textBoxRemarks
            // 
            this.textBoxRemarks.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.textBoxRemarks.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxRemarks.FontSizeFactor = 1F;
            this.textBoxRemarks.Location = new System.Drawing.Point(75, 116);
            this.textBoxRemarks.Name = "textBoxRemarks";
            this.textBoxRemarks.Size = new System.Drawing.Size(419, 20);
            this.textBoxRemarks.TabIndex = 12;
            // 
            // buttonOk
            // 
            this.buttonOk.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOk.AutoSize = true;
            this.buttonOk.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonOk.Location = new System.Drawing.Point(429, 142);
            this.buttonOk.MinimumSize = new System.Drawing.Size(65, 0);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(65, 23);
            this.buttonOk.TabIndex = 13;
            this.buttonOk.Text = "Ok";
            this.buttonOk.UseVisualStyleBackColor = true;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoSize = true;
            this.flowLayoutPanel1.Controls.Add(this.radioButtonPoor);
            this.flowLayoutPanel1.Controls.Add(this.radioButtonOk);
            this.flowLayoutPanel1.Controls.Add(this.radioButtonGood);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(75, 87);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(419, 23);
            this.flowLayoutPanel1.TabIndex = 14;
            // 
            // radioButtonPoor
            // 
            this.radioButtonPoor.AutoSize = true;
            this.radioButtonPoor.ForeColor = System.Drawing.Color.Red;
            this.radioButtonPoor.Location = new System.Drawing.Point(3, 3);
            this.radioButtonPoor.Name = "radioButtonPoor";
            this.radioButtonPoor.Size = new System.Drawing.Size(46, 17);
            this.radioButtonPoor.TabIndex = 0;
            this.radioButtonPoor.TabStop = true;
            this.radioButtonPoor.Text = "poor";
            this.radioButtonPoor.UseVisualStyleBackColor = true;
            // 
            // radioButtonOk
            // 
            this.radioButtonOk.AutoSize = true;
            this.radioButtonOk.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.radioButtonOk.Location = new System.Drawing.Point(55, 3);
            this.radioButtonOk.Name = "radioButtonOk";
            this.radioButtonOk.Size = new System.Drawing.Size(39, 17);
            this.radioButtonOk.TabIndex = 1;
            this.radioButtonOk.TabStop = true;
            this.radioButtonOk.Text = "Ok";
            this.radioButtonOk.UseVisualStyleBackColor = true;
            // 
            // radioButtonGood
            // 
            this.radioButtonGood.AutoSize = true;
            this.radioButtonGood.ForeColor = System.Drawing.Color.Lime;
            this.radioButtonGood.Location = new System.Drawing.Point(100, 3);
            this.radioButtonGood.Name = "radioButtonGood";
            this.radioButtonGood.Size = new System.Drawing.Size(51, 17);
            this.radioButtonGood.TabIndex = 2;
            this.radioButtonGood.TabStop = true;
            this.radioButtonGood.Text = "Good";
            this.radioButtonGood.UseVisualStyleBackColor = true;
            // 
            // pfR_H1Label1
            // 
            this.pfR_H1Label1.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.pfR_H1Label1, 2);
            this.pfR_H1Label1.FontSizeFactor = 1.25F;
            this.pfR_H1Label1.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.pfR_H1Label1.Location = new System.Drawing.Point(3, 5);
            this.pfR_H1Label1.Margin = new System.Windows.Forms.Padding(3, 5, 3, 10);
            this.pfR_H1Label1.Name = "pfR_H1Label1";
            this.pfR_H1Label1.Size = new System.Drawing.Size(95, 17);
            this.pfR_H1Label1.TabIndex = 15;
            this.pfR_H1Label1.Text = "Edit Follow up";
            // 
            // RateFollowUpView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "RateFollowUpView";
            this.Size = new System.Drawing.Size(503, 174);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Kit.PFR_Label pfR_Label1;
        private Kit.PFR_Label pfR_Label9;
        private Kit.PFR_Label pfR_Label7;
        private Kit.PFR_Label pfR_Label6;
        private Theme.ThemedTextBox textBoxFrom;
        private Theme.ThemedTextBox textBoxTo;
        private Theme.ThemedTextBox textBoxRemarks;
        private Kit.AutoSizeButton buttonOk;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.RadioButton radioButtonPoor;
        private System.Windows.Forms.RadioButton radioButtonOk;
        private System.Windows.Forms.RadioButton radioButtonGood;
        private Kit.PFR_H1Label pfR_H1Label1;
    }
}
