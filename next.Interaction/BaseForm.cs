﻿using Microsoft.Win32;
using System.Drawing;
using System.Windows.Forms;

namespace next.Interaction
{
    public partial class BaseForm : Form
    {
        /// <summary>
        /// ref 2014-09: [How to: Respond to Font Scheme Changes in a Windows Forms Application](responding to http://msdn.microsoft.com/en-us/library/ms229594(v=vs.110).aspx)
        /// </summary>
        public BaseForm()
        {
            this.Font = SystemFonts.MessageBoxFont;
            SystemEvents.UserPreferenceChanged += SystemEvents_UserPreferenceChanged;
            this.FormClosing += BaseForm_FormClosing;
            InitializeComponent();
        }

        void BaseForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            SystemEvents.UserPreferenceChanged -= SystemEvents_UserPreferenceChanged;
        }

        void SystemEvents_UserPreferenceChanged(object sender, UserPreferenceChangedEventArgs e)
        {
            if (e.Category == UserPreferenceCategory.Window)
            {
                this.Font = SystemFonts.MessageBoxFont;
            }
        }
    }
}
