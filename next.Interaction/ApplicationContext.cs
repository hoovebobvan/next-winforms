﻿using Kit;
using next.Exchange;
using next.Exchange.CSV;
using next.Interaction.CollectionManagement;
using next.Interaction.CruiseControl;
using next.Interaction.FollowUps;
using next.Interaction.LastFMConnector;
using next.Interaction.Settings;
using next.Interaction.Tags;
using next.Interaction.TrackDetails;
using next.Interaction.TraktorImport;
using next.Store;
using next.Store.FollowUps;
using next.Store.TextIndex;
using next.Store.TrackFiles;
using System;
using System.IO;

namespace next.Interaction
{
    public class ApplicationContext : System.Windows.Forms.ApplicationContext
    {
        public ApplicationContext(BaseForm mainForm)
            : base(mainForm)
        {
            var settingsResource = new SettingsResource(Properties.Settings.Default);

            var requestNMLFileCommand = new RequestFileCommand("Please specify the path to your Traktor NML", "Traktor collection (*.nml)|*.nml");
            var requestFolderInteraction = new RequestFolderInteraction(new RequestFolderCommand());
            var requestDataFolderCommand = new RequestFolderCommand();

            // TODO : Use CompositeViewDecorator 
            var editSettingsInvoker = new EditSettingsInvoker(
                settingsResource,
                requestNMLFileCommand,
                requestDataFolderCommand,
                requestFolderInteraction);

            var collection = new TrackFileCollection();
            var followUps = new FollowUpCollection();
            var cruiseContextFactory = new CruiseContextFactory();
            var cruiseInvoker = new CruiseInvoker(cruiseContextFactory, collection, followUps);

            Func<FileInfo> getNMLFileInfo = () => settingsResource.TraktorNMLFile;
            var traktorImportInvoker = new TraktorImportInvoker(getNMLFileInfo, collection);

            var lastFMInvoker = new LastFMInvoker(collection);
            var lastFMRestClient = Exchange.LastFM.RestClient.Create();
            var getLovedTracks = new Exchange.LastFM.LovedTracks.GetLovedTracks(lastFMRestClient);
            var lastFMTagLovedTracksCommand = new LastFMTagLovedTracks(collection, getLovedTracks, () => settingsResource.LastFMUserName);

            var luceneDirectoryInfo = new DirectoryInfo(Properties.Settings.Default.LuceneIndexPath);
            var trackFileIndex = TrackFileIndex.Create(luceneDirectoryInfo);
            var trackFileIndexConnector = new TrackFileIndexConnector(trackFileIndex, collection);
            var trackFileDataContext = new TrackFileDataContext(collection, trackFileIndex, trackFileIndexConnector);

            var trackDetailsInvoker = new TrackDetailsInvoker();

            var loadCollectionInteraction = new LoadCollectionInteraction(
                new LoadTrackFileCollection(), 
                collection, 
                () => settingsResource.CollectionJsonFileInfo,
                new LoadFollowUps(),
                followUps,
                () => settingsResource.FollowUpsJsonFileInfo);

            var saveCollectionInteraction = new SaveCollectionInteraction(
                new SaveTrackFileCollection(), 
                collection, 
                () => settingsResource.DataFolder);

            var exportCollectionCSVInteraction = new ExportCollectionCSVInteraction(
                new ExportTrackFileCollectionCSV(), 
                collection, 
                new RequestFileCommand("Please select a target filepath", "CSV Files (*.csv)|*.csv"));

            var checkForMissingFiles = new CollectionManagement.CheckForMissingFiles(collection);

            var editFollowUpInvoker = new EditFollowUpInvoker(followUps);

            var tagsInvoker = new TagsInvoker(collection);

            var traktorHistoryInvoker = new TraktorHistoryExplorer.HistoryInvoker(
                () => settingsResource.TraktorHistoryFolder, 
                new Exchange.Traktor.PlaylistReader(), 
                editFollowUpInvoker);

            // TODO : trackdetails and more specifically cruiseInvoker need to be injected
            //          here just for the entrygrid interaction to function
            //
            //      alternative would be to send 'command' objects to a channel, would require less
            //      elaborate 'wiring' (or injecting)
            var managementInvoker = new ManagementInvoker(trackFileDataContext, 
                trackDetailsInvoker,
                cruiseInvoker,
                new CompositeViewDecorator<TrackFileView>
                {
                    {
                        view => view.SettingsRequest += editSettingsInvoker.ExecuteAsync,
                        view => view.SettingsRequest -= editSettingsInvoker.ExecuteAsync 
                    },
                    { 
                        view => view.TraktorImportRequest += traktorImportInvoker.ExecuteAsync,
                        view => view.TraktorImportRequest -= traktorImportInvoker.ExecuteAsync
                    },
                    {
                        view => view.LastFMImportRequest += lastFMTagLovedTracksCommand.Invoke,
                        view => view.LastFMImportRequest -= lastFMTagLovedTracksCommand.Invoke 
                    },
                    {
                        view => view.StartSessionRequest += cruiseInvoker.ExecuteAsync,
                        view => view.StartSessionRequest -= cruiseInvoker.ExecuteAsync
                    },
                    {
                        view => view.LastFMSetupRequest += lastFMInvoker.ExecuteAsync,
                        view => view.LastFMSetupRequest -= lastFMInvoker.ExecuteAsync
                    },
                    {
                        view => view.TraktorHistoryRequest += traktorHistoryInvoker.Invoke,
                        view => view.TraktorHistoryRequest -= traktorHistoryInvoker.Invoke
                    },
                    {
                        view => view.TagsBetaRequest += tagsInvoker.Invoke,
                        view => view.TagsBetaRequest -= tagsInvoker.Invoke
                    },
                    saveCollectionInteraction,
                    loadCollectionInteraction,
                    exportCollectionCSVInteraction,
                    checkForMissingFiles
                });

            managementInvoker.Execute(mainForm);
        }
    }
}
