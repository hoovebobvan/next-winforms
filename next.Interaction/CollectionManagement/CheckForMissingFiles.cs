﻿using Kit;
using next.Store;
using next.Store.TrackFiles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace next.Interaction.CollectionManagement
{
    public class CheckForMissingFiles : IViewDecorator<TrackFileView>
    {
        private TrackFileCollection collection;

        public CheckForMissingFiles(TrackFileCollection collection)
        {
            this.collection = collection;
        }

        public void Attach(TrackFileView target)
        {
            target.CheckForMissingFilesRequest += Run;
        }

        public void Detach(TrackFileView target)
        {
            target.CheckForMissingFilesRequest -= Run;
        }

        async void Run()
        {
            var command = new Store.TrackFiles.CheckForMissingFiles();
            var missing = await command.ExecuteAsync(collection);

            missing.ToList().ForEach(track => collection.Remove(track));

            MessageBox.Show(String.Format("Removed {0} collection entries that could not be located", missing.Count));
        }

        public void Dispose()
        {

        }
    }
}
