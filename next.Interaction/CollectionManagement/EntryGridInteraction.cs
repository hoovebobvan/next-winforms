﻿using next.Interaction.TrackDetails;
using next.Store;
using next.Store.TextIndex;
using next.Store.TrackFiles;
using next.Tracks;
using System;
using System.IO;
using System.Windows.Forms;

namespace next.Interaction.CollectionManagement
{
    public class EntryGridInteraction : IDisposable
    {
        private EntryGridView entryGridView;
        private TrackFileDataContext dataContext;
        private ContextMenuStrip menuStrip;
        private TrackFile currentTrackFile;
        private TrackDetailsInvoker detailsInvoker;
        private CruiseControl.CruiseInvoker cruiseInvoker;

        public EntryGridInteraction(
            EntryGridView entryGridView, 
            TrackFileDataContext dataContext, 
            TrackDetailsInvoker detailsInvoker, 
            CruiseControl.CruiseInvoker cruiseInvoker)
        {
            if (entryGridView == null || dataContext == null)
                throw new ArgumentNullException();

            this.entryGridView = entryGridView;
            this.dataContext = dataContext;
            this.detailsInvoker = detailsInvoker;
            this.cruiseInvoker = cruiseInvoker;

            this.menuStrip = CreateContextMenuStrip();
            this.entryGridView.EntryMenuRequest += entryGridView_EntryMenuRequest;
        }

        public void Dispose()
        {
            this.currentTrackFile = null;
            this.entryGridView.EntryMenuRequest -= entryGridView_EntryMenuRequest;
            this.menuStrip.Dispose();
        }

        private ContextMenuStrip CreateContextMenuStrip()
        {
            var strip = new ContextMenuStrip();
            
            var detailsItem = new ToolStripMenuItem() { Text = "Details..." };
            var startSessionItem = new ToolStripMenuItem() { Text = "Use to start session" };
            
            detailsItem.Click += detailsItem_Click;
            startSessionItem.Click += startSessionItem_Click;

            strip.Items.Add(detailsItem);
            strip.Items.Add(startSessionItem);

            return strip;
        }

        void startSessionItem_Click(object sender, EventArgs e)
        {
            if (currentTrackFile != null)
            {
                cruiseInvoker.Execute(currentTrackFile);
            }
        }

        void detailsItem_Click(object sender, EventArgs e)
        {
            if (currentTrackFile != null)
            {
                detailsInvoker.Execute(currentTrackFile);
            }
        }

        void entryGridView_EntryMenuRequest(Entry entry)
        {
            currentTrackFile = dataContext.Collection.GetByFileInfo(new FileInfo(entry.Path));
            menuStrip.Show(Cursor.Position);
        }

    }
}
