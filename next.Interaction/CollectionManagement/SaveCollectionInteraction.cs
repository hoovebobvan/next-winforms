﻿using Kit;
using next.Store;
using next.Store.TrackFiles;
using System;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace next.Interaction.CollectionManagement
{
    public class SaveCollectionInteraction : IViewDecorator<TrackFileView>
    {
        private TaskScheduler uiScheduler = TaskScheduler.FromCurrentSynchronizationContext();

        private SaveTrackFileCollection saveCollection;
        private TrackFileCollection collection;
        private Func<DirectoryInfo> getTargetDirectoryInfo;
        private TrackFileView view;

        public void Attach(TrackFileView view)
        {
            // TODO : Is this really necessary?
            if (view != null)
                Detach(view);

            view.SaveRequest += target_SaveRequest;
            this.view = view;
        }

        public void Detach(TrackFileView view)
        {
            view.SaveRequest -= target_SaveRequest;
            view = null;
        }

        void target_SaveRequest()
        {
            var targetDirectoryInfo = getTargetDirectoryInfo();
            if (targetDirectoryInfo.Exists)
            {
                var showSaving = view.SubmitActionText("Saving collection...");
                
                saveCollection
                    .Execute(TaskScheduler.Default, collection, getTargetDirectoryInfo())
                    .ContinueWith(antecedent =>
                    {
                        if (antecedent.Exception != null)
                        {
                            MessageBox.Show("An exception occured while saving the collection:\n\n" 
                                + antecedent.Exception.Message);
                        }
                        showSaving.Dispose();
                    },
                    uiScheduler);
            }
        }

        public void Dispose()
        {

        }

        public SaveCollectionInteraction(
            SaveTrackFileCollection saveCollection,
            TrackFileCollection collection,
            Func<DirectoryInfo> getTargetDirectoryInfo)
        {
            this.saveCollection = saveCollection;
            this.collection = collection;
            this.getTargetDirectoryInfo = getTargetDirectoryInfo;
        }
    
    }
}
