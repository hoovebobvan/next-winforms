﻿namespace next.Interaction.CollectionManagement
{
    partial class TrackFileView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.actionLabel = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.buttonStartSession = new Kit.AutoSizeButton();
            this.contextMenuButton1 = new Kit.ContextMenuButton();
            this.collectionMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.trakorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lastFM3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportCSVToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fingerprintingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.checkForMissingFilesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exploreTraktorHistoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsMenuButton = new Kit.ContextMenuButton();
            this.settingsMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.filesAndFoldersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lastFMToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.searchTextBox = new Kit.TextBoxWithHint();
            this.numericEntryDropdown1 = new Kit.NumericEntryDropdown();
            this.lovedTracksCheckbox = new Kit.PFR_Checkbox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.pfR_H1Label1 = new Kit.PFR_H1Label();
            this.entryGridView1 = new next.Interaction.CollectionManagement.EntryGridView();
            this.taggingBETAToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.flowLayoutPanel1.SuspendLayout();
            this.collectionMenuStrip1.SuspendLayout();
            this.settingsMenuStrip.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.label1.Location = new System.Drawing.Point(326, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(8, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 23);
            this.label1.TabIndex = 4;
            this.label1.Text = "Page limit";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // actionLabel
            // 
            this.actionLabel.AutoSize = true;
            this.actionLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.actionLabel.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.actionLabel.Location = new System.Drawing.Point(434, 0);
            this.actionLabel.Margin = new System.Windows.Forms.Padding(8, 0, 2, 0);
            this.actionLabel.Name = "actionLabel";
            this.actionLabel.Size = new System.Drawing.Size(121, 23);
            this.actionLabel.TabIndex = 5;
            this.actionLabel.Text = "short running tasks label";
            this.actionLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel1.AutoSize = true;
            this.flowLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.flowLayoutPanel1.Controls.Add(this.buttonStartSession);
            this.flowLayoutPanel1.Controls.Add(this.contextMenuButton1);
            this.flowLayoutPanel1.Controls.Add(this.settingsMenuButton);
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(504, 2);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(2);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(231, 29);
            this.flowLayoutPanel1.TabIndex = 10;
            this.flowLayoutPanel1.WrapContents = false;
            // 
            // buttonStartSession
            // 
            this.buttonStartSession.AutoSize = true;
            this.buttonStartSession.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonStartSession.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.buttonStartSession.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gray;
            this.buttonStartSession.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonStartSession.ForeColor = System.Drawing.Color.Silver;
            this.buttonStartSession.Location = new System.Drawing.Point(150, 2);
            this.buttonStartSession.Margin = new System.Windows.Forms.Padding(2);
            this.buttonStartSession.MinimumSize = new System.Drawing.Size(65, 0);
            this.buttonStartSession.Name = "buttonStartSession";
            this.buttonStartSession.Size = new System.Drawing.Size(79, 25);
            this.buttonStartSession.TabIndex = 9;
            this.buttonStartSession.Text = "Start session";
            this.buttonStartSession.UseVisualStyleBackColor = true;
            this.buttonStartSession.Click += new System.EventHandler(this.buttonStartSession_Click);
            // 
            // contextMenuButton1
            // 
            this.contextMenuButton1.AutoSize = true;
            this.contextMenuButton1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.contextMenuButton1.ContextMenuStrip = this.collectionMenuStrip1;
            this.contextMenuButton1.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.contextMenuButton1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gray;
            this.contextMenuButton1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.contextMenuButton1.ForeColor = System.Drawing.Color.Silver;
            this.contextMenuButton1.Location = new System.Drawing.Point(72, 2);
            this.contextMenuButton1.Margin = new System.Windows.Forms.Padding(2);
            this.contextMenuButton1.MinimumSize = new System.Drawing.Size(65, 0);
            this.contextMenuButton1.Name = "contextMenuButton1";
            this.contextMenuButton1.Size = new System.Drawing.Size(74, 25);
            this.contextMenuButton1.TabIndex = 10;
            this.contextMenuButton1.Text = "Collection...";
            this.contextMenuButton1.UseVisualStyleBackColor = true;
            // 
            // collectionMenuStrip1
            // 
            this.collectionMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.collectionMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.trakorToolStripMenuItem,
            this.lastFM3ToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.exportCSVToolStripMenuItem,
            this.fingerprintingToolStripMenuItem,
            this.checkForMissingFilesToolStripMenuItem,
            this.exploreTraktorHistoryToolStripMenuItem,
            this.taggingBETAToolStripMenuItem});
            this.collectionMenuStrip1.Name = "importMenuStrip1";
            this.collectionMenuStrip1.Size = new System.Drawing.Size(194, 202);
            // 
            // trakorToolStripMenuItem
            // 
            this.trakorToolStripMenuItem.Name = "trakorToolStripMenuItem";
            this.trakorToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.trakorToolStripMenuItem.Text = "Traktor import";
            this.trakorToolStripMenuItem.Click += new System.EventHandler(this.trakorToolStripMenuItem_Click);
            // 
            // lastFM3ToolStripMenuItem
            // 
            this.lastFM3ToolStripMenuItem.Name = "lastFM3ToolStripMenuItem";
            this.lastFM3ToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.lastFM3ToolStripMenuItem.Text = "Fetch Last FM <3";
            this.lastFM3ToolStripMenuItem.Click += new System.EventHandler(this.lastFM3ToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // exportCSVToolStripMenuItem
            // 
            this.exportCSVToolStripMenuItem.Name = "exportCSVToolStripMenuItem";
            this.exportCSVToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.exportCSVToolStripMenuItem.Text = "Export CSV";
            this.exportCSVToolStripMenuItem.Click += new System.EventHandler(this.exportCSVToolStripMenuItem_Click);
            // 
            // fingerprintingToolStripMenuItem
            // 
            this.fingerprintingToolStripMenuItem.Name = "fingerprintingToolStripMenuItem";
            this.fingerprintingToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.fingerprintingToolStripMenuItem.Text = "Fingerprinting";
            this.fingerprintingToolStripMenuItem.Click += new System.EventHandler(this.fingerprintingToolStripMenuItem_Click);
            // 
            // checkForMissingFilesToolStripMenuItem
            // 
            this.checkForMissingFilesToolStripMenuItem.Name = "checkForMissingFilesToolStripMenuItem";
            this.checkForMissingFilesToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.checkForMissingFilesToolStripMenuItem.Text = "Check for missing files";
            this.checkForMissingFilesToolStripMenuItem.Click += new System.EventHandler(this.checkForMissingFilesToolStripMenuItem_Click);
            // 
            // exploreTraktorHistoryToolStripMenuItem
            // 
            this.exploreTraktorHistoryToolStripMenuItem.Name = "exploreTraktorHistoryToolStripMenuItem";
            this.exploreTraktorHistoryToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.exploreTraktorHistoryToolStripMenuItem.Text = "Explore Traktor history";
            this.exploreTraktorHistoryToolStripMenuItem.Click += new System.EventHandler(this.exploreTraktorHistoryToolStripMenuItem_Click);
            // 
            // settingsMenuButton
            // 
            this.settingsMenuButton.AutoSize = true;
            this.settingsMenuButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.settingsMenuButton.ContextMenuStrip = this.settingsMenuStrip;
            this.settingsMenuButton.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.settingsMenuButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gray;
            this.settingsMenuButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.settingsMenuButton.ForeColor = System.Drawing.Color.Silver;
            this.settingsMenuButton.Location = new System.Drawing.Point(2, 2);
            this.settingsMenuButton.Margin = new System.Windows.Forms.Padding(2);
            this.settingsMenuButton.MinimumSize = new System.Drawing.Size(65, 0);
            this.settingsMenuButton.Name = "settingsMenuButton";
            this.settingsMenuButton.Size = new System.Drawing.Size(66, 25);
            this.settingsMenuButton.TabIndex = 11;
            this.settingsMenuButton.Text = "Settings...";
            this.settingsMenuButton.UseVisualStyleBackColor = true;
            // 
            // settingsMenuStrip
            // 
            this.settingsMenuStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.settingsMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.filesAndFoldersToolStripMenuItem,
            this.lastFMToolStripMenuItem});
            this.settingsMenuStrip.Name = "settingsMenuStrip";
            this.settingsMenuStrip.Size = new System.Drawing.Size(162, 48);
            // 
            // filesAndFoldersToolStripMenuItem
            // 
            this.filesAndFoldersToolStripMenuItem.Name = "filesAndFoldersToolStripMenuItem";
            this.filesAndFoldersToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.filesAndFoldersToolStripMenuItem.Text = "Files and Folders";
            this.filesAndFoldersToolStripMenuItem.Click += new System.EventHandler(this.filesAndFoldersToolStripMenuItem_Click);
            // 
            // lastFMToolStripMenuItem
            // 
            this.lastFMToolStripMenuItem.Name = "lastFMToolStripMenuItem";
            this.lastFMToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.lastFMToolStripMenuItem.Text = "Last FM";
            this.lastFMToolStripMenuItem.Click += new System.EventHandler(this.lastFMToolStripMenuItem_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel1.ColumnCount = 5;
            this.tableLayoutPanel2.SetColumnSpan(this.tableLayoutPanel1, 2);
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.searchTextBox, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label1, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.numericEntryDropdown1, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.actionLabel, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.lovedTracksCheckbox, 1, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(2, 49);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(2, 16, 2, 2);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 4F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(557, 27);
            this.tableLayoutPanel1.TabIndex = 12;
            // 
            // searchTextBox
            // 
            this.searchTextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.searchTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.searchTextBox.ForeColor = System.Drawing.Color.DimGray;
            this.searchTextBox.Hint = "search...";
            this.searchTextBox.HintColor = System.Drawing.Color.DimGray;
            this.searchTextBox.HintEnabled = true;
            this.searchTextBox.HintVisible = true;
            this.searchTextBox.Location = new System.Drawing.Point(0, 2);
            this.searchTextBox.Margin = new System.Windows.Forms.Padding(0, 2, 0, 0);
            this.searchTextBox.MinimumSize = new System.Drawing.Size(4, 24);
            this.searchTextBox.Name = "searchTextBox";
            this.searchTextBox.Size = new System.Drawing.Size(274, 20);
            this.searchTextBox.TabIndex = 11;
            this.searchTextBox.Text = "search...";
            this.searchTextBox.TextColor = System.Drawing.SystemColors.ControlText;
            // 
            // numericEntryDropdown1
            // 
            this.numericEntryDropdown1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.numericEntryDropdown1.DefaultValue = 200;
            this.numericEntryDropdown1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.numericEntryDropdown1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.numericEntryDropdown1.FormattingEnabled = true;
            this.numericEntryDropdown1.Location = new System.Drawing.Point(380, 2);
            this.numericEntryDropdown1.Margin = new System.Windows.Forms.Padding(0, 2, 0, 0);
            this.numericEntryDropdown1.Name = "numericEntryDropdown1";
            this.numericEntryDropdown1.Size = new System.Drawing.Size(46, 21);
            this.numericEntryDropdown1.TabIndex = 3;
            this.numericEntryDropdown1.Text = "200";
            // 
            // lovedTracksCheckbox
            // 
            this.lovedTracksCheckbox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lovedTracksCheckbox.AutoSize = true;
            this.lovedTracksCheckbox.FontSizeFactor = 1F;
            this.lovedTracksCheckbox.ForeColor = System.Drawing.Color.Coral;
            this.lovedTracksCheckbox.Location = new System.Drawing.Point(277, 3);
            this.lovedTracksCheckbox.Name = "lovedTracksCheckbox";
            this.lovedTracksCheckbox.Size = new System.Drawing.Size(38, 17);
            this.lovedTracksCheckbox.TabIndex = 12;
            this.lovedTracksCheckbox.Text = "<3";
            this.lovedTracksCheckbox.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.Controls.Add(this.pfR_H1Label1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.flowLayoutPanel1, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.entryGridView1, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel1, 0, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(8, 8);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(737, 388);
            this.tableLayoutPanel2.TabIndex = 13;
            // 
            // pfR_H1Label1
            // 
            this.pfR_H1Label1.AutoSize = true;
            this.pfR_H1Label1.Dock = System.Windows.Forms.DockStyle.Left;
            this.pfR_H1Label1.FontSizeFactor = 1.5F;
            this.pfR_H1Label1.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.pfR_H1Label1.Location = new System.Drawing.Point(2, 2);
            this.pfR_H1Label1.Margin = new System.Windows.Forms.Padding(2);
            this.pfR_H1Label1.Name = "pfR_H1Label1";
            this.pfR_H1Label1.Size = new System.Drawing.Size(87, 29);
            this.pfR_H1Label1.TabIndex = 1;
            this.pfR_H1Label1.Text = "Track files";
            this.pfR_H1Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // entryGridView1
            // 
            this.entryGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel2.SetColumnSpan(this.entryGridView1, 2);
            this.entryGridView1.Location = new System.Drawing.Point(2, 80);
            this.entryGridView1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.entryGridView1.Name = "entryGridView1";
            this.entryGridView1.Size = new System.Drawing.Size(733, 306);
            this.entryGridView1.TabIndex = 6;
            // 
            // taggingBETAToolStripMenuItem
            // 
            this.taggingBETAToolStripMenuItem.Name = "taggingBETAToolStripMenuItem";
            this.taggingBETAToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.taggingBETAToolStripMenuItem.Text = "Tagging (BETA)";
            this.taggingBETAToolStripMenuItem.Click += new System.EventHandler(this.taggingBETAToolStripMenuItem_Click);
            // 
            // TrackFileView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ContextMenuStrip = this.settingsMenuStrip;
            this.Controls.Add(this.tableLayoutPanel2);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "TrackFileView";
            this.Padding = new System.Windows.Forms.Padding(8);
            this.Size = new System.Drawing.Size(753, 404);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.collectionMenuStrip1.ResumeLayout(false);
            this.settingsMenuStrip.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Kit.PFR_H1Label pfR_H1Label1;
        private Kit.NumericEntryDropdown numericEntryDropdown1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label actionLabel;
        private EntryGridView entryGridView1;
        private Kit.AutoSizeButton buttonStartSession;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.ContextMenuStrip collectionMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem trakorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lastFM3ToolStripMenuItem;
        private Kit.TextBoxWithHint searchTextBox;
        private Kit.ContextMenuButton contextMenuButton1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportCSVToolStripMenuItem;
        private Kit.ContextMenuButton settingsMenuButton;
        private System.Windows.Forms.ContextMenuStrip settingsMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem filesAndFoldersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lastFMToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fingerprintingToolStripMenuItem;
        private Kit.PFR_Checkbox lovedTracksCheckbox;
        private System.Windows.Forms.ToolStripMenuItem checkForMissingFilesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exploreTraktorHistoryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem taggingBETAToolStripMenuItem;
    }
}
