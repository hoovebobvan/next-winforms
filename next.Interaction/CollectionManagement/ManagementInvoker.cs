﻿using Kit;
using next.Interaction.TrackDetails;
using next.Store;
using next.Store.TrackFiles;
using System;
using System.Windows.Forms;

namespace next.Interaction.CollectionManagement
{
    public class ManagementInvoker
    {
        private TrackFileDataContext dataContext;
        private CompositeViewDecorator<TrackFileView> decorator;
        private TrackDetailsInvoker detailsInvoker;
        private CruiseControl.CruiseInvoker cruiseInvoker;

        public ManagementInvoker(
            TrackFileDataContext trackFileDataContext, 
            TrackDetailsInvoker detailsInvoker, 
            CruiseControl.CruiseInvoker cruiseInvoker,
            CompositeViewDecorator<TrackFileView> decorator)
        {
            this.dataContext = trackFileDataContext;
            this.detailsInvoker = detailsInvoker;
            this.decorator = decorator;
            this.cruiseInvoker = cruiseInvoker;
        }

        public void Execute(BaseForm targetForm = null)
        {
            try
            {
                var form = targetForm ?? new BaseForm();

                form.FormClosing += (s, e) =>
                {                                
                    if (MessageBox.Show("Closing might cancel a session hence thing nag, sure you wish to close?", "Close Application", MessageBoxButtons.YesNo) != DialogResult.Yes)
                    {
                        e.Cancel = true;
                    } 
                };

                form.Text = "next: collection management";
         
                var view = new TrackFileView();
                form.PutControl(view);

                decorator.Attach(view);
                var searchInteraction = new SearchInteraction(view, dataContext);
                var entryListInteraction = new EntryGridInteraction(view.EntryGrid, dataContext, detailsInvoker, cruiseInvoker);                

                form.FormClosed += (s, a) =>
                    {
                        decorator.Detach(view);
                        decorator.Dispose();
                        searchInteraction.Dispose();
                        entryListInteraction.Dispose();
                        dataContext.Dispose();
                    };
                                                        
                form.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("An exception occured: {0}", ex.Message));
            }
        }
    }
}
