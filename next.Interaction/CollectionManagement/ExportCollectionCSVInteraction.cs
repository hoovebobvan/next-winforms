﻿using Kit;
using next.Exchange.CSV;
using next.Store;
using next.Store.TrackFiles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace next.Interaction.CollectionManagement
{
    public class ExportCollectionCSVInteraction : IViewDecorator<TrackFileView>
    {
        static readonly TaskScheduler uiScheduler = TaskScheduler.FromCurrentSynchronizationContext();

        private ExportTrackFileCollectionCSV saveTrackFileCollectionCSV;
        private Store.TrackFiles.TrackFileCollection collection;
        private Kit.RequestFileCommand requestFileCommand;
        private TrackFileView view;

        public ExportCollectionCSVInteraction(ExportTrackFileCollectionCSV saveTrackFileCollectionCSV, TrackFileCollection collection, Kit.RequestFileCommand requestFileCommand)
        {
            // TODO: Complete member initialization
            this.saveTrackFileCollectionCSV = saveTrackFileCollectionCSV;
            this.collection = collection;
            this.requestFileCommand = requestFileCommand;
        }

        public void Attach(TrackFileView view)
        {
            if (view != null)
                Detach(view);

            view.ExportCSVRequest += view_ExportCSVRequest;
            this.view = view;
        }

        public void Detach(TrackFileView view)
        {
            view.ExportCSVRequest -= view_ExportCSVRequest;
        }

        void view_ExportCSVRequest()
        {
            var fileInfo = requestFileCommand.ExecuteDialog();
            if (fileInfo != null)
            {
                var showingMessage = view.SubmitActionText("Exporting CSV...");
                
                saveTrackFileCollectionCSV
                    .Execute(TaskScheduler.Default, collection, fileInfo)
                    .ContinueWith(antecedent =>
                    {
                        if (antecedent.Exception != null)
                        {
                            MessageBox.Show("An exception occured during CSV export: " + antecedent.Exception.Message);
                        }
                        showingMessage.Dispose();

                    }, uiScheduler);
            }
        }


        public void Dispose()
        {
            
        }
    }
}
