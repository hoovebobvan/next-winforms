﻿using Kit;
using next.Store.TextIndex;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Disposables;
using System.Windows.Forms;

namespace next.Interaction.CollectionManagement
{
    public partial class TrackFileView : UserControl
    {
        private SortedSet<String> actionSet = new SortedSet<string>();

        public TrackFileView()
        {
            InitializeComponent();

            this.searchTextBox.ValueChanged += _ => RaiseSearchCriteriaChanged();
            this.lovedTracksCheckbox.CheckedChanged += (_, __) => RaiseSearchCriteriaChanged();

            this.numericEntryDropdown1.Items.AddRange(new object[] { 50, 100, 200, 500, 1000 });
            ActionText = "";
        }

        void lovedTracksCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            RaiseSearchCriteriaChanged();
        }

        void searchTextBox_ValueChanged(string obj)
        {
            RaiseSearchCriteriaChanged();
        }

        public event Action<SearchCriteria> SearchCriteriaChanged;

        private void RaiseSearchCriteriaChanged()
        {
            var criteria = new SearchCriteria(searchTextBox.ActualText, lovedTracksCheckbox.Checked);
            SearchCriteriaChanged.Raise(criteria);
        }

        public bool IsIndexAvailable
        {
            set 
            { 
                this.searchTextBox.Enabled = value;
                this.lovedTracksCheckbox.Enabled = value;
            }
        }

        public int PageSize
        {
            get { return numericEntryDropdown1.Value; }
        }

        private string ActionText
        {
            set { actionLabel.Text = value ?? ""; }
        }

        public IDisposable SubmitActionText(string text)
        {
            actionSet.Add(text);
            UpdateActionText();

            return Disposable.Create(() =>
                {
                    if (actionSet.Contains(text))
                    {
                        actionSet.Remove(text);
                        UpdateActionText();
                    }
                });
        }

        private void UpdateActionText()
        {
            ActionText = String.Join(" | ", actionSet.ToArray());
        }

        public IEnumerable<Entry> Entries
        {
            set { this.EntryGrid.Entries = value; }
        }

        public EntryGridView EntryGrid { get { return this.entryGridView1; } }

        public event Action SettingsRequest;
        public event Action StartSessionRequest;
        public event Action SaveRequest;
        public event Action TraktorImportRequest;
        public event Action LastFMImportRequest;
        public event Action ExportCSVRequest;
        public event Action LastFMSetupRequest;
        public event Action FingerprintingRequest;
        public event Action CheckForMissingFilesRequest;
        public event Action TraktorHistoryRequest;
        public event Action TagsBetaRequest;

        private void buttonImport_Click(object sender, EventArgs e)
        {
            TraktorImportRequest.Raise();
        }

        private void buttonStartSession_Click(object sender, EventArgs e)
        {
            StartSessionRequest.Raise();
        }

        private void trakorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TraktorImportRequest.Raise();
        }

        private void lastFM3ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LastFMImportRequest.Raise();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveRequest.Raise();
        }

        private void exportCSVToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ExportCSVRequest.Raise();
        }

        private void filesAndFoldersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SettingsRequest.Raise();
        }

        private void lastFMToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LastFMSetupRequest.Raise();
        }

        private void fingerprintingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FingerprintingRequest.Raise();
        }

        private void checkForMissingFilesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CheckForMissingFilesRequest.Raise();
        }

        private void exploreTraktorHistoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TraktorHistoryRequest.Raise();
        }

        private void taggingBETAToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TagsBetaRequest.Raise();
        }
    }
}
