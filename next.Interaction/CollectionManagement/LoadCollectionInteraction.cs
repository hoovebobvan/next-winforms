﻿using Kit;
using next.Store;
using next.Store.FollowUps;
using next.Store.TrackFiles;
using System;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace next.Interaction.CollectionManagement
{
    public class LoadCollectionInteraction : IViewDecorator<TrackFileView>
    {
        private TaskScheduler uiScheduler = TaskScheduler.FromCurrentSynchronizationContext();

        private LoadTrackFileCollection loadCollectionCommand;
        private TrackFileCollection collection;
        private Func<FileInfo> getCollectionFileInfo;

        private LoadFollowUps loadFollowUps;
        private FollowUpCollection followUpCollection;
        private Func<FileInfo> getFollowUpCollectionFileInfo;

        public void Attach(TrackFileView target)
        {
            target.VisibleChanged += target_Load; 
        }

        public void Detach(TrackFileView target)
        {
            try
            {
                target.VisibleChanged -= target_Load;
            }
            catch { }
        }

        async void target_Load(object sender, EventArgs e)
        {
            var view = (TrackFileView)sender;

            Detach(view);

            var actionShowing = view.SubmitActionText("Loading collection");
            
            await loadCollectionCommand
                .ExecuteAsync(TaskScheduler.Default, collection, getCollectionFileInfo())
                .ContinueWith(antecedent =>
                {
                    if (antecedent.Exception != null)
                    {
                        MessageBox.Show("An exception occured\n\n" + antecedent.Exception.Message);
                    }
                    actionShowing.Dispose();
                }, 
                uiScheduler);

            actionShowing = view.SubmitActionText("Loading follow ups");
            await loadFollowUps.ExecuteAsync(followUpCollection, getFollowUpCollectionFileInfo(), collection);
            actionShowing.Dispose();

        }

        public void Dispose()
        {
        }

        public LoadCollectionInteraction(
            LoadTrackFileCollection loadCollectionCommand,
            TrackFileCollection collection,
            Func<FileInfo> getCollectionFileInfo,
            LoadFollowUps loadFollowUps,
            FollowUpCollection followUpCollection,
            Func<FileInfo> getFollowUpCollectionFileInfo)
        {
            // silly repetition here
            this.loadCollectionCommand = loadCollectionCommand;
            this.collection = collection;
            this.getCollectionFileInfo = getCollectionFileInfo;
            this.loadFollowUps = loadFollowUps;
            this.followUpCollection = followUpCollection;
            this.getFollowUpCollectionFileInfo = getFollowUpCollectionFileInfo;
        }

    }
}
