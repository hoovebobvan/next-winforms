﻿using next.Store;
using next.Store.TextIndex;
using next.Store.TrackFiles;
using System;
using System.Reactive.Linq;

namespace next.Interaction.CollectionManagement
{
    public class SearchInteraction : IDisposable
    {
        private TrackFileView view;
        private TrackFileDataContext dataContext;
        private IDisposable indexStatusSubscription;
        private IDisposable statusShowing = null;

        public SearchInteraction(TrackFileView view, TrackFileDataContext dataContext)
        {
            if (view == null || dataContext == null) throw new ArgumentNullException();

            this.view = view;
            this.dataContext = dataContext;

            this.indexStatusSubscription = dataContext.TrackFileIndexConnector.IsRebuilding
                .ObserveOn(view)
                .Subscribe(isRebuilding =>
                {
                    if (isRebuilding)
                    {
                        view.IsIndexAvailable = false;
                        statusShowing = view.SubmitActionText("Building Index");
                    }
                    else 
                    {
                        view.IsIndexAvailable = true;

                        if (statusShowing != null)
                        {
                            statusShowing.Dispose();
                            statusShowing = null;
                        }
                    }
                });

            view.SearchCriteriaChanged += OnSearchQueryChanged;
        }

        public void Dispose()
        {
            indexStatusSubscription.Dispose();
            
            if (statusShowing != null)
            {
                statusShowing.Dispose();
            }
            
            view.SearchCriteriaChanged -= OnSearchQueryChanged;
        }

        void OnSearchQueryChanged(SearchCriteria criteria)
        {
            var indexEntries = dataContext.TrackFileIndex.Search(criteria, view.PageSize);
            view.Entries = indexEntries;
        }

    }
}
