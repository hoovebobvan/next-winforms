﻿using Kit;
using next.Store.TextIndex;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace next.Interaction.CollectionManagement
{
    public partial class EntryGridView : UserControl
    {
        private SortableBindingList<Entry> entries = 
            new SortableBindingList<Entry>();

        public EntryGridView()
        {
            InitializeComponent();
            dataGridView1.DataSource = entries;

            dataGridView1.CellMouseDown += dataGridView1_CellMouseDown;
            dataGridView1.MouseDown += dataGridView1_MouseDown;
        }

        void dataGridView1_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex >= 0 && e.RowIndex < entries.Count)
            {
                EntryMenuRequest.Raise(entries[e.RowIndex]);
            }
        }

        public event Action<Entry> EntryMenuRequest;

        public IEnumerable<Entry> Entries
        {
            set
            {
                entries.Clear();
                if (value != null)
                {
                    foreach (var entry in value)
                    {
                        entries.Add(entry);
                    }
                }
            }
        }

        // TODO : Reduce boilerplate
        void dataGridView1_MouseDown(object sender, MouseEventArgs e)
        {
            var hitTest = dataGridView1.HitTest(e.X, e.Y);
            var rowIndex = hitTest.RowIndex;
            var colIndex = hitTest.ColumnIndex;

            if (rowIndex >= 0 && rowIndex < entries.Count)
            {
                var gridRow = dataGridView1.Rows[rowIndex];
                var tracks = new List<Entry>();

                if (gridRow.Selected)
                {
                    var selectedIndices = new List<int>();
                    for (int i = 0; i < dataGridView1.RowCount; i++)
                    {
                        if (dataGridView1.Rows[i].Selected)
                        {
                            selectedIndices.Add(i);
                        }
                    }
                    tracks = selectedIndices.Select(ix => entries[ix]).ToList();
                }
                else
                {
                    tracks.Add(entries[rowIndex]);
                }

                var dataObject = new DataObject(DataFormats.FileDrop);
                var filepaths = new StringCollection();

                filepaths.AddRange(tracks.Select(t => t.Path).ToArray());

                dataObject.SetFileDropList(filepaths);
                DoDragDrop(dataObject, DragDropEffects.All);
            }
        }

    }
}
