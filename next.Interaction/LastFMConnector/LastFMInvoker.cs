﻿using Kit;
using LastFM = next.Exchange.LastFM;
using next.Exchange.LastFM.Auth;
using next.Store;
using System;
using System.Diagnostics;
using System.Drawing;
using next.Store.TrackFiles;

namespace next.Interaction.LastFMConnector
{
    public class LastFMInvoker : ICommand
    {
        private TrackFileCollection trackFileCollection;

        public LastFMInvoker(TrackFileCollection trackFileCollection)
        {
            this.trackFileCollection = trackFileCollection;
        }

        public void ExecuteAsync()
        {
            var existingUserName = Properties.Settings.Default.LastFMUserName;
            var existingToken = Properties.Settings.Default.LastFMUserToken;
            var existingSessionKey = Properties.Settings.Default.LastFMSessionKey;

            var form = new LastFMForm();
            var settingsView = form.SettingsView;
            var service = LastFM.Service.Create();

            settingsView.UserName = existingUserName;

            Action<string> setUserName = username =>
                {
                    Properties.Settings.Default.LastFMUserName = username;
                    Properties.Settings.Default.Save();
                };

            Action<string> setToken = token =>
                {
                    Properties.Settings.Default.LastFMUserToken = token;
                    Properties.Settings.Default.Save();

                    var confirmationLink = service.GetConfirmationUri(token);

                    settingsView.Token = token;
                    settingsView.ConfirmationLink = confirmationLink.AbsoluteUri;
                };

            Action<string> setSessionKey = sessionKey =>
                {
                    Properties.Settings.Default.LastFMSessionKey = sessionKey;
                    Properties.Settings.Default.Save();
                    
                    if (String.IsNullOrWhiteSpace(sessionKey))
                    {
                        settingsView.SetSessionKey("Denied", Color.Red);
                    }
                    else
                    {
                        settingsView.SetSessionKey("Granted", Color.Green);
                    }
                };

            if (!String.IsNullOrWhiteSpace(existingToken))
                setToken(existingToken);

            if (!String.IsNullOrWhiteSpace(existingSessionKey))
                setSessionKey(existingSessionKey);

            Action updateUserName = () =>
            {
                setUserName(settingsView.UserName);
            };

            Action requestToken = () =>
                {
                    form.Log("Requesting token");
                    var response = service.GetToken();
                    if (response.Success)
                    {
                        var token = response.Data;
                        form.Log(String.Format("Received response, token : {0}", token));
                        setToken(token);
                    }
                    else
                    {
                        // TODO: error handling
                    }
                };

            Action confirmToken = () =>
            {
                string url = settingsView.ConfirmationLink;
                if (!Uri.IsWellFormedUriString(url, UriKind.Absolute))
                {
                    form.Log("Cannot create confirmation link, please create a new Token first");
                }
                else
                {
                    Process.Start(url);
                }
            };

            Action requestSession = () =>
                {
                    var currentToken = Properties.Settings.Default.LastFMUserToken;
                    if (String.IsNullOrWhiteSpace(currentToken))
                    {
                        form.Log("Need a (confirmed) token to issue session request");
                    }
                    else
                    {
                        form.Log("Requesting session");
                        try
                        {
                            var response = service.GetSession(currentToken);
                            form.Log("Response OK");

                            setSessionKey(response.Data.Key);
                            form.Log("Session key saved");
                        }
                        catch (Exception ex)
                        {
                            form.Log(ex.Message);
                        }
                    }
                };

            Action done = () => form.Close();

            form.UserRequestsGetToken += requestToken;
            form.UserRequestsConfirmToken += confirmToken;
            form.UserRequestsInitiateSession += requestSession;
            form.UserRequestsDone += done;
            settingsView.UserNameChanged += updateUserName;

            form.ShowDialog();

            form.UserRequestsGetToken -= requestToken;
            form.UserRequestsConfirmToken -= confirmToken;
            form.UserRequestsInitiateSession -= requestSession;
            form.UserRequestsDone -= done;
            settingsView.UserNameChanged -= updateUserName;
        }
        
    }
}
