﻿namespace next.Interaction.LastFMConnector
{
    partial class LastFMForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pfR_H1Label1 = new next.Interaction.Theme.ThemedH1Label();
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.buttonGetToken = new next.Interaction.Theme.ThemedButton();
            this.buttonConfirmToken = new next.Interaction.Theme.ThemedButton();
            this.buttonInitiateSession = new next.Interaction.Theme.ThemedButton();
            this.lastFMSettingsView1 = new next.Interaction.LastFMConnector.LastFMSettingsView();
            this.logBox1 = new Kit.LogBox();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.buttonDone = new next.Interaction.Theme.ThemedButton();
            this.tableLayoutPanel.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // pfR_H1Label1
            // 
            this.pfR_H1Label1.AutoSize = true;
            this.pfR_H1Label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.SetFlowBreak(this.pfR_H1Label1, true);
            this.pfR_H1Label1.FontSizeFactor = 1.25F;
            this.pfR_H1Label1.ForeColor = System.Drawing.Color.Silver;
            this.pfR_H1Label1.Location = new System.Drawing.Point(3, 0);
            this.pfR_H1Label1.Margin = new System.Windows.Forms.Padding(3, 0, 3, 12);
            this.pfR_H1Label1.Name = "pfR_H1Label1";
            this.pfR_H1Label1.Size = new System.Drawing.Size(129, 36);
            this.pfR_H1Label1.TabIndex = 1;
            this.pfR_H1Label1.Text = "Last FM Setup";
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel.ColumnCount = 1;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.Controls.Add(this.flowLayoutPanel1, 0, 0);
            this.tableLayoutPanel.Controls.Add(this.lastFMSettingsView1, 0, 1);
            this.tableLayoutPanel.Controls.Add(this.logBox1, 0, 2);
            this.tableLayoutPanel.Controls.Add(this.flowLayoutPanel2, 0, 3);
            this.tableLayoutPanel.Location = new System.Drawing.Point(12, 14);
            this.tableLayoutPanel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 4;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.Size = new System.Drawing.Size(690, 534);
            this.tableLayoutPanel.TabIndex = 2;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoSize = true;
            this.flowLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.flowLayoutPanel1.Controls.Add(this.pfR_H1Label1);
            this.flowLayoutPanel1.Controls.Add(this.buttonGetToken);
            this.flowLayoutPanel1.Controls.Add(this.buttonConfirmToken);
            this.flowLayoutPanel1.Controls.Add(this.buttonInitiateSession);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(3, 4);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(440, 96);
            this.flowLayoutPanel1.TabIndex = 3;
            // 
            // buttonGetToken
            // 
            this.buttonGetToken.AutoSize = true;
            this.buttonGetToken.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonGetToken.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonGetToken.ForeColor = System.Drawing.Color.Silver;
            this.buttonGetToken.Location = new System.Drawing.Point(3, 52);
            this.buttonGetToken.Margin = new System.Windows.Forms.Padding(3, 4, 3, 12);
            this.buttonGetToken.MinimumSize = new System.Drawing.Size(100, 0);
            this.buttonGetToken.Name = "buttonGetToken";
            this.buttonGetToken.Size = new System.Drawing.Size(100, 32);
            this.buttonGetToken.TabIndex = 2;
            this.buttonGetToken.Text = "New Token";
            this.buttonGetToken.UseVisualStyleBackColor = true;
            // 
            // buttonConfirmToken
            // 
            this.buttonConfirmToken.AutoSize = true;
            this.buttonConfirmToken.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonConfirmToken.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonConfirmToken.ForeColor = System.Drawing.Color.Silver;
            this.buttonConfirmToken.Location = new System.Drawing.Point(109, 52);
            this.buttonConfirmToken.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.buttonConfirmToken.MinimumSize = new System.Drawing.Size(100, 0);
            this.buttonConfirmToken.Name = "buttonConfirmToken";
            this.buttonConfirmToken.Size = new System.Drawing.Size(118, 32);
            this.buttonConfirmToken.TabIndex = 3;
            this.buttonConfirmToken.Text = "Confirm Token";
            this.buttonConfirmToken.UseVisualStyleBackColor = true;
            // 
            // buttonInitiateSession
            // 
            this.buttonInitiateSession.AutoSize = true;
            this.buttonInitiateSession.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonInitiateSession.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonInitiateSession.ForeColor = System.Drawing.Color.Silver;
            this.buttonInitiateSession.Location = new System.Drawing.Point(233, 52);
            this.buttonInitiateSession.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.buttonInitiateSession.MinimumSize = new System.Drawing.Size(100, 0);
            this.buttonInitiateSession.Name = "buttonInitiateSession";
            this.buttonInitiateSession.Size = new System.Drawing.Size(204, 32);
            this.buttonInitiateSession.TabIndex = 4;
            this.buttonInitiateSession.Text = "Initiate session (permanent)";
            this.buttonInitiateSession.UseVisualStyleBackColor = true;
            // 
            // lastFMSettingsView1
            // 
            this.lastFMSettingsView1.AutoSize = true;
            this.lastFMSettingsView1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.lastFMSettingsView1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lastFMSettingsView1.ConfirmationLink = "";
            this.lastFMSettingsView1.Location = new System.Drawing.Point(0, 116);
            this.lastFMSettingsView1.Margin = new System.Windows.Forms.Padding(0, 12, 0, 0);
            this.lastFMSettingsView1.Name = "lastFMSettingsView1";
            this.lastFMSettingsView1.Size = new System.Drawing.Size(694, 222);
            this.lastFMSettingsView1.TabIndex = 5;
            this.lastFMSettingsView1.Token = "";
            this.lastFMSettingsView1.UserName = "";
            // 
            // logBox1
            // 
            this.logBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.logBox1.BackColor = System.Drawing.Color.LightGray;
            this.logBox1.Font = new System.Drawing.Font("Courier New", 7.8F);
            this.logBox1.Location = new System.Drawing.Point(3, 347);
            this.logBox1.Margin = new System.Windows.Forms.Padding(3, 9, 3, 4);
            this.logBox1.MinimumSize = new System.Drawing.Size(0, 100);
            this.logBox1.Name = "logBox1";
            this.logBox1.ReadOnly = true;
            this.logBox1.Size = new System.Drawing.Size(688, 137);
            this.logBox1.TabIndex = 4;
            this.logBox1.Text = "";
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel2.AutoSize = true;
            this.flowLayoutPanel2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.flowLayoutPanel2.Controls.Add(this.buttonDone);
            this.flowLayoutPanel2.Location = new System.Drawing.Point(3, 491);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.flowLayoutPanel2.Size = new System.Drawing.Size(688, 40);
            this.flowLayoutPanel2.TabIndex = 6;
            // 
            // buttonDone
            // 
            this.buttonDone.AutoSize = true;
            this.buttonDone.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonDone.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.buttonDone.FlatAppearance.BorderSize = 2;
            this.buttonDone.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gray;
            this.buttonDone.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonDone.ForeColor = System.Drawing.Color.Silver;
            this.buttonDone.Location = new System.Drawing.Point(585, 3);
            this.buttonDone.MinimumSize = new System.Drawing.Size(100, 0);
            this.buttonDone.Name = "buttonDone";
            this.buttonDone.Size = new System.Drawing.Size(100, 34);
            this.buttonDone.TabIndex = 0;
            this.buttonDone.Text = "Done";
            this.buttonDone.UseVisualStyleBackColor = true;
            this.buttonDone.Click += new System.EventHandler(this.buttonDone_Click);
            // 
            // LastFMForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(714, 561);
            this.Controls.Add(this.tableLayoutPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.MinimumSize = new System.Drawing.Size(721, 40);
            this.Name = "LastFMForm";
            this.Text = "";
            this.tableLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.flowLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private next.Interaction.Theme.ThemedH1Label pfR_H1Label1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private next.Interaction.Theme.ThemedButton buttonGetToken;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private Kit.LogBox logBox1;
        private next.Interaction.Theme.ThemedButton buttonConfirmToken;
        private next.Interaction.Theme.ThemedButton buttonInitiateSession;
        private LastFMSettingsView lastFMSettingsView1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private Theme.ThemedButton buttonDone;

    }
}
