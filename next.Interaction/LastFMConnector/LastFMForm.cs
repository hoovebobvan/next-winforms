﻿using Kit;
using System;
using System.Drawing;

namespace next.Interaction.LastFMConnector
{
    public partial class LastFMForm : next.Interaction.BaseForm
    {
        public LastFMForm()
        {
            InitializeComponent();
            buttonGetToken.Click += buttonGetToken_Click;
            buttonConfirmToken.Click += buttonConfirmToken_Click;
            buttonInitiateSession.Click += buttonInitiateSession_Click;
        }

        public event Action UserRequestsGetToken;
        public event Action UserRequestsConfirmToken;
        public event Action UserRequestsInitiateSession;
        public event Action UserRequestsDone;

        void buttonGetToken_Click(object sender, EventArgs e)
        {
            UserRequestsGetToken.Raise();
        }

        void buttonConfirmToken_Click(object sender, EventArgs e)
        {
            UserRequestsConfirmToken.Raise();
        }

        void buttonInitiateSession_Click(object sender, EventArgs e)
        {
            UserRequestsInitiateSession.Raise();
        }

        public void Log(string message)
        {
            this.logBox1.Log(message);
        }

        public LastFMSettingsView SettingsView { get { return lastFMSettingsView1; } }

        private void buttonDone_Click(object sender, EventArgs e)
        {
            UserRequestsDone.Raise();
        }

    }
}
