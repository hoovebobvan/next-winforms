﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Kit;
using next.Interaction.Theme;

namespace next.Interaction.LastFMConnector
{
    public partial class LastFMSettingsView : ThemedUserControl
    {
        public LastFMSettingsView()
        {
            InitializeComponent();
            textboxUsername.TextChanged += textboxUsername_TextChanged;
        }

        void textboxUsername_TextChanged(object sender, EventArgs e)
        {
            UserNameChanged.Raise();
        }

        public event Action UserNameChanged;

        public string UserName
        {
            get { return textboxUsername.Text; }
            set 
            {
                var oldValue = textboxUsername.Text;
                if (value != oldValue)
                {
                    textboxUsername.Text = value;
                }
            }
        }

        public string Token
        {
            get { return textboxToken.Text; }
            set { textboxToken.Text = value; }
        }

        public string ConfirmationLink
        {
            get { return textboxConfirmationLink.Text; }
            set { textboxConfirmationLink.Text = value; }
        }

        public void SetSessionKey(string p, Color color)
        {
            textBoxSessionKey.ForeColor = color;
            textBoxSessionKey.Text = p;
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            textboxUsername.TextChanged -= textboxUsername_TextChanged;

            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}
