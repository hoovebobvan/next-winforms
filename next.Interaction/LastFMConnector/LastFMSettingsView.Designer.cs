﻿namespace next.Interaction.LastFMConnector
{
    partial class LastFMSettingsView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pfR_Label3 = new next.Interaction.Theme.ThemedLabel();
            this.pfR_Label2 = new next.Interaction.Theme.ThemedLabel();
            this.pfR_Label1 = new next.Interaction.Theme.ThemedLabel();
            this.textBoxSessionKey = new next.Interaction.Theme.ThemedTextBox();
            this.textboxConfirmationLink = new next.Interaction.Theme.ThemedTextBox();
            this.pfR_Label4 = new next.Interaction.Theme.ThemedLabel();
            this.pfR_Label5 = new next.Interaction.Theme.ThemedLabel();
            this.textboxToken = new next.Interaction.Theme.ThemedTextBox();
            this.pfR_Label6 = new next.Interaction.Theme.ThemedLabel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.pfR_Label7 = new next.Interaction.Theme.ThemedLabel();
            this.textboxUsername = new next.Interaction.Theme.ThemedTextBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pfR_Label3
            // 
            this.pfR_Label3.AutoSize = true;
            this.pfR_Label3.FontSizeFactor = 1F;
            this.pfR_Label3.Location = new System.Drawing.Point(-383, 119);
            this.pfR_Label3.Name = "pfR_Label3";
            this.pfR_Label3.Size = new System.Drawing.Size(84, 17);
            this.pfR_Label3.TabIndex = 7;
            this.pfR_Label3.Text = "Session key";
            // 
            // pfR_Label2
            // 
            this.pfR_Label2.AutoSize = true;
            this.pfR_Label2.FontSizeFactor = 1F;
            this.pfR_Label2.Location = new System.Drawing.Point(-383, 50);
            this.pfR_Label2.Name = "pfR_Label2";
            this.pfR_Label2.Size = new System.Drawing.Size(112, 17);
            this.pfR_Label2.TabIndex = 8;
            this.pfR_Label2.Text = "Confirmation link";
            // 
            // pfR_Label1
            // 
            this.pfR_Label1.AutoSize = true;
            this.pfR_Label1.FontSizeFactor = 1F;
            this.pfR_Label1.Location = new System.Drawing.Point(-383, 16);
            this.pfR_Label1.Name = "pfR_Label1";
            this.pfR_Label1.Size = new System.Drawing.Size(48, 17);
            this.pfR_Label1.TabIndex = 5;
            this.pfR_Label1.Text = "Token";
            // 
            // textBoxSessionKey
            // 
            this.textBoxSessionKey.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxSessionKey.Location = new System.Drawing.Point(174, 142);
            this.textBoxSessionKey.Margin = new System.Windows.Forms.Padding(3, 3, 3, 10);
            this.textBoxSessionKey.Name = "textBoxSessionKey";
            this.textBoxSessionKey.ReadOnly = true;
            this.textBoxSessionKey.Size = new System.Drawing.Size(510, 22);
            this.textBoxSessionKey.TabIndex = 14;
            // 
            // textboxConfirmationLink
            // 
            this.textboxConfirmationLink.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textboxConfirmationLink.Location = new System.Drawing.Point(174, 73);
            this.textboxConfirmationLink.Margin = new System.Windows.Forms.Padding(3, 3, 3, 10);
            this.textboxConfirmationLink.Multiline = true;
            this.textboxConfirmationLink.Name = "textboxConfirmationLink";
            this.textboxConfirmationLink.ReadOnly = true;
            this.textboxConfirmationLink.Size = new System.Drawing.Size(510, 56);
            this.textboxConfirmationLink.TabIndex = 13;
            // 
            // pfR_Label4
            // 
            this.pfR_Label4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)));
            this.pfR_Label4.AutoSize = true;
            this.pfR_Label4.Location = new System.Drawing.Point(3, 145);
            this.pfR_Label4.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.pfR_Label4.Name = "pfR_Label4";
            this.pfR_Label4.Size = new System.Drawing.Size(84, 29);
            this.pfR_Label4.TabIndex = 11;
            this.pfR_Label4.Text = "Session key";
            // 
            // pfR_Label5
            // 
            this.pfR_Label5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)));
            this.pfR_Label5.AutoSize = true;
            this.pfR_Label5.FontSizeFactor = 1F;
            this.pfR_Label5.Location = new System.Drawing.Point(3, 76);
            this.pfR_Label5.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.pfR_Label5.Name = "pfR_Label5";
            this.pfR_Label5.Size = new System.Drawing.Size(112, 63);
            this.pfR_Label5.TabIndex = 12;
            this.pfR_Label5.Text = "Confirmation link";
            // 
            // textboxToken
            // 
            this.textboxToken.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textboxToken.FontSizeFactor = 1F;
            this.textboxToken.Location = new System.Drawing.Point(174, 38);
            this.textboxToken.Margin = new System.Windows.Forms.Padding(3, 3, 3, 10);
            this.textboxToken.Name = "textboxToken";
            this.textboxToken.ReadOnly = true;
            this.textboxToken.Size = new System.Drawing.Size(510, 22);
            this.textboxToken.TabIndex = 10;
            // 
            // pfR_Label6
            // 
            this.pfR_Label6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)));
            this.pfR_Label6.AutoSize = true;
            this.pfR_Label6.FontSizeFactor = 1F;
            this.pfR_Label6.Location = new System.Drawing.Point(3, 41);
            this.pfR_Label6.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.pfR_Label6.Name = "pfR_Label6";
            this.pfR_Label6.Size = new System.Drawing.Size(48, 29);
            this.pfR_Label6.TabIndex = 9;
            this.pfR_Label6.Text = "Token";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 75F));
            this.tableLayoutPanel1.Controls.Add(this.pfR_Label6, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.textBoxSessionKey, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.textboxToken, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.pfR_Label4, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.textboxConfirmationLink, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.pfR_Label5, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.pfR_Label7, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.textboxUsername, 1, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(687, 174);
            this.tableLayoutPanel1.TabIndex = 15;
            // 
            // pfR_Label7
            // 
            this.pfR_Label7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)));
            this.pfR_Label7.AutoSize = true;
            this.pfR_Label7.FontSizeFactor = 1F;
            this.pfR_Label7.Location = new System.Drawing.Point(3, 6);
            this.pfR_Label7.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.pfR_Label7.Name = "pfR_Label7";
            this.pfR_Label7.Size = new System.Drawing.Size(73, 29);
            this.pfR_Label7.TabIndex = 9;
            this.pfR_Label7.Text = "Username";
            // 
            // textboxUsername
            // 
            this.textboxUsername.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textboxUsername.BackColor = System.Drawing.Color.WhiteSmoke;
            this.textboxUsername.FontSizeFactor = 1F;
            this.textboxUsername.Location = new System.Drawing.Point(174, 3);
            this.textboxUsername.Margin = new System.Windows.Forms.Padding(3, 3, 3, 10);
            this.textboxUsername.Name = "textboxUsername";
            this.textboxUsername.Size = new System.Drawing.Size(510, 22);
            this.textboxUsername.TabIndex = 10;
            // 
            // LastFMSettingsView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.pfR_Label3);
            this.Controls.Add(this.pfR_Label2);
            this.Controls.Add(this.pfR_Label1);
            this.Name = "LastFMSettingsView";
            this.Size = new System.Drawing.Size(693, 180);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private next.Interaction.Theme.ThemedLabel pfR_Label3;
        private next.Interaction.Theme.ThemedLabel pfR_Label2;
        private next.Interaction.Theme.ThemedLabel pfR_Label1;
        private next.Interaction.Theme.ThemedTextBox textBoxSessionKey;
        private next.Interaction.Theme.ThemedTextBox textboxConfirmationLink;
        private next.Interaction.Theme.ThemedLabel pfR_Label4;
        private next.Interaction.Theme.ThemedLabel pfR_Label5;
        private next.Interaction.Theme.ThemedTextBox textboxToken;
        private next.Interaction.Theme.ThemedLabel pfR_Label6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private next.Interaction.Theme.ThemedLabel pfR_Label7;
        private next.Interaction.Theme.ThemedTextBox textboxUsername;
    }
}
