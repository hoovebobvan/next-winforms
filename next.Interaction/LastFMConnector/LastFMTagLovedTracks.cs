﻿using Kit;
using Newtonsoft.Json.Linq;
using next.Exchange.LastFM.LovedTracks;
using next.Interaction.Properties;
using next.Interaction.Settings;
using next.Store;
using next.Store.TrackFiles;
using next.Tracks;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace next.Interaction.LastFMConnector
{
    public class LastFMTagLovedTracks 
    {
        private TrackFileCollection collection;
        private GetLovedTracks getLovedTracks;
        private Func<string> getUsername;

        public LastFMTagLovedTracks(TrackFileCollection collection, GetLovedTracks getLovedTracks, Func<string> getUsername)
        {
            if (collection == null || getLovedTracks == null) throw new ArgumentNullException();

            this.collection = collection;
            this.getLovedTracks = getLovedTracks;
            this.getUsername = getUsername;
        }

        public async void Invoke()
        {
            try
            {
                string username = getUsername();

                if (string.IsNullOrWhiteSpace(username))
                {
                    MessageBox.Show("You need to provide you LastFM username (under 'Settings')");
                }
                else
                {
                    var matchResult = await Execute(username);
                    int updateCount = 0;

                    foreach (var track in collection)
                    {
                        if (matchResult.Matches.Contains(track))
                        {
                            if (track.AddLike(username))
                                updateCount++;
                        }
                        else
                        {
                            if (track.RemoveLike(username))
                                updateCount++;
                        }
                    }

                    string message = String.Format(resultMessageFormat,
                        matchResult.EntryCount, updateCount, matchResult.NotFound.Count(), matchResult.Exceptions.Count());

                    MessageBox.Show(message);
                }
            }
            catch (Exception ex)
            {
                Debug.Write(ex.ToString());
                MessageBox.Show("An exception occured");
            }
        }

        public async Task<MatchWithCollection.Result> Execute(string username)
        {                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            
            var lovedTracks = await getLovedTracks.Invoke(username);
            var matchResult = MatchWithCollection.Execute(collection, lovedTracks);           
            return matchResult;
        }

        private static string resultMessageFormat = String.Join(Environment.NewLine, new string[]
        {
            "{0} entries processed",
            "{1} trackfiles were updated",
            "{2} loved tracks were not found",
            "{3} exceptions occured"
        });

    }
}
