﻿using Newtonsoft.Json;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace next.Store.TrackFiles
{
    public class SaveTrackFileCollection
    {
        public Task Execute(TaskScheduler scheduler, TrackFileCollection collection, DirectoryInfo targetDirectoryInfo)
        {
            return Task.Factory.StartNew(_ =>
            {
                ExecuteSync(collection, targetDirectoryInfo);
            },
            scheduler);
        }

        public void ExecuteSync(TrackFileCollection collection, DirectoryInfo targetDirectoryInfo)
        {
            var filePath = Path.Combine(targetDirectoryInfo.FullName, LoadTrackFileCollection.CollectionFileName);

            using (var writer = new StreamWriter(filePath, append: false, encoding: Encoding.UTF8))
            {
                var jsonDTOs = collection.Select(trackFile => TrackFileDTO.ToDTO(trackFile));
                writer.Write(JsonConvert.SerializeObject(jsonDTOs, Formatting.Indented));
            }
        }
    }
}
