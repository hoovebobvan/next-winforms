﻿using next.Tonality;
using next.Tracks;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace next.Store.TrackFiles
{
    public class TrackFileDTO
    {
        public string artist { get; set; }
        public string title { get; set; }
        public string release_name { get; set; }
        public string release_track { get; set; }
        public string filepath { get; set; }
        public double? bpm { get; set; }
        public int? root_note { get; set; }
        public string dm_tonality { get; set; }
        public DateTime? importDate { get; set; }
        public List<string> liked_by { get; set; }
        public List<string> tags { get; set; }

        // not used for revival but may be of use to those interested consuming the json files
        public double? n100_root_note { get; set; }
        public string open_key_code { get; set; }

        public static TrackFileDTO ToDTO(TrackFile trackFile)
        {
            return new TrackFileDTO
            {
                title = trackFile.Title,
                artist = trackFile.Artist,
                release_name = trackFile.ReleaseTitle,
                release_track = trackFile.ReleaseTrackId,
                filepath = trackFile.FileInfo.FullName,
                bpm = trackFile.BPM > 0 ? (double?) trackFile.BPM : null,
                root_note = trackFile.Key != null ? (int?)trackFile.Key.Note : null,
                dm_tonality = trackFile.Key != null ? trackFile.Key.Tonality.Name : String.Empty,
                n100_root_note = trackFile.N100Key != null ? (double?)trackFile.N100Key.RootNote : null,
                open_key_code = trackFile.OpenKeyCode != null ? trackFile.OpenKeyCode.ToString() : String.Empty,
                importDate = trackFile.ImportDate,
                liked_by = trackFile.Likes.ToList(),
                tags = trackFile.Tags.ToList()
            };
        }

        public static TrackFile ToTrackFile(TrackFileDTO dto)
        {
            // TODO: This should be a single constructor call

            DMKey rootKey = dto.root_note != null && DMTonality.Names.Contains(dto.dm_tonality)
                ? new DMKey(new Note12((int)dto.root_note), DMTonality.PerName(dto.dm_tonality))
                : null;

            var track = new TrackFile(
                new FileInfo(dto.filepath),
                dto.artist,
                dto.title,
                dto.release_name,
                dto.release_track,
                dto.bpm != null ? (double)dto.bpm : 0,
                rootKey,
                importDate: dto.importDate                  
            );

            if (rootKey != null) 
            {
                var rootKeyOKC = ((OpenKeyCode)rootKey).ToString();
                if (rootKeyOKC != dto.open_key_code)
                {
                    Debug.WriteLine(String.Format("Mismatch for dmkey {0} vs open_key_code {1} -- {2} - {3}", rootKeyOKC, dto.open_key_code, dto.artist, dto.title));
                }
            }

            if (dto.liked_by != null && dto.liked_by.Any())
            {
                foreach(var name in dto.liked_by)
                    track.AddLike(name);
            }

            if (dto.tags != null && dto.tags.Any())
            {
                foreach (var tag in dto.tags)
                    track.AddTag(tag);
            }

            return track;
        }
    }
}
