﻿using next.Store.TextIndex;
using System;

namespace next.Store.TrackFiles
{
    public class TrackFileDataContext : IDisposable
    {
        private TrackFileCollection collection;
        private TrackFileIndex trackFileIndex;
        private TrackFileIndexConnector trackFileIndexConnector;

        public TrackFileDataContext(TrackFileCollection collection, TrackFileIndex trackFileIndex, TrackFileIndexConnector trackFileIndexConnector)
        {
            this.collection = collection;
            this.trackFileIndex = trackFileIndex;
            this.trackFileIndexConnector = trackFileIndexConnector;
        }

        public TrackFileIndex TrackFileIndex
        {
            get { return trackFileIndex; }
        }

        public TrackFileIndexConnector TrackFileIndexConnector
        {
            get { return trackFileIndexConnector; }
        }

        public TrackFileCollection Collection
        {
            get { return collection; }
        }

        public void Dispose()
        {
            trackFileIndexConnector.Dispose();
            collection = null;
            trackFileIndex = null;
            collection = null;
        }
    }
}
