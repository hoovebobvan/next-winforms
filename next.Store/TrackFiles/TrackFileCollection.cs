﻿using next.Tracks;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace next.Store.TrackFiles
{
    public class TrackFileCollection : ICollection<TrackFile>, INotifyCollectionChanged
    {
        private object changeLock = new object();

        public ISet<string> AllFans = new SortedSet<string>();
        public ISet<string> AllTags = new SortedSet<string>();

        private Dictionary<string, TrackFile> tracksPerFilePath = 
            new Dictionary<string, TrackFile>();

        // TODO : Refactor, make a class that works like a Lookup rather than the mumbo jumbo that's going on with this object
        private Dictionary<string, List<TrackFile>> tracksPerCanonicalName = 
            new Dictionary<string, List<TrackFile>>();
        
        private SortedSet<TrackFile> allTracks = new SortedSet<TrackFile>();

        public TrackFileCollection(IEnumerable<TrackFile> tracks = null)
        {
            tracks = tracks ?? new List<TrackFile>();
            UpsertMany(tracks);
        }

        private void SubmitTrackFile(TrackFile trackFile)
        {
            UpsertMany(new TrackFile[] { trackFile });
        }

        public void UpsertMany(IEnumerable<TrackFile> trackFiles)
        {
            // TODO: Split methods

            lock (changeLock)
            {
                foreach (var trackFile in trackFiles.Where(tf => tf.Key != null))
                {
                    if (!tracksPerFilePath.ContainsKey(trackFile.FileInfo.FullName))
                    {
                        allTracks.Add(trackFile);
                        tracksPerFilePath[trackFile.FileInfo.FullName] = trackFile;

                        if (tracksPerCanonicalName.ContainsKey(trackFile.CanonicalName))
                        {
                            tracksPerCanonicalName[trackFile.CanonicalName].Add(trackFile);
                        }
                        else
                        {
                            tracksPerCanonicalName[trackFile.CanonicalName] = new List<TrackFile> { trackFile };
                        }

                        if (trackFile.IsLiked)
                            foreach (var fan in trackFile.Likes)
                                AllFans.Add(fan);

                        if (trackFile.Tags != null && trackFile.Tags.Any())
                            foreach (var tag in trackFile.Tags)
                                AllTags.Add(tag);

                        RaiseCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, trackFile));
                    }
                    else
                    {
                        var existing = tracksPerFilePath[trackFile.FileInfo.FullName];

                        bool bpmMismatch = existing.BPM != trackFile.BPM;
                        bool keyMismatch = existing.Key != trackFile.Key;

                        if (keyMismatch)
                        {
                            Debug.WriteLine(String.Format("Key mismatch; old {0}; new {1}; {2} - {3}", existing.Key, trackFile.Key, existing.Artist ?? "", existing.Title ?? ""));
                        }

                        if (bpmMismatch || keyMismatch)
                        {                          
                            tracksPerFilePath[trackFile.FileInfo.FullName] = trackFile;

                            if (tracksPerCanonicalName.ContainsKey(trackFile.CanonicalName))
                            {
                                tracksPerCanonicalName[trackFile.CanonicalName].Remove(existing);
                                tracksPerCanonicalName[trackFile.CanonicalName].Add(trackFile);
                            }
                            else
                            {
                                tracksPerCanonicalName[trackFile.CanonicalName] = new List<TrackFile> { trackFile };
                            }
                            RaiseCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, existing));
                            RaiseCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, trackFile));
                        }

                    }

                }
            }
        }

        public TrackFile GetByFileInfo(FileInfo fileInfo)
        {
            return GetByPath(fileInfo.FullName);
        }

        public TrackFile GetByPath(string path)
        {
            return tracksPerFilePath.ContainsKey(path) ? tracksPerFilePath[path] : null;
        }

        public IEnumerable<TrackFile> GetByCanonicalName(string canonicalName)
        {
            return tracksPerCanonicalName.ContainsKey(canonicalName)
                ? tracksPerCanonicalName[canonicalName]
                : new List<TrackFile>();
        }

        public void Add(TrackFile item)
        {
            SubmitTrackFile(item);
        }

        public void Clear()
        {
            allTracks.Clear();
            tracksPerFilePath.Clear();
            tracksPerCanonicalName.Clear();
            RaiseCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        public bool Contains(TrackFile item)
        {
            return allTracks.Contains(item);
        }

        public void CopyTo(TrackFile[] array, int arrayIndex)
        {
            allTracks.Skip(arrayIndex).ToList().CopyTo(array);
        }

        public int Count
        {
            get { return allTracks.Count; }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        public IEnumerable<TrackFile> GetOuterSection(ISet<string> filenames)
        {
            return allTracks.Where(track => !filenames.Contains(track.FilePath));
        }

        public bool Remove(TrackFile item)
        {
            bool wasRemoved = allTracks.Remove(item);
            if (wasRemoved)
            {
                tracksPerFilePath.Remove(item.FileInfo.FullName);

                if (tracksPerCanonicalName.ContainsKey(item.CanonicalName))
                {
                    tracksPerCanonicalName[item.CanonicalName].Remove(item);
                }

                RaiseCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, item));
            }
            return wasRemoved;
        }

        public IEnumerator<TrackFile> GetEnumerator()
        {
            return allTracks.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return allTracks.GetEnumerator();
        }

        public event NotifyCollectionChangedEventHandler CollectionChanged;

        private void RaiseCollectionChanged(NotifyCollectionChangedEventArgs eventArgs)
        {
            CollectionChanged?.Invoke(this, eventArgs);
        }
    }
}
