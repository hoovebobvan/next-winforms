﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace next.Store.TrackFiles
{
    public class LoadTrackFileCollection
    {
        // TODO : Move this outta here, use FileInfo instead
        public static string CollectionFileName = "next-track-collection.json";

        // TODO : Make result object containing success indication and possible errors
        public async Task<bool> ExecuteAsync(TaskScheduler scheduler, TrackFileCollection collection, FileInfo source)
        {
            return await Task.Factory.StartNew(_ => Execute(collection, source), scheduler);
        }

        public bool Execute(TrackFileCollection collection, FileInfo source)
        {
            try
            {
                if (source.Exists)
                {
                    using (var reader = new StreamReader(source.FullName, Encoding.UTF8))
                    {
                        var trackDTOs = JsonConvert.DeserializeObject<IEnumerable<TrackFileDTO>>(reader.ReadToEnd());

                        var trackFiles = trackDTOs.Select(dto => TrackFileDTO.ToTrackFile(dto));
                        collection.UpsertMany(trackFiles);
                    }
                    return true;
                }

            }
            catch
            {
            }
            return false;

        }

    }
}
