﻿using next.Tracks;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;

namespace next.Store
{
    public class TrackFileCollection : ICollection<TrackFile>, INotifyCollectionChanged
    {
        private Dictionary<string, TrackFile> tracksPerFilePath = new Dictionary<string, TrackFile>();

        // TODO : Refactor, make a class that works like a Lookup rather than the mumbo jumbo that's going on with this object
        private Dictionary<string, List<TrackFile>> tracksPerCanonicalName = new Dictionary<string, List<TrackFile>>();
        
        private SortedSet<TrackFile> allTracks = new SortedSet<TrackFile>();

        public TrackFileCollection(IEnumerable<TrackFile> tracks = null)
        {
            tracks = tracks ?? new List<TrackFile>();
            SubmitTrackFiles(tracks);
        }

        private void SubmitTrackFile(TrackFile trackFile)
        {
            SubmitTrackFiles(new TrackFile[] { trackFile });
        }

        public void SubmitTrackFiles(IEnumerable<TrackFile> trackFiles)
        {
            lock (changeLock)
            {
                foreach (var trackFile in trackFiles.Where(tf => tf.Key != null))
                {
                    if (!tracksPerFilePath.ContainsKey(trackFile.FileInfo.FullName))
                    {
                        allTracks.Add(trackFile);
                        tracksPerFilePath[trackFile.FileInfo.FullName] = trackFile;

                        if (tracksPerCanonicalName.ContainsKey(trackFile.CanonicalName))
                        {
                            tracksPerCanonicalName[trackFile.CanonicalName].Add(trackFile);
                        }
                        else
                        {
                            tracksPerCanonicalName[trackFile.CanonicalName] = new List<TrackFile> { trackFile };
                        }
                    }
                }
            }
            RaiseCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, trackFiles));
        }

        public TrackFile GetByFileInfo(FileInfo fileInfo)
        {
            return GetByPath(fileInfo.FullName);
        }

        public TrackFile GetByPath(string path)
        {
            return tracksPerFilePath.ContainsKey(path) ? tracksPerFilePath[path] : null;
        }

        public IEnumerable<TrackFile> GetByCanonicalName(string canonicalName)
        {
            return tracksPerCanonicalName.ContainsKey(canonicalName)
                ? tracksPerCanonicalName[canonicalName]
                : new List<TrackFile>();
        }

        private object changeLock = new object();

        public void Add(TrackFile item)
        {
            SubmitTrackFile(item);
        }

        public void AddMany(IEnumerable<TrackFile> trackFiles)
        {
            SubmitTrackFiles(trackFiles);
        }

        public void Clear()
        {
            allTracks.Clear();
            RaiseCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        public bool Contains(TrackFile item)
        {
            return allTracks.Contains(item);
        }

        public void CopyTo(TrackFile[] array, int arrayIndex)
        {
            allTracks.Skip(arrayIndex).ToList().CopyTo(array);
        }

        public int Count
        {
            get { return allTracks.Count; }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        public bool Remove(TrackFile item)
        {
            bool wasRemoved = allTracks.Remove(item);
            if (wasRemoved)
            {
                tracksPerFilePath.Remove(item.FileInfo.FullName);

                if (tracksPerCanonicalName.ContainsKey(item.CanonicalName))
                {
                    tracksPerCanonicalName[item.CanonicalName].Remove(item);
                    if (tracksPerCanonicalName[item.CanonicalName].Count == 0)
                    {
                        tracksPerCanonicalName[item.CanonicalName] = null;
                    }
                }

                RaiseCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, item));
            }
            return wasRemoved;
        }

        public IEnumerator<TrackFile> GetEnumerator()
        {
            return allTracks.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return allTracks.GetEnumerator();
        }

        public event NotifyCollectionChangedEventHandler CollectionChanged;

        private void RaiseCollectionChanged(NotifyCollectionChangedEventArgs eventArgs)
        {
            var copy = CollectionChanged;
            if (copy != null)
            {
                copy(this, eventArgs);
            }
        }
    }
}
