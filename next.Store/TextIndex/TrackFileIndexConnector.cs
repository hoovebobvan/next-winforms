﻿using next.Store.TrackFiles;
using System;
using System.Collections.Specialized;
using System.Linq;
using System.Reactive.Linq;
using System.Reactive.Subjects;

namespace next.Store.TextIndex
{
    public class TrackFileIndexConnector : IDisposable
    {
        private TrackFileCollection collection;
        private TrackFileIndex index;
        private IDisposable subscription;
        private object rebuildLock = new object();
        private Subject<bool> rebuildingSubject = new Subject<bool>();

        public TrackFileIndexConnector(TrackFileIndex index, TrackFileCollection collection)
        {
            this.index = index;
            this.collection = collection;

            subscription = Observable
                .FromEventPattern<NotifyCollectionChangedEventHandler, NotifyCollectionChangedEventArgs>
                (
                    h => collection.CollectionChanged += h,
                    h => collection.CollectionChanged -= h
                )
                .Throttle(TimeSpan.FromSeconds(1))
                .Select(eventArgs => true)
                .Subscribe(RebuildIndex);
        }

        private void RebuildIndex(bool dummy)
        {
            lock (rebuildLock)
            {
                rebuildingSubject.OnNext(true);
                index.Clear();
                index.AddMany(collection.ToList());
                rebuildingSubject.OnNext(false);
            }
        }

        public IObservable<bool> IsRebuilding 
        { 
            get { return rebuildingSubject.AsObservable(); } 
        }

        public void Dispose()
        {
            rebuildingSubject.OnCompleted();
            rebuildingSubject.Dispose();
            subscription.Dispose();
        }
    }
}
