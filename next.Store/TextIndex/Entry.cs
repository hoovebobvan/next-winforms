﻿
using next.Tracks;

namespace next.Store.TextIndex
{
    public class Entry
    {
        public string Identifier { get; set; }
        public string Artist { get; set; }
        public string Title { get; set; }
        public string ReleaseTitle { get; set; }
        public double BPM { get; set; }
        public string Path { get; set; }
        public string OpenKeyCode { get; set; }
        public string IsLovedTrack { get; set; }

        public static Entry FromTrackFile(TrackFile trackFile)
        {
            return new Entry
            {
                Identifier = trackFile.LuceneIdentifier,
                Artist = trackFile.Artist,
                Title = trackFile.Title,
                ReleaseTitle = trackFile.ReleaseTitle,
                BPM = trackFile.BPM,
                Path = trackFile.FileInfo.FullName,
                OpenKeyCode = trackFile.OpenKeyCode,
                IsLovedTrack = trackFile.IsLiked ? "true" : "false"
            };
        }
    }
}
