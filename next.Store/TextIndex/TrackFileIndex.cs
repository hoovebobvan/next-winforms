﻿using Lucene.Net.Analysis.Standard;
using Lucene.Net.Documents;
using Lucene.Net.Index;
using Lucene.Net.Linq.Fluent;
using Lucene.Net.Linq.Mapping;
using Lucene.Net.QueryParsers;
using Lucene.Net.Search;
using Lucene.Net.Store;
using next.Tracks;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using Version = Lucene.Net.Util.Version;

namespace next.Store.TextIndex
{
    public partial class TrackFileIndex
    {
        private DirectoryInfo pathToIndex;
        private Lucene.Net.Store.Directory directory;
        private IDocumentMapper<Entry> documentMapper;
        private string[] searchedFields;

        public TrackFileIndex(DirectoryInfo pathToIndex)
        {
            // Deliberately keeping the option to store the index on disk for debugging purposes
            if (pathToIndex == null) throw new ArgumentNullException();
            this.pathToIndex = pathToIndex;

            var mapping = GetMapping();
            this.documentMapper = mapping.ToDocumentMapper();

            this.directory = new RAMDirectory(); // GetFSDirectory();

            searchedFields = documentMapper.IndexedProperties.Where(s => s != "IsLovedTrack").ToArray();
        }

        public void AddMany(IEnumerable<TrackFile> trackFiles)
        {
            var analyzer = new StandardAnalyzer(Version.LUCENE_30);

            using (var indexWriter = new IndexWriter(directory, analyzer, IndexWriter.MaxFieldLength.UNLIMITED))
            {
                foreach (var trackFile in trackFiles)
                {
                    var entry = Entry.FromTrackFile(trackFile);
                    add(entry, indexWriter);
                }

                analyzer.Close();
            }
        }

        private void add(Entry entry, IndexWriter indexWriter)
        {
            var query = new TermQuery(new Term("Identifier", entry.Identifier));
            indexWriter.DeleteDocuments(query);

            var document = new Document();
            documentMapper.ToDocument(entry, document);

            indexWriter.AddDocument(document);
        }

        public void Delete(TrackFile trackFile)
        {
            var analyzer = new StandardAnalyzer(Version.LUCENE_30);
            using (var indexWriter = new IndexWriter(directory, analyzer, IndexWriter.MaxFieldLength.UNLIMITED))
            {
                // remove older index entry
                var searchQuery = new TermQuery(new Term("Identifier", trackFile.LuceneIdentifier));
                indexWriter.DeleteDocuments(searchQuery);

                // close handles
                analyzer.Close();
            }
        }

        public bool Clear()
        {
            try
            {
                var analyzer = new StandardAnalyzer(Version.LUCENE_30);
                using (var indexWriter = new IndexWriter(directory, analyzer, true, IndexWriter.MaxFieldLength.UNLIMITED))
                {
                    indexWriter.DeleteAll();

                    analyzer.Close();
                }
            }
            catch (Exception)
            {
                // TODO: Improve expection handling
                return false;
            }
            return true;
        }

        public void Optimize()
        {
            var analyzer = new StandardAnalyzer(Version.LUCENE_30);
            
            using (var indexWriter = new IndexWriter(directory, analyzer, IndexWriter.MaxFieldLength.UNLIMITED)) 
            {
                analyzer.Close();
                indexWriter.Optimize();                
            } 
        }

        // TODO : The query would better be a command object 
        public IEnumerable<Entry> Search(SearchCriteria criteria, int limit = 100)
        {
            using (var indexSearcher = new IndexSearcher(directory, false))
            {
                string queryString = criteria.Query.Trim();

                var analyzer = new StandardAnalyzer(Version.LUCENE_30);

                Query query = null;

                if (!String.IsNullOrEmpty(queryString))
                {
                    var parser = new MultiFieldQueryParser(
                        Version.LUCENE_30,
                        searchedFields,
                        analyzer);

                    try
                    {
                        query = parser.Parse(queryString.Trim());
                    }
                    catch (ParseException)
                    {
                        query = parser.Parse(QueryParser.Escape(queryString));
                    }
                }


                //QueryFilter falseFilter = new QueryFilter(new TermQuery(new Term("BOOL_FIELD", "0")));
                //searcher.search(query, falseFilter, maxResults);

                Query lovedTracksQuery = null;
                if (criteria.LovedTrack)
                {
                    lovedTracksQuery = new TermQuery(new Term("IsLovedTrack", "true"));
                }

                if (query == null)
                {
                    query = lovedTracksQuery;
                }

                if (lovedTracksQuery != null)
                {
                    var booleanQuery = new BooleanQuery();

                    booleanQuery.Add(lovedTracksQuery, Occur.MUST);
                    booleanQuery.Add(query, Occur.MUST);

                    query = booleanQuery;
                }

                if (query != null)
                {
                    var hits = indexSearcher
                    .Search(query, null, limit, Sort.RELEVANCE)
                    .ScoreDocs;

                    foreach (var hit in hits)
                    {
                        var entry = new Entry();
                        documentMapper.ToObject(indexSearcher.Doc(hit.Doc), null, entry);
                        yield return entry;
                    }
                }
            }
        }

        private ClassMap<Entry> GetMapping()
        {
            var mapping = new ClassMap<Entry>(Lucene.Net.Util.Version.LUCENE_30);

            mapping.Key(t => t.Identifier);
            mapping.Property(t => t.Path).NotIndexed().Stored();

            mapping.Property(t => t.Artist).Analyzed().Stored();
            mapping.Property(t => t.Title).Analyzed().Stored();
            mapping.Property(t => t.ReleaseTitle).Analyzed().Stored();
            mapping.Property(t => t.OpenKeyCode).Analyzed().Stored();
            mapping.Property(t => t.IsLovedTrack).Analyzed().Stored();
            mapping.Property(t => t.BPM).AsNumericField().WithPrecisionStep(8);

            return mapping;
        }

        public static TrackFileIndex Create(DirectoryInfo indexDirectoryInfo)
        {
            if (!indexDirectoryInfo.Exists)
            {
                indexDirectoryInfo.Create();
                
                // allow time for the directory to be available upon next request
                Thread.Sleep(100); 
            }

            return new TrackFileIndex(indexDirectoryInfo);
        }
    }
}
