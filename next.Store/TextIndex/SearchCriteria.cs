﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace next.Store.TextIndex
{
    public class SearchCriteria
    {
        public SearchCriteria(string query, bool lovedTrack)
        {
            this.Query = query;
            this.LovedTrack = lovedTrack;
        }

        public string Query { get; private set; }
        public bool LovedTrack { get; private set; }
    }
}
