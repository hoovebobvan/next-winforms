﻿using next.FollowUps;
using next.Store.TrackFiles;
using next.Tracks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace next.Store.FollowUps
{
    public class FollowUpCollection : IEnumerable<FollowUp>
    {
        private List<FollowUp> empty = new List<FollowUp>();

        private SortedSet<FollowUp> followUps = new SortedSet<FollowUp>();

        private Dictionary<string, SortedSet<FollowUp>> followUpsPerFilePath = new Dictionary<string, SortedSet<FollowUp>>();

        public void Insert(FollowUp followUp)
        {
            string fromName = followUp.From.FilePath;

            if (followUpsPerFilePath.ContainsKey(fromName))
            {
                followUpsPerFilePath[fromName].Add(followUp);
            }
            else
            {
                followUpsPerFilePath[fromName] = new SortedSet<FollowUp>(new FollowUp[] { followUp });
            }

            followUps.Add(followUp);
        }

        public bool Update(FollowUp followUp)
        {
            if (followUps.Contains(followUp))
            {
                followUps.Remove(followUp); // this looks odd by by virtue of IEquatable it will remove the old item
                followUps.Add(followUp);

                followUpsPerFilePath[followUp.From.FilePath].Remove(followUp);
                followUpsPerFilePath[followUp.From.FilePath].Add(followUp);

                return true;
            }

            return false;
        }

        public IEnumerable<FollowUp> GetMatchesFrom(TrackFile t)
        {
            return GetMatchesFrom(t.FilePath);
        }

        public IEnumerable<FollowUp> GetMatchesFrom(string fileName)
        {
            if (followUpsPerFilePath.ContainsKey(fileName))
            {
                return followUpsPerFilePath[fileName].AsEnumerable();
            }
            else
            {
                return empty;
            }
        }

        public IEnumerator<FollowUp> GetEnumerator()
        {
            return followUps.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return followUps.GetEnumerator();
        }

        public void UpSert(FollowUp followUp)
        {
            if (followUps.Contains(followUp))
            {
                Update(followUp);
            }
            else
            {
                Insert(followUp);
            }
        }

        public FollowUp Get(Tracks.TrackFile from, Tracks.TrackFile to)
        {
            if (followUps.Contains(new FollowUp(from, to)))
            {
                return followUpsPerFilePath[from.FilePath].FirstOrDefault(fu => to == fu.To);
            }
            return null;
        }
    }
}
