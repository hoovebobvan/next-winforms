﻿
using next.FollowUps;
using next.Tracks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace next.Store.FollowUps
{
    public class TrackDTO
    {
        public string filepath { get; set; }
        public string artist { get; set; }
        public string title { get; set; }
        public string release { get; set; }

        public static TrackDTO From(TrackFile trackFile)
        {
            var dto = new TrackDTO();
            
            dto.filepath = trackFile.FileInfo.FullName;
            dto.artist = trackFile.Artist;
            dto.title = trackFile.Title;
            dto.release = trackFile.ReleaseTitle;

            return dto;
        }
    }
}
