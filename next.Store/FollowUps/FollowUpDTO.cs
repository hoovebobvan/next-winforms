﻿using next.FollowUps;
using next.Store.TrackFiles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace next.Store.FollowUps
{
    public class FollowUpDTO
    {
        public TrackDTO track_from { get; set; }
        public TrackDTO track_to { get; set; }
        public int rating { get; set; }
        public string remarks { get; set; }

        public static FollowUpDTO From(FollowUp followUp)
        {
            var dto = new FollowUpDTO();

            dto.track_from = TrackDTO.From(followUp.From);
            dto.track_to = TrackDTO.From(followUp.To);
            dto.rating = (int)followUp.Rating;
            dto.remarks = followUp.Remarks;

            return dto;
        }

        public static FollowUp Revive(TrackFileCollection tracks, FollowUpDTO dto)
        {
            var from = tracks.GetByPath(dto.track_from.filepath);
            var to = tracks.GetByPath(dto.track_to.filepath);
            Rating rating;

            if (Enum.TryParse<Rating>(dto.rating.ToString(), out rating) && from != null && to != null)
            {
                return new FollowUp(from, to, rating, dto.remarks);
            }

            return null;
        }


    }
}
