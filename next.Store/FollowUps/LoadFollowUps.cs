﻿using Newtonsoft.Json;
using next.FollowUps;
using next.Store.TrackFiles;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace next.Store.FollowUps
{
    public class LoadFollowUps
    {
        public async Task<FollowUpCollection> ExecuteAsync(FollowUpCollection collection, FileInfo fileInfo, TrackFileCollection tracks)
        {
            var followUps = new FollowUpCollection();

            if (fileInfo.Exists)
            {
                using (var reader = new StreamReader(fileInfo.FullName))
                {
                    string jsonText = await reader.ReadToEndAsync();
                    var dtos = await Task.Factory.StartNew(() => JsonConvert.DeserializeObject<List<FollowUpDTO>>(jsonText));

                    foreach (var dto in dtos)
                    {
                        var followUp = FollowUpDTO.Revive(tracks, dto);
                        if (followUp != null)
                            followUps.Insert(followUp);
                    }
                }
            }
            
            return followUps;
        }

    }
}
