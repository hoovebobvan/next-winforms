﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace next.Store.FollowUps
{
    public class SaveFollowUps
    {
        public async Task<bool> Execute(FollowUpCollection collection, FileInfo destination)
        {
            var dtos = collection.Select(fu => FollowUpDTO.From(fu)).ToList();

            try
            {
                using (var writer = new StreamWriter(destination.FullName))
                {
                    var jsonText = JsonConvert.SerializeObject(dtos, Formatting.Indented);
                    await writer.WriteAsync(jsonText);
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
