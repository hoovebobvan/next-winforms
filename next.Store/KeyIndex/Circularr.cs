﻿using next.Modulo;
using System;
using System.Collections.Generic;

namespace next.Store.KeyIndex
{
    public class Circularr<T>
    {
        T[] items;

        public Circularr(T[] items)
        {
            this.items = items;
        }

        public T this[int index]
        {
            get { return items[index.Modulo(items.Length)]; }
        }

        public IEnumerable<T> GetSlice(int center, int maxDeviation)
        {
            if (maxDeviation < 0 || maxDeviation >= Math.Floor(items.Length / 2.0))
                throw new ArgumentOutOfRangeException();

            int current = center - maxDeviation;
            int amount = 2 * maxDeviation + 1;

            for (int x = amount; x > 0; x--)
            {
                yield return this[current];
                current += 1;
            }
        }
    }
}
