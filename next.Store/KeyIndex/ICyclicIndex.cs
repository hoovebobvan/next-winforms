﻿
namespace next.Store.KeyIndex
{
    interface ICyclicIndex<T, TNumeric>
    {
        System.Collections.Generic.IEnumerable<T> GetSection(TNumeric id);
        void Put(T t);
    }
}
