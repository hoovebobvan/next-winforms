﻿using next.Tonality;
using next.Tracks;
using System;
using System.Collections.Generic;

namespace next.Store.KeyIndex
{
    public class DMFloatKeyIndex<T> 
        where T : IEquatable<T>
    {
        private Dictionary<DMTonality, PitchedNoteIndex<T>> indexPerDM = new Dictionary<DMTonality, PitchedNoteIndex<T>>();

        private Func<T, DMFloatKey> getDMFloatKey;

        public DMFloatKeyIndex(Func<T, DMFloatKey> getDMFloatKey)
        {
            if (getDMFloatKey == null) throw new ArgumentNullException();
            this.getDMFloatKey = getDMFloatKey;

            foreach (string name in DMTonality.Names)
            {
                indexPerDM[DMTonality.PerName(name)] = new PitchedNoteIndex<T>(t => getDMFloatKey(t).RootNote);
            }
        }

        public void Put(T t)
        {
            var dmFloatKey = getDMFloatKey(t);
            if (dmFloatKey != null)
                indexPerDM[dmFloatKey.Tonality].Put(t);
        }

        public IEnumerable<T> GetSlice(DMFloatKey centre, CentimeOffset maxDeviation)
        {
            return indexPerDM[centre.Tonality].GetSlice(centre.RootNote, maxDeviation);
        }

        public void Remove(T t)
        {
            var dmFloatKey = getDMFloatKey(t);
            if (dmFloatKey != null)
                indexPerDM[dmFloatKey.Tonality].remove(t);

        }

        public void Reset()
        {
            foreach (var dmTonality in indexPerDM.Keys)
            {
                indexPerDM[dmTonality].Clear();
            }
        }
    }
}
