﻿using next.Modulo;
using System;
using System.Collections.Generic;

namespace next.Store.KeyIndex
{
    public class CyclicIndex<T> : ICyclicIndex<T, int>
        where T : IEquatable<T>
    {
        private int bucketCount;

        // TODO : SortedSet may speed up removal 
        List<T>[] buckets; 
        Circularr<List<T>> bucketCycle;
        private Func<T, int> getId;

        public CyclicIndex(int size, Func<T, int> getId)
        {
            if (getId == null || getId == null) throw new ArgumentNullException();
            this.bucketCount = size;
            this.buckets = new List<T>[size];
            this.bucketCycle = new Circularr<List<T>>(this.buckets);
            this.getId = item => (getId(item)).Modulo(size);
        }

        public void Put(T t)
        {
            var bucketId = this.getId(t);

            if (this.buckets[bucketId] == null)
                this.buckets[bucketId] = new List<T>();

            this.buckets[bucketId].Add(t);
        }

        public bool Remove(T t)
        {
            var bucketId = this.getId(t);
            return this.buckets[bucketId].Remove(t);
        }

        public IEnumerable<T> GetSection(int id)
        {
            foreach (var track in bucketCycle[id] ?? new List<T>())
                yield return track;
        }

        public IEnumerable<T> GetSlice(int centre, int offset)
        {
            foreach (var bucket in bucketCycle.GetSlice(centre, offset))
                if (bucket != null)
                    foreach (T item in bucket)
                        yield return item;
        }

        internal void Clear()
        {
            foreach (var bucket in this.buckets)
                bucket.Clear();
        }
    }
}
