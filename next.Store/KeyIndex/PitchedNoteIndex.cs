﻿using System;
using System.Collections.Generic;
using next.Modulo;
using next.Tonality;
using next.Tracks;

namespace next.Store.KeyIndex
{
    public class PitchedNoteIndex<T> : ICyclicIndex<T, PitchedNote>
        where T : IEquatable<T>
    {
        public static int CentimesCount = 1200;

        private Func<T, PitchedNote> getPitchedNote;

        private CyclicIndex<T> chain;

        public PitchedNoteIndex(Func<T, PitchedNote> getPitchedNote)
        {
            if (getPitchedNote == null) throw new ArgumentNullException();

            this.getPitchedNote = getPitchedNote;
            this.chain = new CyclicIndex<T>(CentimesCount, t => pitchedNoteToCentime(getPitchedNote(t)));
        }

        public void Put(T t)
        {
            chain.Put(t);
        }

        public void remove(T t)
        {
            chain.Remove(t);
        }

        private int pitchedNoteToCentime(PitchedNote pitchedNote)
        {
            return (int)Math.Round(pitchedNote * 100.0).Modulo(CentimesCount);
        }

        public IEnumerable<T> GetSection(PitchedNote pitchedNote)
        {
            return chain.GetSection(pitchedNoteToCentime(pitchedNote));
        }

        public IEnumerable<T> GetSlice(PitchedNote centre, CentimeOffset maxOffset)
        {
            int discreteCentre = (int)(Math.Round(100 * centre));
            int discreteOffset = (int)Math.Round(maxOffset);

            return chain.GetSlice(discreteCentre, discreteOffset);
        }

        internal void Clear()
        {
            chain.Clear();
        }
    }
}
