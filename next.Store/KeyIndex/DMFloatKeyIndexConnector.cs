﻿using next.Store.TrackFiles;
using next.Tracks;
using System;
using System.Collections.Specialized;
using System.Linq;

namespace next.Store.KeyIndex
{
    public class DMFloatKeyIndexConnector : IDisposable
    {
        private TrackFileCollection collection;
        private DMFloatKeyIndex<TrackFile> dmFloatKeyIndex = new DMFloatKeyIndex<TrackFile>(t => t.N100Key);

        public DMFloatKeyIndexConnector(TrackFileCollection collection, DMFloatKeyIndex<TrackFile> dmFloatKeyIndex)
        {
            if (collection == null) throw new ArgumentNullException();
            this.collection = collection;

            foreach(var track in collection)
            {
                try
                {
                    dmFloatKeyIndex.Put(track);
                }
                catch (Exception ex)
                {
                    var message = ex.Message;
                }
            }

            collection.CollectionChanged += OnCollectionChanged;
        }

        public void OnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {            
            var newItem = e.NewItems == null ? null : e.NewItems.OfType<TrackFile>().FirstOrDefault();
            var oldItem = e.OldItems == null ? null : e.OldItems.OfType<TrackFile>().FirstOrDefault();

            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    if (newItem != null)
                        dmFloatKeyIndex.Put(newItem);
                    break;

                case NotifyCollectionChangedAction.Remove:
                    if (oldItem != null)
                        dmFloatKeyIndex.Remove(oldItem);
                    break;

                case NotifyCollectionChangedAction.Reset:
                    dmFloatKeyIndex.Reset();
                    break;
                
                default:
                    throw new NotImplementedException("Inexhaustive switch");
            }
        }

        public void Dispose()
        {
            this.collection.CollectionChanged -= OnCollectionChanged;
            this.collection = null;
            this.dmFloatKeyIndex = null;
        }
    }
}
