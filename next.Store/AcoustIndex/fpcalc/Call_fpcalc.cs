﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace next.Store.AcoustIndex.fpcalc
{
    public class Call_fpcalc
    {
        public class Result
        {
            public int ExitCode;
            public Exception RunException;
            public StringBuilder Output;
            public StringBuilder Error;
        }

        private FileInfo fpcalcFileInfo;

        public Call_fpcalc(FileInfo fpcalcFileInfo)
        {
            if (fpcalcFileInfo == null) throw new ArgumentException();
            this.fpcalcFileInfo = fpcalcFileInfo;
        }

        public Result Execute(FileInfo trackFile, string args = null)
        {
            var runResults = new Result
            {
                Output = new StringBuilder(),
                Error = new StringBuilder(),
                RunException = null
            };

            try
            {
                if (fpcalcFileInfo.Exists)
                {
                    using (Process proc = new Process())
                    {
                        proc.StartInfo.FileName = fpcalcFileInfo.FullName;
                        proc.StartInfo.Arguments = String.Format("{0} \"{1}\"", args, trackFile.FullName);
                        // proc.StartInfo.WorkingDirectory = workingDirectory;
                        proc.StartInfo.UseShellExecute = false;
                        proc.StartInfo.RedirectStandardOutput = true;
                        proc.StartInfo.RedirectStandardError = true;
                        proc.StartInfo.CreateNoWindow = true;
                        proc.OutputDataReceived +=
                            (o, e) => runResults.Output.Append(e.Data).Append(Environment.NewLine);
                        proc.ErrorDataReceived +=
                            (o, e) => runResults.Error.Append(e.Data).Append(Environment.NewLine);
                        proc.Start();
                        proc.BeginOutputReadLine();
                        proc.BeginErrorReadLine();
                        proc.WaitForExit();
                        runResults.ExitCode = proc.ExitCode;
                    }
                }
                else
                {
                    throw new ArgumentException("Invalid executable path.", "exePath");
                }
            }
            catch (Exception e)
            {
                runResults.RunException = e;
            }
            return runResults;

        }
    }
}
