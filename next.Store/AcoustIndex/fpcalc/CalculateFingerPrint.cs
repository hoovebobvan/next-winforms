﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace next.Store.AcoustIndex.fpcalc
{
    public class CalculateFingerprint
    {
        public const string defaultArgs = "-LENGTH 1200 -raw";

        private Call_fpcalc call_fpcalc;
        private Func<string, Fingerprint> toFingerprint;

        public CalculateFingerprint(Call_fpcalc call_fpcalc, Func<string, Fingerprint> toFingerPrint)
        {
            this.call_fpcalc = call_fpcalc;
            this.toFingerprint = toFingerPrint;
        }

        public Task<Result> Execute(TaskScheduler scheduler, FileInfo trackFileInfo)
        {
            return Task<Result>.Factory.StartNew(_ =>
                {
                    return ExecuteSync(trackFileInfo);

                }, scheduler);
        }

        public Result ExecuteSync(FileInfo trackFileInfo)
        {
            if (!trackFileInfo.Exists)
                return new FailResult(trackFileInfo) { ErrorMessage = "File not found: " + trackFileInfo.FullName };

            var result = call_fpcalc.Execute(trackFileInfo, defaultArgs);

            if (result.ExitCode == 0)
            {
                try
                {
                    var fp = toFingerprint(result.Output.ToString());
                    return new SuccesResult(fp);
                }
                catch (Exception ex)
                {
                    return new FailResult(trackFileInfo) { ErrorMessage = ex.Message };
                }
            }
            else
            {
                return new FailResult(trackFileInfo) { ErrorMessage = result.Error.ToString() };
            }
        }

        public class Result
        {
            public Result(FileInfo fileInfo)
            {
                this.FileInfo = fileInfo;
            }
            public bool IsSuccess { get; internal set; }
            public FileInfo FileInfo { get; internal set; }
        }

        public class SuccesResult : Result
        {
            public SuccesResult(Fingerprint fingerprint)
                : base(fingerprint.TrackFileInfo)
            {
                this.Fingerprint = fingerprint;
                IsSuccess = true;
            }
            public Fingerprint Fingerprint { get; internal set; }
        }

        public class FailResult : Result
        {
            public FailResult(FileInfo fileInfo)
                : base(fileInfo)
            {
                IsSuccess = false;
            }
            public string ErrorMessage { get; internal set; }
        }

        public static CalculateFingerprint Create()
        {
            var calcFP = new CalculateFingerprint(
                new Call_fpcalc(new FileInfo(@"acoustId\fpcalc.exe")), 
                new Func<string, Fingerprint>(ToFingerPrint.FromOutputString));

            return calcFP;
        }
    }
}
