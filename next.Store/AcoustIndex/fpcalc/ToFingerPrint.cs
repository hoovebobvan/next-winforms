﻿using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace next.Store.AcoustIndex.fpcalc
{
    public class ToFingerPrint
    {
        public static Regex rawOutputPattern =
            new Regex(@"FILE=([^\r\n]+)[\r\n]+DURATION=([\d]+)[\s\t\r\n]+FINGERPRINT=([\-\,\d]+)[\r\n]*", RegexOptions.IgnoreCase);
        //                   <--- 1 -->                <- 2 ->                       <--- 3 --->
        // 1: File 
        // 2: Duration
        // 3: Fingerprint (raw) 

        public static Fingerprint FromOutputString(string outputString)
        {
            var match = rawOutputPattern.Match(outputString);
            if (!match.Success) throw new FormatException("cannot parse output string");

            string filepath = match.Groups[1].Value;
            var fileInfo = new FileInfo(filepath);
            int trackLengthSeconds = int.Parse(match.Groups[2].Value);
            string rawFingerprint = match.Groups[3].Value;
            Int32[] fingerprint = rawFingerprint
                .Split(',')
                .Select(int32string => Int32.Parse(int32string))
                .ToArray();

            return new Fingerprint(fileInfo, trackLengthSeconds, fingerprint);
        }
    }
}
