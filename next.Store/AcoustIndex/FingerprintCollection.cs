﻿using System;
using System.Collections.Generic;

namespace next.Store.AcoustIndex
{
    public class FingerprintCollection : ICollection<Fingerprint>
    {
        private SortedSet<Fingerprint> items = new SortedSet<Fingerprint>();

        public void Add(Fingerprint item)
        {
            items.Add(item);
        }

        public void AddMany(IEnumerable<Fingerprint> fingerprints)
        {
            foreach(var fingerprint in fingerprints)
            {
                items.Add(fingerprint);
            }
        }

        public void Clear()
        {
            items.Clear();
        }

        public bool Contains(Fingerprint item)
        {
            return items.Contains(item);
        }

        public void CopyTo(Fingerprint[] array, int arrayIndex)
        {
            items.CopyTo(array, arrayIndex);
        }

        public int Count
        {
            get { return items.Count; }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        public bool Remove(Fingerprint item)
        {
            return items.Remove(item);
        }

        public IEnumerator<Fingerprint> GetEnumerator()
        {
            return items.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return items.GetEnumerator();
        }
    }
}
