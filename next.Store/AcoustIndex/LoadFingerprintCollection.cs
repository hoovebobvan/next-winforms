﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace next.Store.AcoustIndex
{
    public class LoadFingerprintCollection
    {
        public static string CollectionFileName = "next-track-fingerprints.json";

        public Task Execute(TaskScheduler scheduler, FingerprintCollection collection, DirectoryInfo directoryInfo)
        {
            return Task.Factory.StartNew(_ =>
            {
                ExecuteSync(collection, directoryInfo);
            },
            scheduler);
        }

        private void ExecuteSync(FingerprintCollection collection, DirectoryInfo directoryInfo)
        {
            var filePath = Path.Combine(directoryInfo.FullName, LoadFingerprintCollection.CollectionFileName);

            if (File.Exists(filePath))
            {
                using (var reader = new StreamReader(filePath, Encoding.UTF8))
                {
                    var dto_s = JsonConvert.DeserializeObject<IEnumerable<FingerprintDTO>>(reader.ReadToEnd());
                    var fingerPrints = dto_s.Select(dto => FingerprintDTO.ToFingerprint(dto));
                    collection.AddMany(fingerPrints);
                }
            }

        }


    }
}
