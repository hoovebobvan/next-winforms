﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace next.Store.AcoustIndex
{
    public class SaveFingerpintCollection
    {
        public Task Execute(TaskScheduler scheduler, FingerprintCollection collection, DirectoryInfo directoryInfo)
        {
            return Task.Factory.StartNew(_ =>
            {
                ExecuteSync(collection, directoryInfo);
            },
            scheduler);
        }

        private void ExecuteSync(FingerprintCollection collection, DirectoryInfo directoryInfo)
        {
            var filePath = Path.Combine(directoryInfo.FullName, LoadFingerprintCollection.CollectionFileName);

            using (var writer = new StreamWriter(filePath, append: false, encoding: Encoding.UTF8))
            {
                var jsonDTOs = collection.Select(fp => FingerprintDTO.FromFingerprint(fp));
                writer.Write(JsonConvert.SerializeObject(jsonDTOs, Formatting.Indented));
            }
        }
    }
}
