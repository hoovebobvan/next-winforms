﻿using next.Store.AcoustIndex.fpcalc;
using next.Store.TrackFiles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace next.Store.AcoustIndex
{
    public class FingerprintDataContext
    {
        private FingerprintCollection fingerprintCollection;
        private TrackFileCollection trackFileCollection;
        private BatchCalculateFingerprint batchCalculateFingerprint;

        public FingerprintDataContext(FingerprintCollection fingerprintCollection, TrackFileCollection trackFileCollection, BatchCalculateFingerprint batchCalculateFingerprint)
        {
            // TODO: Complete member initialization
            this.fingerprintCollection = fingerprintCollection;
            this.trackFileCollection = trackFileCollection;
            this.batchCalculateFingerprint = batchCalculateFingerprint;
        }

        public IObservable<CalculateFingerprint.Result> CalculateMissingFingerprints()
        {
            return null;
        }
    }
}
