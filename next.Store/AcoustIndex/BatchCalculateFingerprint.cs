﻿using next.Store.AcoustIndex.fpcalc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace next.Store.AcoustIndex
{
    public class BatchCalculateFingerprint
    {
        private CalculateFingerprint calculateFingerprint = CalculateFingerprint.Create();

        public Task<IObservable<CalculateFingerprint.Result>> Execute(
            TaskScheduler scheduler, 
            IEnumerable<FileInfo> trackFiles, 
            CancellationToken cancellationToken)
        {
            return Task<IObservable<CalculateFingerprint.Result>>.Factory.StartNew(_ =>
                {
                    return ExecuteSync(trackFiles, cancellationToken);
                
                }, scheduler);
        }

        private IObservable<CalculateFingerprint.Result> ExecuteSync(
            IEnumerable<FileInfo> trackFiles, 
            CancellationToken cancellationToken)
        {
            return Observable.Create<CalculateFingerprint.Result>(
                (IObserver<CalculateFingerprint.Result> observer) =>
                {
                    foreach (var trackFile in trackFiles)
                    {
                        if (cancellationToken.IsCancellationRequested)
                        {
                            throw new OperationCanceledException(cancellationToken);
                        }

                        try
                        {
                            var calcResult = calculateFingerprint.ExecuteSync(trackFile);
                            observer.OnNext(calcResult);
                        }
                        catch 
                        {
                            // TODO: log something
                        }
                    }

                    observer.OnCompleted();

                    return Disposable.Empty;
                });
        }
    }
}
