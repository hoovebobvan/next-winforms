﻿using System;

namespace next.Store.AcoustIndex
{
    public class FeatureVector
    {
        public const int MaxFeatureDepth = 16;

        private UInt32 value;

        public FeatureVector(UInt32 value)
        {
            this.value = value;
        }

        public FeatureVector(Int32 value)
        {
            unchecked
            {
                this.value = (UInt32)value;
            }
        }

        public UInt32 Value { get { return value; } }

        public string BitString { get { return Convert.ToString(value, 2).PadLeft(32, '0'); } }

        public static implicit operator UInt32(FeatureVector fv)
        {
            return fv.Value;
        }

        public static int Difference(UInt32 a, UInt32 b, int featureDepth = MaxFeatureDepth)
        {
            UInt32 uncommon = a ^ b;
            uncommon &= masksPerFeatureDepth[featureDepth];
            return CountBits(uncommon);
        }

        public static int CountBits(uint value)
        {
            // TODO: If this becomes part of an inner loop consider byte-chunk based lookup table
            int count = 0;
            while (value != 0)
            {
                count++;
                value &= value - 1;
            }
            return count;
        }

        private static UInt32[] masksPerFeatureDepth = new UInt32[MaxFeatureDepth];

        static FeatureVector()
        {
            masksPerFeatureDepth[0] = 0;
            for (int i = 1; i <= masksPerFeatureDepth.Length; i++)
            {
                masksPerFeatureDepth[i] = (masksPerFeatureDepth[i - 1] << 2) + 3;
            }
        }
    }
}
