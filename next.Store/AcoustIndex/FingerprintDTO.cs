﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace next.Store.AcoustIndex
{
    public class FingerprintDTO
    {
        public string filepath { get; set; }
        public int duration_seconds { get; set; }
        public Int32[] featureVectors { get; set; }

        public static FingerprintDTO FromFingerprint(Fingerprint fp)
        {
            var featureVectors = new List<Int32>();
            var dto = new FingerprintDTO
            {
                filepath = fp.TrackFileInfo.FullName,
                duration_seconds = fp.TrackDurationSeconds
            };
            unchecked
            {
                dto.featureVectors = fp.FingerPrint
                    .Select(fp_uint => (Int32)fp_uint.Value)
                    .ToArray();
            }
            return dto;
        }

        public static Fingerprint ToFingerprint(FingerprintDTO dto)
        {
            return new Fingerprint(
                new FileInfo(dto.filepath), 
                dto.duration_seconds, 
                dto.featureVectors
            );
        }

    }
}
