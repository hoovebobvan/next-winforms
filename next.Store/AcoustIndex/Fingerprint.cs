﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace next.Store.AcoustIndex
{
    public class Fingerprint : IEquatable<Fingerprint>, IComparable<Fingerprint>
    {
        private List<FeatureVector> fingerPrint;

        public Fingerprint(FileInfo fileInfo, int trackDurationSeconds, Int32[] fingerPrint)
        {
            if (fileInfo == null) throw new ArgumentNullException();

            this.TrackFileInfo = fileInfo;
            this.TrackDurationSeconds = trackDurationSeconds;
            this.fingerPrint = fingerPrint.Select(int_32 => new FeatureVector(int_32)).ToList();
        }

        public FileInfo TrackFileInfo { get; private set; }
        public int TrackDurationSeconds { get; private set; }
        public IEnumerable<FeatureVector> FingerPrint { get { return fingerPrint.AsEnumerable(); } }
        public int FingerPrintSize { get { return fingerPrint.Count; } }

        public bool Equals(Fingerprint that)
        {
            return this.TrackFileInfo.FullName == that.TrackFileInfo.FullName;
        }

        public int CompareTo(Fingerprint that)
        {
            return StringComparer.OrdinalIgnoreCase.Compare(
                this.TrackFileInfo.FullName, that.TrackFileInfo.FullName);
        }
    }
}
