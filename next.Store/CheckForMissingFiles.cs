﻿using next.Tracks;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace next.Store
{
    public class CheckForMissingFiles 
    {
        public async Task<SortedSet<TrackFile>> ExecuteAsync(TrackFileCollection collection)
        {
            var missing = new SortedSet<TrackFile>();

            foreach (var track in collection)
            {
                if (!track.FileInfo.Exists)
                {
                    missing.Add(track);
                }
            }

            return missing;
        }
    }
}
