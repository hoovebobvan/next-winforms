﻿using next.Tonality;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace next
{
    [TestFixture]
    public class OpenKeyCodeTest
    {
        /*
         *      (OpenKeyNumber, DurRootNote, MollRootNote)
         *      :
         * 		(1, "C", "A"),
         *		(2, "G", "E"),
         *		(3, "D", "B"),
         *		(4, "A", "F#"),
         *		(5, "E", "C#"),
         *		(6, "B", "G#"),
         *		(7, "F#", "D#"),
         *		(8, "Db", "Bb"),
         *		(9, "Ab", "F"),
         *		(10, "Eb", "C"),
         *		(11, "Bb", "G"),
         *		(12, "F", "D")	
         */

        private static List<OpenKeyCode> allKeyCodes = Enumerable.Range(1, 12)
            .SelectMany(n => new List<OpenKeyCode> { 
                new OpenKeyCode(String.Format("{0}d", n)), 
                new OpenKeyCode(String.Format("{0}m", n))
            })
            .ToList();

        [Test]
        public void OpenKeyCodeConvertsToDMKey()
        {
            var dmKeysPerOKC = new Dictionary<OpenKeyCode, DMKey>();

            foreach (var okc in allKeyCodes)
            {
                DMKey key = (DMKey)okc;
                dmKeysPerOKC[okc] = key;
            }

            Assert.IsTrue(true);
        }
    }
}
