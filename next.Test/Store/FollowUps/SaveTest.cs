﻿using next.FollowUps;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace next.Store.FollowUps
{
    [TestFixture]
    public class LoadAndSaveTest
    {
        private const string silliconscally_default = "M:\\music\\releases\\silicon-scally---bioroid\\09---silicon-scally---default.flac";
    
        public static FileInfo GetTempLocation()
        {
            return new FileInfo(Path.Combine(Path.GetTempPath(), "savefollupstest.json"));
        }

        public static async Task<FollowUpCollection> GetSampleCollection()
        {
            var sampleTracksCollection = await Store.TrackFiles.LoadAndSaveTests.LoadSampleCollection();

            var siliconScally_Default = sampleTracksCollection.GetByPath("M:\\music\\releases\\silicon-scally---bioroid\\09---silicon-scally---default.flac");
            var siliconScally_Render = sampleTracksCollection.GetByPath("M:\\music\\releases\\silicon-scally---bioroid\\10---silicon-scally---rendered.flac");
            var age_InTheBeginning = sampleTracksCollection.GetByPath("M:\\music\\releases\\age---isolation\\01---age---in-the-beginning.flac");
            var age_nocturne = sampleTracksCollection.GetByPath("M:\\music\\releases\\age---age-analogue-generated-energy-the-force-inc-years-1993-1998\\972435---age---nocturne.mp3");

            var followUpCollection = new FollowUpCollection();

            followUpCollection.Insert(new FollowUp(siliconScally_Default, age_InTheBeginning, Rating.Ok));
            followUpCollection.Insert(new FollowUp(siliconScally_Render, age_InTheBeginning, Rating.Negative));
            followUpCollection.Insert(new FollowUp(siliconScally_Default, age_nocturne, Rating.Good));
            followUpCollection.Insert(new FollowUp(age_nocturne, siliconScally_Default, Rating.Ok));

            return followUpCollection;
        }

        public async Task<FileInfo> SaveSampleFollowUps()
        {
            var collection = await GetSampleCollection();

            var save = new SaveFollowUps();
            var tempLocation = GetTempLocation();
            bool saveSuccess = await save.Execute(collection, tempLocation);

            return saveSuccess ? tempLocation : null;
        }

        [Test]
        public async void CanSaveSampleCollection()
        {
            var followUpsFile = await SaveSampleFollowUps();
            Assert.IsTrue(followUpsFile != null);
        }

        [Test]
        public async void CanLoadPreviouslySavedSampleCollection()
        {
            var sampleTracksCollection = await Store.TrackFiles.LoadAndSaveTests.LoadSampleCollection();
            var sampleFollowUps = await GetSampleCollection();
            var followUpsFile = await SaveSampleFollowUps();

            var load = new LoadFollowUps();
            var loadedFollowUps = await load.ExecuteAsync(new FollowUpCollection(), GetTempLocation(), sampleTracksCollection);

            Assert.IsTrue(sampleFollowUps.GetMatchesFrom(silliconscally_default).Count() == loadedFollowUps.GetMatchesFrom(silliconscally_default).Count());
        }
    }
}
