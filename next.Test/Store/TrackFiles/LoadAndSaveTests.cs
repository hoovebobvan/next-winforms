﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace next.Store.TrackFiles
{
    [TestFixture]
    public class LoadAndSaveTests
    {    
        public static async Task<TrackFileCollection> LoadSampleCollection()
        {
            var exeFileLocation = new FileInfo(Assembly.GetExecutingAssembly().Location);
            var sampleCollectionFileInfo = new FileInfo(Path.Combine(exeFileLocation.Directory.FullName, "Resources", "samplecollection.json"));

            var load = new LoadTrackFileCollection();
            var collection = new TrackFileCollection();
            await load.ExecuteAsync(TaskScheduler.Default, collection, sampleCollectionFileInfo);

            return collection;
        }
        
        [Test]
        public async void LoadsCollectionFile()
        {
            var collection = await LoadSampleCollection();            

            // the test file contains 95 entries,
            // two of which have no key information
            Assert.AreEqual(93, collection.Count);
        }
    }
}
