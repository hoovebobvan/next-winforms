﻿
namespace next.Modulo
{
    public static class ModuloExtensions
    {
        public static int Modulo(this int number, int m)
        {
            int remainder = number % m;
            return remainder < 0 ? remainder + m : remainder;
        }

        public static double Modulo(this double number, double m)
        {
            double remainder = number % m;
            return remainder < 0 ? remainder + m : remainder;
        }
    }
}
