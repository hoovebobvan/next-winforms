﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace next.Tracks
{
    // TODO : Just make a CanonicalName class
    public static class GetCanonicalNameExtension
    {
        // TODO : Rather than using this for name-based matching use the lucene index to 
        //          find proper approximations

        public static Dictionary<String, String> replacements = new Dictionary<string, string>
        {
            { " ", "-" },
            { "_", "-" },
            { "&", "and" },
            { ",", "-" },
            { "`", "'" },
            { ".", "-" },
            { "â", "a" },
            { "é", "e" },
            { "ê", "e" },
            { "(original mix)", ""},
        };

        private static Regex sequentialDashPattern = new Regex("-+");
        private static Regex trimDashPattern = new Regex("^-+|-+&");

        public static string GetCanonicalName(this TrackFile track)
        {
            return GetCanonicalName(track.Artist, track.Title);
        }

        public static string GetCanonicalName(string artist, string title)
        {
            return String.Format("{0}---{1}", NormalizeNamePart(artist), NormalizeNamePart(title));
        }

        public static string NormalizeNamePart(string part)
        {
            part = part ?? "";
            part = part.Trim().ToLower();
            foreach (var key in replacements.Keys)
            {
                part = part.Replace(key, replacements[key]);
            }
            part = sequentialDashPattern.Replace(part, "-");
            part = trimDashPattern.Replace(part, "");
            return part;
        }
    }
}
