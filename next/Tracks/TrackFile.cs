﻿using next.Tonality;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.IO;
using System.Linq;

namespace next.Tracks
{
    // TODO : Move 'LuceneIdentifier' to TrackFileIndex.Entry

    public class TrackFile: ITrackFile, IEquatable<TrackFile>, IComparable<TrackFile>
    {
        private string luceneIdentifier;
        private SortedSet<string> likes = new SortedSet<string>();
        private SortedSet<string> tags = new SortedSet<string>();

        public string Artist { get; protected set; }
        public string Artist_Title { get { return String.Format("{0} - {1}", Artist, Title); } }
        public string Title { get; protected set; }
        public string CanonicalName { get; protected set; }
        public string ReleaseTitle { get; protected set; }
        public string ReleaseTrackId { get; protected set; }
        public double BPM { get; protected set; }
        public DMKey Key { get; protected set; }
        public DMFloatKey N100Key { get; private set; }
        public OpenKeyCode OpenKeyCode { get; protected set; }
        public DateTime? ImportDate { get; protected set; }
        public Centime? N100PhasePerCent { get { return N100Key == null ? null : (Centime?)N100Key.KeyPhasePercent; } }

        public FileInfo FileInfo { get; private set; }

        // Law of Demeter

        public TrackFile(
            FileInfo fileInfo,
            string artist,
            string title,
            string releaseTitle,
            string releaseTrackId,
            double bpm,
            DMKey key = null,
            DateTime? importDate = null,
            ISet<string> liked_by = null,
            ISet<string> tags_in = null)
        {
            this.FileInfo = fileInfo;
            this.Artist = artist;
            this.Title = title;
            this.CanonicalName = this.GetCanonicalName();
            this.ReleaseTitle = releaseTitle;
            this.ReleaseTrackId = releaseTrackId;
            this.BPM = bpm;
            this.Key = key;
            this.N100Key = this.DeriveN100Key();
            this.OpenKeyCode = key == null ? null : (OpenKeyCode)key;
            this.luceneIdentifier = ToLuceneIdentifier(fileInfo);
            this.ImportDate = importDate;

            if (liked_by != null && liked_by.Any())
            {
                liked_by.ToList().ForEach(name => AddLike(name));
            }

            if (tags_in != null && tags_in.Any())
            {
                tags_in.ToList().ForEach(tag => AddTag(tag));
            }
        }

        public TrackFile(ITrackFile source)
            : this(
            source.FileInfo,
            source.Artist, 
            source.Title, 
            source.ReleaseTitle, 
            source.ReleaseTrackId, 
            source.BPM, 
            source.Key, 
            importDate: source.ImportDate)
        { }

        public string FilePath { get { return FileInfo.FullName;  } }

        public bool IsLiked { get { return likes.Any(); } }

        public bool AddLike(string by)
        {
            return likes.Add(by);
        }

        public bool RemoveLike(string by)
        {
            return likes.Remove(by);
        }

        public bool AddTag(string tag)
        {
            return tags.Add(tag);
        }

        public bool RemoveTag(string tag)
        {
            return tags.Remove(tag);
        }

        public bool HasTag(string tag)
        {
            return tags.Contains(tag);
        }

        public ImmutableSortedSet<string> Likes
        {
            get { return likes.ToImmutableSortedSet(); }
        }

        public ImmutableSortedSet<string> Tags
        {
            get { return tags.ToImmutableSortedSet(); }
        }

        public void ClearLikes()
        {
            likes.Clear();
        }

        #region IEquatable
        public static bool operator ==(TrackFile a, TrackFile b)
        {
            return Object.ReferenceEquals(a, b)
                || (object)a != null 
                && (object)b != null
                && a.FileInfo.FullName == b.FileInfo.FullName;
        }

        public static bool operator !=(TrackFile a, TrackFile b)
        {
            return !(a == b);
        }

        public override bool Equals(object that)
        {
            return this == that as TrackFile;
        }

        public override int GetHashCode()
        {
            return FileInfo.FullName.GetHashCode();
        }
        #endregion

        #region IComparable
        public int CompareTo(TrackFile that)
        {
            return StringComparer.OrdinalIgnoreCase.Compare(this.FileInfo.FullName, that.FileInfo.FullName);
        }

        public bool Equals(TrackFile that)
        {
            return this == that;
        }
        #endregion

        public static string ToLuceneIdentifier(FileInfo fileInfo)
        {
            return fileInfo.FullName.Replace("\\", "|");
        }

        public string LuceneIdentifier { get { return luceneIdentifier; } }
    }
}
