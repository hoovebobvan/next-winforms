﻿using next.Tonality;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.IO;

namespace next.Tracks
{
    public interface ITrackFile
    {
        FileInfo FileInfo { get; }
        string Artist { get; }
        string Title { get; }
        string ReleaseTitle { get; }
        string ReleaseTrackId { get; }
        double BPM { get; }
        DMKey Key { get; }
        DateTime? ImportDate { get; }
    }
}
