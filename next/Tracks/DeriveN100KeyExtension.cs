﻿
using next.Tonality;

namespace next.Tracks
{
    public static class DeriveN100KeyExtension
    {
        public static double NormBPM = 100.0;

        public static DMPitchedKey DeriveN100Key(this TrackFile track)
        {
            if (track.Key == null || track.BPM == 0)
                return null;

            var tempoFactor = NormBPM / (double)track.BPM;
            var pitchedKey = DMPitchedKey.Create(track.Key, tempoFactor);
            return pitchedKey;
        }
    }
}
