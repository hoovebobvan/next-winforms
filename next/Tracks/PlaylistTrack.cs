﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace next.Tracks
{
    public class PlaylistTrack : IEquatable<PlaylistTrack>
    {
        public string Identifier { get; set; }
        public string Artist { get; set; }
        public string Title { get; set; }
        public string Release { get; set; }

        // TODO : Add reference to collection tracks if it can be made

        public bool Equals(PlaylistTrack that)
        {
            return that != null && this.Identifier == that.Identifier;
        }
    }
}
