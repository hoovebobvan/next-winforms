﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace next.Tracks
{
    public class Playlist
    {
        private List<PlaylistTrack> sequence = new List<PlaylistTrack>();

        public Playlist(List<PlaylistTrack> sequence)
        {
            if (sequence == null) throw new ArgumentNullException("sequence");
            this.sequence = sequence;
        }

        public string Title { get; set; }

        public IEnumerable<PlaylistTrack> Sequence { get { return sequence.AsEnumerable(); } }
    }
}
