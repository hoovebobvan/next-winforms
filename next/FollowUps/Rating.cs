﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace next.FollowUps
{
    public enum Rating 
    { 
        Negative = -1, 
        Unknown = 0, 
        Ok = 1, 
        Good = 2 
    }
}
