﻿using next.Tracks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace next.FollowUps
{
    public class FollowUp : IComparable<FollowUp>, IEquatable<FollowUp>
    {
        public FollowUp(TrackFile from, TrackFile to, Rating rating = Rating.Unknown, string remarks = "")
        {
            this.From = from;
            this.To = to;
            this.Rating = rating;
            this.Remarks = remarks;
        }

        public TrackFile From { get; private set; }
        public TrackFile To { get; private set; }
        public Rating Rating { get; private set; }
        public string Remarks { get; private set; }

        public int CompareTo(FollowUp that)
        {
            int comparison = From.CanonicalName.CompareTo(that.From.CanonicalName);
            if (comparison != 0)
                return comparison;

            return To.CanonicalName.CompareTo(that.To.CanonicalName);
        }

        public bool Equals(FollowUp that)
        {
            // for now the intention is to just have 1 follow up at a time, 
            // assuming the application has 1 typical user
            return this.From == that.From && this.To == that.To;
        }
    }
}
