﻿using System;

namespace next.Tonality
{
    public class DMKey 
    {
        private Note12 note;
        private DMTonality tonality;

        public DMKey(Note12 note, DMTonality tonality)
        {
            this.note = note;
            this.tonality = tonality;
        }

        public Note12 Note { get { return this.note; } }
        public DMTonality Tonality { get { return this.tonality; } }

        public static bool operator ==(DMKey a, DMKey b)
        {
            return Object.ReferenceEquals(a, b)
                || (object)a != null
                && (object)b != null
                && a.Note.Value == b.Note.Value && a.Tonality.Name == b.Tonality.Name;
        }

        public static bool operator !=(DMKey a, DMKey b)
        {
            return !(a == b);
        }

        public override bool Equals(object that)
        {
            return this == (that as DMKey);
        }

        public override int GetHashCode()
        {
            return (int)Note.Value + (Tonality.IsMajor ? 0 : 12);
        }

        public override string ToString()
        {
            return Note.Value.ToString() + Tonality.Name;
        }
    }
}
