﻿using next.Modulo;
using System;
using System.Collections.Generic;

namespace next.Tonality
{
    public enum Note12Values { C, Db, D, Eb, E, F, Gb, G, Ab, A, Bb, B }

    public struct Note12
    {
        public static Dictionary<string, Note12Values> commonNotationMapping =
            new Dictionary<string, Note12Values>
            {
                { "c", Note12Values.C },
                { "c#", Note12Values.Db }, { "db", Note12Values.Db },
                { "d", Note12Values.D },
                { "d#", Note12Values.Eb }, { "eb", Note12Values.Eb },
                { "e", Note12Values.E },
                { "f", Note12Values.F },
                { "f#", Note12Values.Gb }, { "gb", Note12Values.Gb },
                { "g", Note12Values.G },
                { "g#", Note12Values.Ab }, { "ab", Note12Values.Ab },
                { "a", Note12Values.A }, 
                { "a#", Note12Values.Bb }, { "bb", Note12Values.Bb },
                { "b", Note12Values.B }
            };

        private Note12Values value;

        public Note12Values Value { get { return value; } }

        public Note12(string repr)
        {
            string reprForLookup = repr.Trim().ToLower();
            
            if (!commonNotationMapping.ContainsKey(reprForLookup))
                throw new ArgumentException("invalid note representation");

            this.value = commonNotationMapping[reprForLookup];
        }

        public Note12(int value)
        {
            value = value.Modulo(12);
            this.value = (Note12Values)value;  
        }

        public static implicit operator int(Note12 note12)
        {
            return (int)note12.value;
        }

        public override string ToString()
        {
            return Enum.GetName(typeof(Note12Values), this.value);
        }
    }
}
