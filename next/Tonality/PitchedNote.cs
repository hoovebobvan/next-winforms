﻿using next.Modulo;
using System;

namespace next.Tonality
{
    public struct PitchedNote
    {
        public static double Dodeca = 12.0;

        private double value;

        public PitchedNote(double note)
        {
            double canonicalNote = note.Modulo(Dodeca);
            this.value = canonicalNote;
        }

        public double Value { get { return this.value; } }

        public static implicit operator double(PitchedNote pitchedNote)
        {
            return pitchedNote.value;
        }

        public static PitchedNote operator +(PitchedNote a, double b)
        {
            return new PitchedNote(a.Value + b);
        }
        
        public static PitchedNote operator -(PitchedNote a, double b)
        {
            return new PitchedNote(a.Value - b);
        }

        public static bool operator ==(PitchedNote a, double b)
        {
            return a.Value == b;
        }

        public static bool operator !=(PitchedNote a, double b)
        {
            return !(a == b);
        }

        public override int GetHashCode()
        {
            return this.Value.GetHashCode();
        }

        public override bool Equals(object that)
        {
            if (Object.ReferenceEquals(this, that))
                return true;

            return that is PitchedNote && (PitchedNote)that == this;
        }

        public override string ToString()
        {
            return Value.ToString("0.00");
        }
    }
}
