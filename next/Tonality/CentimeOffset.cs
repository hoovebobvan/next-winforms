﻿using next.Modulo;

namespace next.Tonality
{
    public struct CentimeOffset
    {
        public static readonly double Cent = 100;
        public static double DemiCent = 50;

        private double offsetValue;

        public CentimeOffset(double value)
        {
            double valueModuloCent = (value).Modulo(Cent);
            this.offsetValue = valueModuloCent > DemiCent 
                ? valueModuloCent - Cent : valueModuloCent;
        }

        public static implicit operator double(CentimeOffset c)
        {
            return c.Value;
        }

        public static PitchedNote operator +(PitchedNote a, CentimeOffset b)
        {
            return new PitchedNote(a.Value + b.Value / Cent);
        }

        public static PitchedNote operator -(PitchedNote a, CentimeOffset b)
        {
            return new PitchedNote(a.Value - b.Value / Cent);
        }

        public override string ToString()
        {
            return Value.ToString("0.0");
        }

        public double Value { get { return offsetValue; } }
    }
}
