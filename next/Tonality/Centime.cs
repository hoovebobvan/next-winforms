﻿using next.Modulo;

namespace next.Tonality
{
    public struct Centime
    {
        public static readonly double Cent = 100;

        private double valueModuloCent;

        public Centime(double value)
        {
            this.valueModuloCent = value.Modulo(Cent);
        }

        public Centime(PitchedNote note)
        {
            this.valueModuloCent = (note.Value * 100).Modulo(Cent);
        }

        public static implicit operator double(Centime centime)
        {
            return centime.Value;
        }

        public CentimeOffset GetOffsetTo(Centime that)
        {
            return new CentimeOffset(that.Value - this.Value);
        }

        public double Value { get { return valueModuloCent; } }

        public override string ToString()
        {
            return Value.ToString("0");
        }
    }
}
