﻿using System;
using System.Collections.Generic;

namespace next.Tonality
{
    // TODO : Use an enum :S

    public struct DMTonality : IEquatable<DMTonality>
    {
        public static List<String> Names = new List<String> { "d", "m" };
        internal int dm_name_index;

        public string Name { get { return Names[dm_name_index]; } }

        public bool IsMajor { get { return dm_name_index == 0; } }

        private DMTonality(string dm_name)
        {
            this.dm_name_index = Names.IndexOf(dm_name);
        }

        static DMTonality()
        {
            Major = new DMTonality("d");
            Minor = new DMTonality("m");
        }

        public static bool operator ==(DMTonality a, DMTonality b)
        {
            return a.dm_name_index == b.dm_name_index;
        }

        public static bool operator !=(DMTonality a, DMTonality b)
        {
            return !(a.dm_name_index == b.dm_name_index);
        }

        public override int GetHashCode()
        {
            return dm_name_index.GetHashCode();
        }

        public static DMTonality Major;
        public static DMTonality Minor; 

        public static DMTonality PerName(string name)
        {
            switch (name.ToLower())
            {
                case "d": return Major;
                case "m": return Minor;
                default: throw new ArgumentException();
            }
        }

        public override string ToString()
        {
            return this.Name;
        }

        internal static DMTonality Other(DMTonality dMTonality)
        {
            return dMTonality.IsMajor ? Minor : Major;
        }

        public bool Equals(DMTonality that)
        {
            return that != null && this == that;
        }

        public override bool Equals(object obj)
        {
            return obj is DMTonality && this.Equals((DMTonality) obj);
        }
    }
}
