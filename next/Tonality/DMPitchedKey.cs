﻿using System;

namespace next.Tonality
{
    public class DMPitchedKey : DMFloatKey
    {
        public static double SemitoneFactor = Math.Pow(2.0, (1.0 / 12.0));

        public PitchedNote OriginNote { get; protected set; }

        public DMPitchedKey(PitchedNote pitchedNote, DMTonality tonality, PitchedNote originNote)
            : base(pitchedNote, tonality)
        {
            this.OriginNote = originNote;
        }

        public static DMPitchedKey Create(DMKey key, double tempoFactor)
        {
            var originRootNote = new PitchedNote((double)key.Note);
            var semitoneOffset = Math.Log(tempoFactor, SemitoneFactor);
            var pitchedRootNote = new PitchedNote(originRootNote + semitoneOffset);
            return new DMPitchedKey(pitchedRootNote, key.Tonality, new PitchedNote(originRootNote));
        }
    }
}
