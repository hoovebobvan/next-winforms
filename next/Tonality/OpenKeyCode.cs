﻿using next.Modulo;
using System;
using System.Text.RegularExpressions;

namespace next.Tonality
{
    public class OpenKeyCode
    {
        public static Regex Pattern = new Regex("(0?[1-9]|10|11|12)([dm])", RegexOptions.IgnoreCase);

        private string code;
        private int number;
        private DMTonality dmTonality;

        public OpenKeyCode(string code)
        {
            if (String.IsNullOrWhiteSpace(code)) throw new ArgumentException("Empty key code");
            var matchResult = Pattern.Match(code);
            if (!matchResult.Success)
                throw new ArgumentException("Invalid key code");

            this.code = code;
            this.number = int.Parse(matchResult.Groups[1].Value);
            this.dmTonality = DMTonality.PerName(matchResult.Groups[2].Value);
        }

        public string Code { get { return this.code; } }
        public int Number { get { return this.number; } }
        public DMTonality DMTonality { get { return this.dmTonality; } }

        public override string ToString()
        {
            return code;
        }

        public static implicit operator string(OpenKeyCode openKeyCode)
        {
            return openKeyCode == null ? "" : openKeyCode.code;
        }

        public static explicit operator DMKey(OpenKeyCode openKeyCode)
        {
            /*
             *  The formula below is based on the following mapping 
             *  
             *      (OpenKeyNumber, DurRootNote, MollRootNote, durNumber, mollNumber)
             *      :
             * 		1,      "C",    "A"     0   9
             *		2,      "G",    "E"     7   4
             *		3,      "D",    "B"     2   11
             *		4,      "A",    "F#"    9   6
             *		5,      "E",    "C#"    4   1
             *		6,      "B",    "G#"    11  8
             *		7,      "F#",   "D#"    6   3
             *		8,      "Db",   "Bb"    1   10
             *		9,      "Ab",   "F"     8   5
             *		10,     "Eb",   "C"     3   0
             *		11,     "Bb",   "G"     10  7
             *		12,     "F",    "D"	    5   2
             */
            int dmtNumber =
                (
                    (openKeyCode.Number - 1) * 7
                    -
                    (openKeyCode.DMTonality.IsMajor ? 0 : 3)
                )
                .Modulo(12);

            return new DMKey(new Note12(dmtNumber), openKeyCode.DMTonality);
        }

        public static explicit operator OpenKeyCode(DMKey dmKey)
        {
            /*
             *  0       1d     10m
             *  1       8d     5m
             *  2       3d     12m 
             *  3       10d    7m
             *  4       5d     2m
             *  ...
             */ 
            int openKeyNumber = 
                (
                    dmKey.Note * 7 
                    - 
                    (dmKey.Tonality.IsMajor ? 0 : 3)
                )
                .Modulo(12) + 1;

            return new OpenKeyCode(String.Format("{0}{1}", openKeyNumber, dmKey.Tonality));
        }
    }
}
