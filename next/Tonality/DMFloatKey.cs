﻿using System;

namespace next.Tonality
{
    public class DMFloatKey
    {
        public DMTonality Tonality { get; protected set; }
        public PitchedNote RootNote { get; protected set; }
        public bool IsMajor { get { return this.Tonality.IsMajor; } }
        public Centime KeyPhasePercent { get; protected set; }

        public DMFloatKey(PitchedNote rootNote, DMTonality dmTonality)
        {
            this.Tonality = dmTonality;
            this.RootNote = rootNote;
            this.KeyPhasePercent = new Centime(RootNote);
        }

        public override string ToString()
        {
            return String.Format("{0} {1}", RootNote.ToString(), Tonality.Name);
        }

        public static DMFloatKey FromDMKey(DMKey dmKey)
        {
            var pitchedNote = new PitchedNote(dmKey.Note);
            return new DMFloatKey(pitchedNote, dmKey.Tonality);
        }
    }
}
