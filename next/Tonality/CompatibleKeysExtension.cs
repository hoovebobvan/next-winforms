﻿using System.Collections.Generic;

namespace next.Tonality
{
    public static class CompatibleKeysExtension
    {
        public static int[] fifthAndFourth = new int[] { +7, -7 };

        public static DMFloatKey GetTonalRelative(this DMFloatKey dmf)
        {
            var offset = dmf.Tonality.IsMajor ? -3.0 : +3.0;
            return new DMFloatKey(new PitchedNote(dmf.RootNote + offset), DMTonality.Other(dmf.Tonality));
        }

        public static IEnumerable<DMFloatKey> GetCompatibleKeys(this DMFloatKey originKey, bool includeTTNeighbors = false)
        {
            yield return originKey;
            yield return originKey.GetTonalRelative();

            foreach (int offset in fifthAndFourth)
            {
                var offsetKey = new DMFloatKey(originKey.RootNote + offset, originKey.Tonality);
                yield return offsetKey;

                if (includeTTNeighbors)
                {
                    yield return offsetKey.GetTonalRelative();
                }
            }
        }
    }
}
